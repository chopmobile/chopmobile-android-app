package Model;

import java.util.ArrayList;

public interface CartUpdateListener {
	
	
	public void onCartUpdate(ArrayList<SubProduct> cartList, Double totalSum, String type, SubProduct cartProduct);
	
	public void onAllBrandProduct(String brandId);
	
	

}
