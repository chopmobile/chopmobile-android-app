package Model;

public class SubProduct {

	String item_id;
	String item_name;
	String item_image,item_bigimage;
	String MRP_price;
	String Selling_price;

	public String getProduct_quantity() {
		return product_quantity;
	}

	public void setProduct_quantity(String product_quantity) {
		this.product_quantity = product_quantity;
	}

	String validdiscount;
	String offerPercentage;
	String pd_wieght;
	String item_in_cart, brandId, brandName, description;
	String showprod, isOutOfStock;
	int count,attribute_id;
	String isFavorite, avgRating, isGift,locationspecific, avgRatingIcon,prd_alias;
	String product_quantity;

	public String getitem_ID() {
		return item_id;
	}

	public void setitem_ID(String item_id) {
		this.item_id = item_id;
	}

	public String getitem_name() {
		return item_name;
	}

	public void setitem_name(String item_name1) {
		this.item_name = item_name1;
	}

	public String getShowProduct() {
		return showprod;
	}

	public void setShowProduct(String showprod) {
		this.showprod = showprod;
	}

	public String getMRP_price() {
		return MRP_price;
	}

	public void setMRP_price(String MRP_price) {
		this.MRP_price = MRP_price;
	}

	public String getSelling_price() {
		return Selling_price;
	}

	public void setSelling_price(String Selling_price) {
		this.Selling_price = Selling_price;
	}

	public String getvaliddiscount() {
		return validdiscount;
	}

	public void setvaliddiscount(String validdiscount1) {
		this.validdiscount = validdiscount1;
	}

	public String getpd_wieght() {
		return pd_wieght;
	}

	public void setpd_wieght(String pd_wieght1) {
		this.pd_wieght = pd_wieght1;
	}

	public int getqty_count() {
		return count;
	}

	public void setqty_count(int item_qty_count) {
		this.count = item_qty_count;
	}

	public String getitem_in_cart() {
		return item_in_cart;
	}

	public void setitem_in_cart(String item_in_cart1) {
		this.item_in_cart = item_in_cart1;
	}

	public String getitem_image() {
		return item_image;
	}

	public void setitem_image(String item_image) {
		this.item_image = item_image;
	}

	public String getitem_bigimage() {
		return item_bigimage;
	}

	public void setitem_bigimage(String item_bimage) {
		this.item_bigimage = item_bimage;
	}

	public String getIsOutOfStock() {
		return isOutOfStock;
	}

	public void setIsOutOfStock(String isOutOfStock) {
		this.isOutOfStock = isOutOfStock;
	}

	public String getBrandId() {
		return brandId;
	}

	public void setBrandId(String brandId) {
		this.brandId = brandId;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getIsFavorite() {
		return isFavorite;
	}

	public void setIsFavorite(String isFavorite) {
		this.isFavorite = isFavorite;
	}

	public void setIsGift(String isGift) {
		this.isGift = isGift;
	}

	public String getAvgRatingIcon() {
		return avgRatingIcon;
	}

	public void setAvgRatingIcon(String avgRatingicon) {
		this.avgRatingIcon = avgRatingicon;
	}

	public String getAvgRating() {
		return avgRating;
	}

	public void setAvgRating(String avgRating) {
		this.avgRating = avgRating;
	}

	public String getOfferPercentage() {
		return offerPercentage;
	}

	public void setOfferPercentage(String offerPercentage) {
		this.offerPercentage = offerPercentage;
	}

	public void setlocationspecific(String locationspecific) {
		this.locationspecific = locationspecific;
	}


	public String getlocationspecific() {
		return locationspecific;
	}

	public int getAttribute_id() {
		return attribute_id;
	}

	public void setAttribute_id(int attribute_id) {
		this.attribute_id = attribute_id;
	}

	public String getProduct_Alias() {
		return prd_alias;
	}

	public void setProduct_Alias(String prd_alias) {
		this.prd_alias = prd_alias;
	}

}
