package Model;

import java.util.ArrayList;

/**
 * Created by user1 on 24-Mar-17.
 */

public class Prouduct_Addons {
    private int addon_id;
    private String addon_name;
    private String addon_price;
    private String addon_status;
    ArrayList<String> selectedStrings = new ArrayList<String>();

    public String getAddon_price() {
        return addon_price;
    }

    public void setAddon_price(String addon_price) {
        this.addon_price = addon_price;
    }

    public String getAddon_name() {
        return addon_name;
    }

    public void setAddon_name(String addon_name) {
        this.addon_name = addon_name;
    }

    public int getAddon_id() {
        return addon_id;
    }

    public void setAddon_id(int addon_id) {
        this.addon_id = addon_id;
    }

    public ArrayList<String> getSelectedStrings() {
        return selectedStrings;
    }

    public void setSelectedStrings(ArrayList<String> selectedStrings) {
        this.selectedStrings = selectedStrings;
    }

    public String getAddon_status() {
        return addon_status;
    }

    public void setAddon_status(String addon_status) {
        this.addon_status = addon_status;
    }
}
