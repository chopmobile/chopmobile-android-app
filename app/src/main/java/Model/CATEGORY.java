package Model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by user1 on 06-Mar-17.
 */

public class CATEGORY implements Serializable{
    private String category_id;
    private String category_name;
    private String image;
 private ArrayList<Products1>menu_data=new ArrayList<>();

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public ArrayList<Products1> getMenu_data() {
        return menu_data;
    }

    public void setMenu_data(ArrayList<Products1> menu_data) {
        this.menu_data = menu_data;
    }
}
