package Model;

import java.util.ArrayList;

/**
 * Created by user1 on 12-Apr-17.
 */

public class ORDER_HISTORY {
    public String order_id;
    public String order_date;
    public String delivery_type;
    public String price;
    public String pickup_time;
    public String pickup_status;
    public String delivery_address;
    private ArrayList<Products1>products=new ArrayList<>();

    public String getOrder_date() {
        return order_date;
    }

    public void setOrder_date(String order_date) {
        this.order_date = order_date;
    }

    public String getDelivery_type() {
        return delivery_type;
    }

    public void setDelivery_type(String delivery_type) {
        this.delivery_type = delivery_type;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getPickup_time() {
        return pickup_time;
    }

    public void setPickup_time(String pickup_time) {
        this.pickup_time = pickup_time;
    }

    public String getDelivery_address() {
        return delivery_address;
    }

    public void setDelivery_address(String delivery_address) {
        this.delivery_address = delivery_address;
    }

    public String getPickup_status() {
        return pickup_status;
    }

    public void setPickup_status(String pickup_status) {
        this.pickup_status = pickup_status;
    }

    public ArrayList<Products1> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<Products1> products) {
        this.products = products;
    }
}
