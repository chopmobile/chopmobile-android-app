package Model;

import java.util.ArrayList;

/**
 * Created by user1 on 24-Mar-17.
 */

public class PRODUCT_DETAILS {
    private String menu_name;
    private String menu_image;
    private String image;
    private String menu_price;
    private String id;
        private ArrayList<Prouduct_Addons> menu_data=new ArrayList<>();

    public String getMenu_name() {
        return menu_name;
    }

    public void setMenu_name(String menu_name) {
        this.menu_name = menu_name;
    }

    public String getMenu_image() {
        return menu_image;
    }

    public void setMenu_image(String menu_image) {
        this.menu_image = menu_image;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public ArrayList<Prouduct_Addons> getMenu_data() {
        return menu_data;
    }

    public void setMenu_data(ArrayList<Prouduct_Addons> menu_data) {
        this.menu_data = menu_data;
    }

    public String getMenu_price() {
        return menu_price;
    }

    public void setMenu_price(String menu_price) {
        this.menu_price = menu_price;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
