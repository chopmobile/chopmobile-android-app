package Model;

/**
 * Created by user1 on 13-Mar-17.
 */

public class Settings {

    private String app_color;
    private String flash_screen;
    private String app_text_color;
    private String app_logo;
    private String font;
    private String transparent_app_color;
    private String transparent_text_color;
    private String background_app_color;
    private String background_button_image_color;
    private String background_button_text_color;
    private String background_image;
    private String app_topbar_color;



    public String getApp_color() {
        return app_color;
    }

    public void setApp_color(String app_color) {
        this.app_color = app_color;
    }

    public String getFlash_screen() {
        return flash_screen;
    }

    public void setFlash_screen(String flash_screen) {
        this.flash_screen = flash_screen;
    }

    public String getApp_logo() {
        return app_logo;
    }

    public void setApp_logo(String app_logo) {
        this.app_logo = app_logo;
    }

    public String getFont() {
        return font;
    }

    public void setFont(String font) {
        this.font = font;
    }

    public String getTransparent_app_color() {
        return transparent_app_color;
    }

    public void setTransparent_app_color(String transparent_app_color) {
        this.transparent_app_color = transparent_app_color;
    }

    public String getTransparent_text_color() {
        return transparent_text_color;
    }

    public void setTransparent_text_color(String transparent_text_color) {
        this.transparent_text_color = transparent_text_color;
    }

    public String getBackground_app_color() {
        return background_app_color;
    }

    public void setBackground_app_color(String background_app_color) {
        this.background_app_color = background_app_color;
    }

    public String getApp_text_color() {
        return app_text_color;
    }

    public void setApp_text_color(String app_text_color) {
        this.app_text_color = app_text_color;
    }

    public String getBackground_button_image_color() {
        return background_button_image_color;
    }

    public void setBackground_button_image_color(String background_button_image_color) {
        this.background_button_image_color = background_button_image_color;
    }

    public String getBackground_button_text_color() {
        return background_button_text_color;
    }

    public void setBackground_button_text_color(String background_button_text_color) {
        this.background_button_text_color = background_button_text_color;
    }

    public String getApp_topbar_color() {
        return app_topbar_color;
    }

    public void setApp_topbar_color(String app_topbar_color) {
        this.app_topbar_color = app_topbar_color;
    }

    public String getBackground_image() {
        return background_image;
    }

    public void setBackground_image(String background_image) {
        this.background_image = background_image;
    }
}
