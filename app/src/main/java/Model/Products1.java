package Model;

import java.io.Serializable;

/**
 * Created by user1 on 14-Mar-17.
 */

public class Products1 implements Serializable{
    private String food_type;
    private String menu_cuisine;
    private String menu_description;
    private String menu_id;
    private String menu_image;
    private String menu_name;
    private String menu_total;
    private String quantity;
    private String menuitem_quantity;

    public String getFood_type() {
        return food_type;
    }

    public void setFood_type(String food_type) {
        this.food_type = food_type;
    }

    public String getMenu_cuisine() {
        return menu_cuisine;
    }

    public void setMenu_cuisine(String menu_cuisine) {
        this.menu_cuisine = menu_cuisine;
    }

    public String getMenu_id() {
        return menu_id;
    }

    public void setMenu_id(String menu_id) {
        this.menu_id = menu_id;
    }

    public String getMenu_description() {
        return menu_description;
    }

    public void setMenu_description(String menu_description) {
        this.menu_description = menu_description;
    }

    public String getMenu_image() {
        return menu_image;
    }

    public void setMenu_image(String menu_image) {
        this.menu_image = menu_image;
    }

    public String getMenu_name() {
        return menu_name;
    }

    public void setMenu_name(String menu_name) {
        this.menu_name = menu_name;
    }

    public String getMenu_total() {
        return menu_total;
    }

    public void setMenu_total(String menu_total) {
        this.menu_total = menu_total;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getMenuitem_quantity() {
        return menuitem_quantity;
    }

    public void setMenuitem_quantity(String menuitem_quantity) {
        this.menuitem_quantity = menuitem_quantity;
    }
}
