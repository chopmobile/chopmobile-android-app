package Model;


public class Products {
    String name,image,mrp,selling,gram,offerer;
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getMrp() {
		return mrp;
	}

	public void setMrp(String mrp) {
		this.mrp = mrp;
	}

	public String getSelling() {
		return selling;
	}

	public void setSelling(String selling) {
		this.selling = selling;
	}

	public String getGram() {
		return gram;
	}

	public void setGram(String gram) {
		this.gram = gram;
	}

	public String getOfferer() {
		return offerer;
	}

	public void setOfferer(String offerer) {
		this.offerer = offerer;
	}

	String menu_id, menu_name, submenu_id, submenu_name, submenu_count,brandId,brandName;

	String item_id, item_name, offer_status, offer, actual_price, final_price,
			cat_id, cat_name, validity_status, validity_date, pd_wieght,
			validdiscount, pd_qty, total_purchase, pd_short_description,
			pd_description, couponcode, location_specific, loc_avail,
			loc_address, city_name, location_name, item_image, item_image_big,
			item_in_cart;

	SubProduct pd0, pd1, pd2, pd3, pd4;

	String Is_Favorite;

	public String getIs_Favorite() {
		return Is_Favorite;
	}

	public void setIs_Favorite(String Is_Favorite) {
		this.Is_Favorite = Is_Favorite;
	}

	public SubProduct getprod_0() {
		return pd0;
	}

	public void setprod_0(SubProduct pd0) {
		this.pd0 = pd0;
	}

	public SubProduct getprod_1() {
		return pd1;
	}

	public void setprod_1(SubProduct pd1) {
		this.pd1 = pd1;
	}

	public SubProduct getprod_2() {
		return pd2;
	}

	public void setprod_2(SubProduct pd2) {
		this.pd2 = pd2;
	}

	public SubProduct getprod_3() {
		return pd3;
	}

	public void setprod_3(SubProduct pd3) {
		this.pd3 = pd3;
	}

	public SubProduct getprod_4() {
		return pd4;
	}

	public void setprod_4(SubProduct pd4) {
		this.pd4 = pd4;
	}

	public String getmenu_id() {
		return menu_id;
	}

	public void setmenu_id(String menu_id) {
		this.menu_id = menu_id;
	}

	public String getmenu_name() {
		return menu_name;
	}

	public void setmenu_name(String menu_name) {
		this.menu_name = menu_name;
	}

	public String getsubmenu_id() {
		return submenu_id;
	}

	public void setsubmenu_id(String submenu_id) {
		this.submenu_id = submenu_id;
	}

	public String getsubmenu_name() {
		return submenu_name;
	}

	public void setsubmenu_name(String submenu_name) {
		this.submenu_name = submenu_name;
	}

	public String getsubmenu_count() {
		return submenu_count;
	}

	public void setsubmenu_count(String submenu_count) {
		this.submenu_count = submenu_count;
	}

	public String getitem_id() {
		return item_id;
	}

	public void setitem_id(String item_id) {
		this.item_id = item_id;
	}

	public String getitem_name() {
		return item_name;
	}

	public void setitem_name(String item_name) {
		this.item_name = item_name;
	}

	public String getvaliddiscount() {
		return validdiscount;
	}

	public void setvaliddiscount(String validdiscount) {
		this.validdiscount = validdiscount;
	}

	public String getoffer_status() {
		return offer_status;
	}

	public void setoffer_status(String offer_status) {
		this.offer_status = offer_status;
	}

	public String getoffer() {
		return offer;
	}

	public void setoffer(String offer) {
		this.offer = offer;
	}

	public String getactual_price() {
		return actual_price;
	}

	public void setactual_price(String actual_price) {
		this.actual_price = actual_price;
	}

	public String getfinal_price() {
		return final_price;
	}

	public void setfinal_price(String final_price) {
		this.final_price = final_price;
	}

	public String getcat_id() {
		return cat_id;
	}

	public void setcat_id(String cat_id) {
		this.cat_id = cat_id;
	}

	public String getcat_name() {
		return cat_name;
	}

	public void setcat_name(String cat_name) {
		this.cat_name = cat_name;
	}

	public String getvalidity_status() {
		return validity_status;
	}

	public void setvalidity_status(String validity_status) {
		this.validity_status = validity_status;
	}

	public String getvalidity_date() {
		return validity_date;
	}

	public void setvalidity_date(String validity_date) {
		this.validity_date = validity_date;
	}

	public String getpd_wieght() {
		return pd_wieght;
	}

	public void setpd_wieght(String pd_wieght) {
		this.pd_wieght = pd_wieght;
	}

	public String getpd_qty() {
		return pd_qty;
	}

	public void setpd_qty(String pd_qty) {
		this.pd_qty = pd_qty;
	}

	public String gettotal_purchase() {
		return total_purchase;
	}

	public void settotal_purchase(String total_purchase) {
		this.total_purchase = total_purchase;
	}

	public String getpd_short_description() {
		return pd_short_description;
	}

	public void setpd_short_description(String pd_short_description) {
		this.pd_short_description = pd_short_description;
	}

	public String getpd_description() {
		return pd_description;
	}

	public void setpd_description(String pd_description) {
		this.pd_description = pd_description;
	}

	public String getcouponcode() {
		return couponcode;
	}

	public void setcouponcode(String couponcode) {
		this.couponcode = couponcode;
	}

	public String getlocation_specific() {
		return location_specific;
	}

	public void setlocation_specific(String location_specific) {
		this.location_specific = location_specific;
	}

	public String getlocation_address() {
		return loc_address;
	}

	public void setlocation_address(String loc_address) {
		this.loc_address = loc_address;
	}

	public String getlocation_availability() {
		return loc_avail;
	}

	public void setlocation_availability(String loc_avail) {
		this.loc_avail = loc_avail;
	}

	public String getcity_name() {
		return city_name;
	}

	public void setcity_name(String city_name) {
		this.city_name = city_name;
	}

	public String getlocation_name() {
		return location_name;
	}

	public void setlocation_name(String location_name) {
		this.location_name = location_name;
	}

	public String getitem_image() {
		return item_image;
	}

	public void setitem_image(String item_image) {
		this.item_image = item_image;
	}

	public String getitem_image_big() {
		return item_image_big;
	}

	public void setitem_image_big(String item_image_big) {
		this.item_image_big = item_image_big;
	}

	public String getitem_in_cart() {
		return item_in_cart;
	}

	public void setitem_in_cart(String item_in_cart) {
		this.item_in_cart = item_in_cart;
	}

	public String getBrandId() {
		return brandId;
	}

	public void setBrandId(String brandId) {
		this.brandId = brandId;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	

}
