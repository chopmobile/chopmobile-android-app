package Adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.omkarsoft.chopmobile.Product_Details;
import com.omkarsoft.chopmobile.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import Model.PRODUCT_DETAILS;
import other.AppController;

/**
 * Created by user1 on 12-Apr-17.
 */

public class Search_Adapter extends RecyclerView.Adapter<Search_Adapter.ViewHolder> {

    Activity activity;
    private ArrayList<PRODUCT_DETAILS> speakers_arr;

    // Provide a suitable constructor
    public Search_Adapter(Activity activity, ArrayList<PRODUCT_DETAILS> speakers_arr) {
        this.activity = activity;
        this.speakers_arr = speakers_arr;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_adapter, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element


        PRODUCT_DETAILS speakers_order = speakers_arr.get(position);
        holder.offer_image.setTag(position);
        ImageLoader imageLoader = AppController.getInstance().getImageLoader();

        Picasso.with(activity)
                .load(speakers_order.getMenu_image().toString())
                .into(holder.offer_image);

        holder.product_name.setText(speakers_order.getMenu_name());
        holder.product_price.setText("$"+speakers_order.getMenu_price());
        onclick_listeners(holder,position);
    }


    private void onclick_listeners(final ViewHolder holder, final int position) {
        holder.offer_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos=(Integer)view.getTag();
                Intent i=new Intent(activity,Product_Details.class);
                i.putExtra("Product_ID",speakers_arr.get(pos).getId());
                i.putExtra("Product_Name",speakers_arr.get(pos).getMenu_name());
                i.putExtra("Tabvalue",String.valueOf(0));
                i.putExtra("Offers_Value",String.valueOf(3));
                activity.startActivity(i);
            }
        });
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }
    @Override
    public int getItemCount() {
        return speakers_arr.size();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        ImageView offer_image;
        TextView product_name,product_price;


        public ViewHolder(View v) {
            super(v);


            offer_image = (ImageView) v.findViewById(R.id.imgThumbnail);
            product_name=(TextView)v.findViewById(R.id.lblstore);
            product_price=(TextView)v.findViewById(R.id.prod_off_cost);

        }


    }
}
