package Adapters;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.omkarsoft.chopmobile.R;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import HellpersClass.CHOPMOBILE_CONSTANTS;
import HellpersClass.Utils;
import Model.Prouduct_Addons;

/**
 * Created by DELL on 27/6/2016.
 */
public class Addon_Adapter extends RecyclerView.Adapter<Addon_Adapter.ViewHolder> {

    Activity activity;
    ArrayList<Prouduct_Addons> category_list;
    Prouduct_Addons addons;
    ArrayList<String> selectedStrings;
    String product_id;
    String quantity;
    ViewHolder vh;
    String SELECTED_ADDONID, CHECKE_VALUE;
    private ProgressDialog pDialog;

    public Addon_Adapter(Activity activity, ArrayList<Prouduct_Addons> category_list, ArrayList<String> selectedStrings, String product_id, String quantity) {
        // Provide a suitable constructor
        this.activity = activity;
        this.category_list = category_list;
        this.selectedStrings = selectedStrings;
        this.product_id = product_id;
        this.quantity = quantity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.addon_adapter, parent, false);
        // set the view's size, margins, paddings and layout parameters
        vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        pDialog = new ProgressDialog(activity);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
        ColorStateList colorStateList = new ColorStateList(
                new int[][]{

                        new int[]{-android.R.attr.state_enabled}, //disabled
                        new int[]{android.R.attr.state_enabled} //enabled
                },
                new int[] {

                        Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")) //disabled
                        ,Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")) //enabled

                }
        );
        holder.root_lay.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")));

        //  holder.root_lay.setImageUrl(category_list.get(position).getImage(),imageLoader);
        holder.addon_name.setText(category_list.get(position).getAddon_name());
        holder.addon_name.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")));
        holder.addon_name.setButtonTintList(colorStateList);
        holder.root_lay.setText("$" + category_list.get(position).getAddon_price());

        if(category_list.get(position).getAddon_status().equals("1")){
            holder.addon_name.setChecked(true);
        }
        else {
            holder.addon_name.setChecked(false);
        }

//        if(Prefs.getString(CHOPMOBILE_CONSTANTS.product_quantity, "").equals("0")){
//            holder.addon_name.setChecked(false);
//            SELECTED_ADDONID="0";
//        }

            onclick_listeners(vh, position);

    }

    private void onclick_listeners(final ViewHolder holder, final int position) {
        addons = new Prouduct_Addons();

        holder.addon_name.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    if (Integer.parseInt(Prefs.getString(CHOPMOBILE_CONSTANTS.product_quantity, "")) <= 0) {
                        Toast.makeText(activity, "Please add Product to Cart", Toast.LENGTH_LONG).show();
                        holder.addon_name.setChecked(false);
                    }
                    else {
                        System.out.println("cuming inside if condi---");
                        selectedStrings.add(String.valueOf(category_list.get(position).getAddon_id()));
                        for (int i = 0; i < selectedStrings.size(); i++) {
                            System.out.println("selectedarray---" + selectedStrings.get(i).toString());
                        }
                        SELECTED_ADDONID = String.valueOf(category_list.get(position).getAddon_id());
                        Fetch_Product(activity, holder);
                        CHECKE_VALUE = "1";
                    }
                } else {
                    System.out.println("cuming inside else condi---");
                    selectedStrings.remove(String.valueOf(category_list.get(position).getAddon_id()));
                    SELECTED_ADDONID = String.valueOf(category_list.get(position).getAddon_id());
                    Fetch_Product(activity, holder);
                    CHECKE_VALUE = "0";
                }


            }
        });



    }

    @Override
    public int getItemCount() {
        return category_list.size();
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case

        TextView root_lay, product_price;
        CheckBox addon_name;

        public ViewHolder(View v) {
            super(v);

            //  video_name = (TextView) v.findViewById(R.id.aie_order_history_orderno);
            root_lay = (TextView) v.findViewById(R.id.add_price);
            addon_name = (CheckBox) v.findViewById(R.id.checkbox_add);
            product_price = (TextView) activity.findViewById(R.id.price);

        }
    }

    public void Fetch_Product(Context context, final ViewHolder holder) {
        showProgressDialog();
        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest sr = new StringRequest(Request.Method.POST, Utils.DOMAIN_NAME, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                hideProgressDialog();
                System.out.println("responsestring123---" + response.toString());
                Gson gson = new Gson();
                try {
                    JSONObject jobj = new JSONObject(response);
                    System.out.println("favevalueplus--" + jobj.getString("detail_page_total"));
                    holder.product_price.setText("$" + jobj.getString("detail_page_total"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // mPostCommentResponse.requestEndedWithError(error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("page_name", "oncheck_menuaddon");
                params.put("menu_id", product_id);
                params.put("menuaddon_id", SELECTED_ADDONID);
                params.put("qty", Prefs.getString(CHOPMOBILE_CONSTANTS.product_quantity, ""));
                params.put("checked", CHECKE_VALUE);
                params.put("user_id", Prefs.getString(CHOPMOBILE_CONSTANTS.shared_user_id, ""));
                //params.put("location_id",Prefs.getString(CHOPMOBILE_CONSTANTS.location_id,""));
                params.put("rest",Prefs.getString("",Prefs.getString(CHOPMOBILE_CONSTANTS.Restaurant_Id,"")));
                System.out.println("productallparams--" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(sr);
        sr.setShouldCache(false);
    }

    private void showProgressDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideProgressDialog() {
        if (pDialog.isShowing())
            pDialog.hide();
        pDialog.dismiss();
    }

}

