package Adapters;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.omkarsoft.chopmobile.Checkout;
import com.omkarsoft.chopmobile.Product_Details;
import com.omkarsoft.chopmobile.R;
import com.pixplicity.easyprefs.library.Prefs;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import HellpersClass.CHOPMOBILE_CONSTANTS;
import HellpersClass.Utils;
import Model.Products1;
import other.AppController;

import static com.omkarsoft.chopmobile.R.id.imageView2;

/**
 * Created by user1 on 31-Mar-17.
 */

public class CheckOut_Adapter extends RecyclerView.Adapter<CheckOut_Adapter.ViewHolder> {

    Activity activity;
    private ArrayList<Products1> events_arr;
    JSONArray odr_his_jArray;
    Boolean edit_text = true;
    String menu_price, overall_price;
    private ProgressDialog pDialog;
    // Provide a suitable constructor
    public CheckOut_Adapter(Activity activity, ArrayList<Products1> events_arr) {
        this.activity = activity;
        this.events_arr = events_arr;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.checkout_adapter, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        pDialog = new ProgressDialog(activity);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);

        Products1 events_order = events_arr.get(position);
        holder.edit_qty.setTag(position);
        holder.delete_item.setTag(position);
        System.out.println("checkevents---"+events_arr.get(position).toString());

        holder.title.setText(events_order.getMenu_name());
        holder.menu_price.setText("Total: $" + events_order.getMenu_total());
        holder.menu_quantity.setText("" + events_order.getQuantity());
        ImageLoader imageLoader = AppController.getInstance().getImageLoader();

        //background
        holder.title.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_text_color,"")));
        holder.menu_price.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_text_color,"")));
        holder.menu_quantity.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_text_color,"")));
        holder.checkout_whole.setBackgroundColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_background_color,"")));
        holder.checkout_totalprice.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")));
        holder.taxt_precntage.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")));
        holder.Taxamount.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")));
        holder.textView7.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_text_color,"")));


        Picasso.with(activity)
                .load(events_order.getMenu_image())
                .resize(350, 350)
                .centerCrop()
                .into(holder.imageView12);

        onclick_listeners(holder, position);


    }


    private void onclick_listeners(final ViewHolder holder, final int position) {

//        if (events_arr.get(position).getDescription() != null && !events_arr.get(position).getDescription().isEmpty()) {
//
//            holder.view_more.setVisibility(View.VISIBLE);
//        }
//
        holder.edit_qty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int pos = (Integer) v.getTag();
                if (edit_text) {
                    System.out.println("cuming inside edit true");
                    holder.menu_quantity.setFocusableInTouchMode(true);
                    holder.menu_quantity.setFocusable(true);
                    holder.menu_quantity.setClickable(true);
                    holder.menu_quantity.setBackgroundColor(Color.parseColor("#4CBB17"));
                    edit_text = false;
                } else {
                    holder.menu_quantity.setBackgroundColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_background_color,"")));
                    holder.menu_quantity.setFocusableInTouchMode(false);
                    holder.menu_quantity.setFocusable(false);
                    System.out.println("cuming inside edit false");
                    String quantity_change = holder.menu_quantity.getText().toString();
                    System.out.println("insideonclicklistener--" + events_arr.get(position).getMenu_id());
                    postNewQunatity(activity, events_arr.get(position).getMenu_id(), holder.menu_quantity.getText().toString(),holder);

                    edit_text = true;
                }

            }
        });
        holder.delete_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = (Integer) view.getTag();
                postDeleteItem(activity,events_arr.get(pos).getMenu_id());
            }
        });
        holder.imageView12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    Intent i = new Intent(activity, Product_Details.class);
//                System.out.println("productidintent---"+position_value);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    i.putExtra("Product_ID", events_arr.get(position).getMenu_id());
                    i.putExtra("Product_Name", events_arr.get(position).getMenu_name());
                    i.putExtra("Tabvalue", String.valueOf("0"));
                    activity.startActivity(i);
                    activity.finish();
            }
        });

    }


    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public int getItemCount() {
        return events_arr.size();
    }

    // Provide a reference to the views for each data item
// Complex data items may need more than one view per item, and
// you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        TextView date, title, event_location, view_more;
        TextView menu_price;
        ImageView imageView12, edit_qty,delete_item;
        RelativeLayout whole_layout;
        EditText menu_quantity;
        TextView checkout_totalprice,youpay,Taxamount,taxt_precntage,textView7;
        RelativeLayout checkout_whole;

        public ViewHolder(View v) {
            super(v);

            //  video_name = (TextView) v.findViewById(R.id.aie_order_history_orderno);
            title = (TextView) v.findViewById(R.id.textView6);
            imageView12 = (ImageView) v.findViewById(imageView2);
            menu_price = (TextView) v.findViewById(R.id.textView8);
            menu_quantity = (EditText) v.findViewById(R.id.textView9);
            edit_qty = (ImageView) v.findViewById(R.id.imageView4);
            checkout_totalprice = (TextView) activity.findViewById(R.id.total_amount);
            Taxamount = (TextView) activity.findViewById(R.id.tax_pay);
            taxt_precntage = (TextView) activity.findViewById(R.id.taxt_precntage);
            youpay=(TextView)activity.findViewById(R.id.you_pay);
            delete_item = (ImageView) v.findViewById(R.id.imageView3);
            checkout_whole=(RelativeLayout)v.findViewById(R.id.checkout_whole);
            textView7=(TextView)v.findViewById(R.id.textView7);
        }


    }

    public void postNewQunatity(Context context, String id, String quantity,final ViewHolder holder) {
        // mPostCommentResponse.requestStarted();
        showProgressDialog();
        RequestQueue queue = Volley.newRequestQueue(context);
        final String menu_id = id;
        final String menu_quantity = quantity;
        StringRequest sr = new StringRequest(Request.Method.POST, Utils.DOMAIN_NAME, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //mPostCommentResponse.requestCompleted();
                System.out.println("responsestring123---" + response.toString());
                aie_fetch_event_details(response,holder);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // mPostCommentResponse.requestEndedWithError(error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("page_name", "update_cart_quantity");
                params.put("user_id", Prefs.getString(CHOPMOBILE_CONSTANTS.shared_user_id, ""));
                params.put("menuid", menu_id);
                params.put("quantity", menu_quantity);
                //params.put("location_id",Prefs.getString(CHOPMOBILE_CONSTANTS.location_id,""));
                params.put("rest",Prefs.getString("",Prefs.getString(CHOPMOBILE_CONSTANTS.Restaurant_Id,"")));
                System.out.println("addparamscheckout---" + params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(sr);
    }

    public void aie_fetch_event_details(String jsonObject,ViewHolder holder) {
        Gson gson = new Gson();
        hideProgressDialog();
        try {
            JSONObject jobj = new JSONObject(jsonObject);
            System.out.println("josobjectfromser--"+jobj);
            if(jobj.getString("success").equals("1")) {
            System.out.println("checkout_jArray.length()" + jobj.getString("menu_total"));
            menu_price = jobj.getString("menu_total");
            overall_price = jobj.getString("total");
            System.out.println("menupricetotal--"+menu_price+overall_price);
            if (menu_price!= null && !menu_price.isEmpty()) {
                holder.menu_price.setText("$"+menu_price);
            }
            if (overall_price!= null && !overall_price.isEmpty()) {
                holder.checkout_totalprice.setText("$" +jobj.getString("mytotal").toString());
                holder.taxt_precntage.setText("Tax"+"("+jobj.getString("sales_tax").toString()+"%include alltaxes)");
                holder.Taxamount.setText("$"+jobj.getString("sales_tax_amt").toString());
                holder.youpay.setText("$"+overall_price);
            }
        }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void showProgressDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideProgressDialog() {
        if (pDialog.isShowing())
            pDialog.hide();
        pDialog.dismiss();
    }

    public void postDeleteItem(Context context, String id) {
        // mPostCommentResponse.requestStarted();
        showProgressDialog();
        RequestQueue queue = Volley.newRequestQueue(context);
        final String menu_id = id;
        StringRequest sr = new StringRequest(Request.Method.POST, Utils.DOMAIN_NAME, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //mPostCommentResponse.requestCompleted();
                System.out.println("deleteresponsestring123---" + response.toString());
                aie_fetch_delete_details(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // mPostCommentResponse.requestEndedWithError(error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("page_name", "removecart");
                params.put("user_id", Prefs.getString(CHOPMOBILE_CONSTANTS.shared_user_id, ""));
                params.put("menuid", menu_id);
                params.put("device_id",Utils.getDeviceId(activity));
                params.put("device_type","ANDROID");
               // params.put("location_id",Prefs.getString(CHOPMOBILE_CONSTANTS.location_id,""));
                params.put("rest",Prefs.getString("",Prefs.getString(CHOPMOBILE_CONSTANTS.Restaurant_Id,"")));
               // params.put("quantity", menu_quantity);

                System.out.println("addparamscheckout---" + params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(sr);
    }

    public void aie_fetch_delete_details(String jsonObject) {
        Gson gson = new Gson();
        hideProgressDialog();
        try {
            JSONObject jobj = new JSONObject(jsonObject);
            System.out.println("josobjectfromser--"+jobj);
            if(jobj.getString("success").equals("1")) {
                Intent i=new Intent(activity, Checkout.class);
                activity.startActivity(i);
                activity.finish();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
