package Adapters;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.omkarsoft.chopmobile.Favroite_List;
import com.omkarsoft.chopmobile.Product_Details;
import com.omkarsoft.chopmobile.R;
import com.pixplicity.easyprefs.library.Prefs;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import HellpersClass.CHOPMOBILE_CONSTANTS;
import HellpersClass.Utils;
import Model.FAVRIOTE;

/**
 * Created by user1 on 12-Apr-17.
 */

public class Favriote_Adapter extends RecyclerView.Adapter<Favriote_Adapter.ViewHolder> {

    Activity activity;
    private ArrayList<FAVRIOTE> speakers_arr;
    private ProgressDialog pDialog;

    // Provide a suitable constructor
    public Favriote_Adapter(Activity activity, ArrayList<FAVRIOTE> speakers_arr) {
        this.activity = activity;
        this.speakers_arr = speakers_arr;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.favriote_adapter, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        pDialog = new ProgressDialog(activity);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);

        //bacl
        holder.favrite_adpter_back.setBackgroundColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_background_color,"")));
        holder.menutype.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_text_color,"")));
        holder.menu_name.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_text_color,"")));
        holder.menu_price.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_text_color,"")));

        holder.imageView6.setTag(position);

        FAVRIOTE speakers_order = speakers_arr.get(position);
        Picasso.with(activity)
                .load(speakers_order.getMenu_image().toString())
                .resize(350, 350)
                .centerCrop()
                .into(holder.offer_image);
        holder.menutype.setText(speakers_order.getFood_type().toString());
        holder.menu_name.setText(speakers_order.getMenu_name().toString());
        holder.menu_price.setText("$" + speakers_order.getMenu_price().toString());
        onclick_listeners(holder, position);
    }


    private void onclick_listeners(final ViewHolder holder, final int position) {

        holder.imageView6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = (Integer) view.getTag();
                postDeleteItem(activity, speakers_arr.get(pos).getMenu_id());
            }
        });
        holder.offer_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(activity,Product_Details.class);
                System.out.println("productidintent---"+speakers_arr.get(position).getMenu_id());
                i.putExtra("Product_ID",speakers_arr.get(position).getMenu_id());
                i.putExtra("Product_Name",speakers_arr.get(position).getMenu_name());
                i.putExtra("Tabvalue",String.valueOf(0));
                i.putExtra("Offers_Value",String.valueOf(2));
                activity.startActivity(i);
                activity.finish();
            }
        });
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public int getItemCount() {
        return speakers_arr.size();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        ImageView offer_image;
        TextView menu_price, menu_name, menutype;
        ImageView imageView6;
        CardView favrite_adpter_back;
        public ViewHolder(View v) {
            super(v);


            offer_image = (ImageView) v.findViewById(R.id.imageView5);
            menutype = (TextView) v.findViewById(R.id.textView12);
            menu_name = (TextView) v.findViewById(R.id.textView13);
            menu_price = (TextView) v.findViewById(R.id.textView14);
            imageView6 = (ImageView) v.findViewById(R.id.imageView6);
            favrite_adpter_back=(CardView)v.findViewById(R.id.favrite_adpter_back);
        }


    }

    public void postDeleteItem(Context context, String id) {
        // mPostCommentResponse.requestStarted();
        showProgressDialog();
        RequestQueue queue = Volley.newRequestQueue(context);
        final String menu_id = id;
        StringRequest sr = new StringRequest(Request.Method.POST, Utils.DOMAIN_NAME, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //mPostCommentResponse.requestCompleted();
                System.out.println("deleteresponsestring123---" + response.toString());
                aie_fetch_delete_details(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // mPostCommentResponse.requestEndedWithError(error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("page_name", "remove_favourite");
                params.put("userid", Prefs.getString(CHOPMOBILE_CONSTANTS.shared_user_id, ""));
                params.put("menuid", menu_id);
                //params.put("location_id",Prefs.getString(CHOPMOBILE_CONSTANTS.location_id,""));
                params.put("rest",Prefs.getString("",Prefs.getString(CHOPMOBILE_CONSTANTS.Restaurant_Id,"")));
                // params.put("quantity", menu_quantity);

                System.out.println("addparamscheckout---" + params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(sr);
    }

    public void aie_fetch_delete_details(String jsonObject) {
        Gson gson = new Gson();
        hideProgressDialog();
        try {
            JSONObject jobj = new JSONObject(jsonObject);
            System.out.println("josobjectfromser--" + jobj);
            if (jobj.getString("success").equals("1")) {
                Intent i = new Intent(activity, Favroite_List.class);
                activity.startActivity(i);
                activity.finish();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void showProgressDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideProgressDialog() {
        if (pDialog.isShowing())
            pDialog.hide();
        pDialog.dismiss();
    }
}
