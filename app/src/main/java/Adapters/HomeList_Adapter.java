package Adapters;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.omkarsoft.chopmobile.Product_TabPage;
import com.omkarsoft.chopmobile.R;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.ArrayList;

import HellpersClass.CHOPMOBILE_CONSTANTS;
import Model.CATEGORY;
import other.AppController;

/**
 * Created by DELL on 27/6/2016.
 */
public class HomeList_Adapter extends RecyclerView.Adapter<HomeList_Adapter.ViewHolder> {

    Activity activity;
    ArrayList<CATEGORY> category_list;


    ViewHolder vh;
    ImageLoader.ImageContainer imageContainer;
    public HomeList_Adapter(Activity activity, ArrayList<CATEGORY> category_list) {
        // Provide a suitable constructor
        this.activity = activity;
        this.category_list = category_list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.main_adapter, parent, false);
        // set the view's size, margins, paddings and layout parameters
        vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

      ImageLoader imageLoader = AppController.getInstance().getImageLoader();



        //color changes
     //   holder.cate_name_back.setBackgroundColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_background_color,"")));
        holder.category_name.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_text_color,"")));

        // when applying a new view cancel the previous request first


//        if (imageContainer != null) {
//            imageContainer.cancelRequest();
//        }

        // calculate the max height and max width

//        imageContainer = imageLoader.get(category_list.get(position).getImage().toString(),
//                new DefaultImageListener(holder.root_lay), maxWidth, maxHeight);

        holder.root_lay.setImageUrl(category_list.get(position).getImage(), imageLoader);

        holder.category_name.setText(category_list.get(position).getCategory_name());

        onclick_listeners(vh, position);
    }

    private void onclick_listeners(final ViewHolder holder, final int position) {

        holder.category_whole.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(activity, Product_TabPage.class);
                i.putExtra("POSITION_VALUE", String.valueOf(position));
                System.out.println("positionvalue" + position);
                activity.startActivity(i);
//                activity.finish();

            }
        });

    }

    @Override
    public int getItemCount() {
        return category_list.size();
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case

        NetworkImageView root_lay;
        TextView category_name;
        RelativeLayout cate_name_back,category_whole;
        public ViewHolder(View v) {
            super(v);

            //  video_name = (TextView) v.findViewById(R.id.aie_order_history_orderno);
            root_lay = (NetworkImageView) v.findViewById(R.id.image_category);
            category_name = (TextView) v.findViewById(R.id.category_name);
//            cate_name_back=(RelativeLayout)v.findViewById(R.id.cate_name_back);
            category_whole=(RelativeLayout)v.findViewById(R.id.category_whole);

        }


    }

    private class DefaultImageListener implements ImageLoader.ImageListener {
        private ImageView imageView;

        public DefaultImageListener(ImageView view) {
            imageView = view;
        }

        @Override
        public void onErrorResponse(VolleyError error) {
            //handle errors
        }

        @Override
        public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
            if (response.getBitmap() != null) {
                imageView.setImageBitmap(response.getBitmap());
            }
        }
    }

}
