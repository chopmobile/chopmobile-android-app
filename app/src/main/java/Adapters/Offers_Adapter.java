package Adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.omkarsoft.chopmobile.Product_Details;
import com.omkarsoft.chopmobile.R;

import java.util.ArrayList;

import Model.OFFERS;
import other.AppController;

/**
 * Created by user1 on 12-Apr-17.
 */

public class Offers_Adapter extends RecyclerView.Adapter<Offers_Adapter.ViewHolder> {

    Activity activity;
    private ArrayList<OFFERS> speakers_arr;

    // Provide a suitable constructor
    public Offers_Adapter(Activity activity, ArrayList<OFFERS> speakers_arr) {
        this.activity = activity;
        this.speakers_arr = speakers_arr;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.offers_adapter, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element


        OFFERS speakers_order = speakers_arr.get(position);

        ImageLoader imageLoader = AppController.getInstance().getImageLoader();
        holder.offer_image.setImageUrl(speakers_order.getImages().toString(),imageLoader);
        onclick_listeners(holder,position);
    }


    private void onclick_listeners(final ViewHolder holder, final int position) {

        holder.offer_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(activity,Product_Details.class);
                System.out.println("productidintent---"+speakers_arr.get(position).getProduct_id());
                i.putExtra("Product_ID",speakers_arr.get(position).getProduct_id());
                i.putExtra("Product_Name",speakers_arr.get(position).getMenu_name());
                i.putExtra("Tabvalue",String.valueOf(0));
                i.putExtra("Offers_Value",String.valueOf(1));
                activity.startActivity(i);
                activity.finish();
            }
        });
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }
    @Override
    public int getItemCount() {
        return speakers_arr.size();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        NetworkImageView offer_image;

        public ViewHolder(View v) {
            super(v);


            offer_image = (NetworkImageView) v.findViewById(R.id.image_promo);

        }


    }
}
