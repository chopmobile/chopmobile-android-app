package Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.omkarsoft.chopmobile.HomeActivity;
import com.omkarsoft.chopmobile.R;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import HellpersClass.CHOPMOBILE_CONSTANTS;
import HellpersClass.Utils;
import Model.SELECT_RESTURANT;

/**
 * Created by user1 on 12-Apr-17.
 */

public class Restaurant_Adapter extends RecyclerView.Adapter<Restaurant_Adapter.ViewHolder> {


    Activity activity;
    private ArrayList<SELECT_RESTURANT> speakers_arr;
    int selectedPosition = 0;
    RadioButton selected = null;

    // Provide a suitable constructor
    public Restaurant_Adapter(Activity activity, ArrayList<SELECT_RESTURANT> speakers_arr) {
        this.activity = activity;
        this.speakers_arr = speakers_arr;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.select_restaurant_adapter, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element


        SELECT_RESTURANT speakers_order = speakers_arr.get(position);

        //background color\
        holder.resturant_backgound.setBackgroundColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_background_color,"")));
        holder.order_id.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_text_color,"")));
        ColorStateList colorStateList = new ColorStateList(
                new int[][]{

                        new int[]{-android.R.attr.state_enabled}, //disabled
                        new int[]{android.R.attr.state_enabled} //enabled
                },
                new int[] {

                        Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_text_color,"")) //disabled
                        ,Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_text_color,"")) //enabled

                }
        );
        holder.order_id.setButtonTintList(colorStateList);

        System.out.println("arraylengthvalue" + speakers_order.getId());
        holder.order_id.setText(speakers_order.getStreet_address1() + "\n" + speakers_order.getState() + "\n" + speakers_order.getCity() + "-" + speakers_order.getPincode());

//        selectedPosition=Integer.getInteger(Prefs.getString(CHOPMOBILE_CONSTANTS.location_id,"0"));

//        if (selectedPosition == position) {
//            holder.order_id.setChecked(true);
//        }else {
//            holder.order_id.setChecked(true);
////            if(mSelectedRB != null && h.radio != mSelectedRB){
////                mSelectedRB = h.radio;
////            }
//        }
        System.out.println("sharevalue---"+Prefs.getString(CHOPMOBILE_CONSTANTS.location_id,""));
        System.out.println("ararayposition---"+speakers_order.getId());
        if(Prefs.getString(CHOPMOBILE_CONSTANTS.use_location_future,"").equals("1")) {
            holder.use_futute.setChecked(true);
            if (Integer.parseInt(Prefs.getString(CHOPMOBILE_CONSTANTS.location_id, "0")) == Integer.parseInt(speakers_arr.get(position).getId())) {
                holder.order_id.setChecked(true);
            }
        }
        onclick_listeners(holder, position);
    }


    private void onclick_listeners(final ViewHolder holder, final int position) {


                if(Integer.parseInt(Prefs.getString(CHOPMOBILE_CONSTANTS.location_id,"0"))==Integer.parseInt(speakers_arr.get(position).getId())){
                    holder.order_id.setChecked(true);
                }
                else {
                    holder.order_id.setChecked(false);
                }
        holder.order_id.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isChecked = holder.order_id.isChecked();

                System.out.println("selectedlocatioid--" + speakers_arr.get(position).getId());
                Prefs.putString(CHOPMOBILE_CONSTANTS.location_id,speakers_arr.get(position).getId());
                if (selected != null) {
                    selected.setChecked(false);
                }

                holder.order_id.setChecked(true);
                selected = holder.order_id;

        notifyDataSetChanged();

            }
        });

        holder.submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("locationidvalue----"+Prefs.getString(CHOPMOBILE_CONSTANTS.location_id,""));
                if(!Prefs.getString(CHOPMOBILE_CONSTANTS.location_id,"").equals("0")&&!Prefs.getString(CHOPMOBILE_CONSTANTS.location_id,"").isEmpty()&&Prefs.getString(CHOPMOBILE_CONSTANTS.location_id,"")!=null) {
                    if(holder.use_futute.isChecked()){
                        Prefs.putString(CHOPMOBILE_CONSTANTS.use_location_future,"1");
                    }
                    else {
                        Prefs.putString(CHOPMOBILE_CONSTANTS.use_location_future,"0");
                    }

                    Intent i = new Intent(activity, HomeActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    activity.startActivity(i);
                    activity.finish();
//                    Restaurant_Status(activity);
                }
                else {
                    Toast.makeText(activity,"Please select Restaurant Location",Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    @Override
    public int getItemCount() {
        return speakers_arr.size();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        TextView Order_date, order_type, delivery_address, pickup_status, pickup_time;
        RelativeLayout whole_relative;
        Button submit;
        AppCompatRadioButton order_id;
        CheckBox use_futute;
        LinearLayout resturant_backgound;
        public ViewHolder(View v) {
            super(v);
            order_id = (AppCompatRadioButton) v.findViewById(R.id.radio_button);
            submit = (Button) activity.findViewById(R.id.rest_submit);
            use_futute=(CheckBox)activity.findViewById(R.id.radiogroup);
            resturant_backgound=(LinearLayout)v.findViewById(R.id.resturant_backgound);
        }


    }

    public void Restaurant_Status(Context context) {
        // mPostCommentResponse.requestStarted();
//        showProgressDialog();
        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest sr = new StringRequest(Request.Method.POST, Utils.DOMAIN_NAME, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //mPostCommentResponse.requestCompleted();
                System.out.println("deleteresponsestring123---" + response.toString());
                aie_fetch_speakers_details(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // mPostCommentResponse.requestEndedWithError(error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("page_name", "restaurant_timing");
                params.put("location_id",Prefs.getString(CHOPMOBILE_CONSTANTS.location_id,""));
                Calendar cal = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
                System.out.println("currenttime---"+sdf.format(cal.getTime()).toString() );

                params.put("time",sdf.format(cal.getTime()));
                params.put("rest",Prefs.getString("",Prefs.getString(CHOPMOBILE_CONSTANTS.Restaurant_Id,"")));
                // params.put("quantity", menu_quantity);

                System.out.println("stripeaddparamscheckout---" + params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(sr);
    }

    private void aie_fetch_speakers_details(String jsonObject) {
        Gson gson = new Gson();
        try {
            JSONObject jobj = new JSONObject(jsonObject);
//            hideProgressDialog();
            System.out.println("urlresponseinmmind---"+(jobj.getString("success").equals("1"))+","+(jobj.getString("rest_open_close").equals("1")));
            Prefs.putString(CHOPMOBILE_CONSTANTS.rest_open_status,jobj.getString("rest_open_close"));
            if((jobj.getString("success").equals("1"))&&(jobj.getString("rest_open_close").equals("1"))) {
                    Intent i = new Intent(activity, HomeActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    activity.startActivity(i);
                    activity.finish();
            }
            else {
                System.out.println("cuming inside else part");
                LayoutInflater factory = LayoutInflater.from(activity);
                final View deleteDialogView = factory.inflate(R.layout.restaurant_close, null);
                final AlertDialog deleteDialog = new AlertDialog.Builder(activity).create();
                deleteDialog.setView(deleteDialogView);

                TextView text = (TextView) deleteDialogView.findViewById(R.id.text_dialog);
                text.setText(jobj.getString("rest_open_close_msg").toString()+"\n");
                TextView text1 = (TextView) deleteDialogView.findViewById(R.id.time);
                text1.setText("\t Restuarant close Time "+jobj.getString("rest_timing_to").toString());
                TextView text2 = (TextView) deleteDialogView.findViewById(R.id.open_time);
                text2.setText("Resturant Open Time "+jobj.getString("rest_timing_from").toString());

                Button dialogButton = (Button) deleteDialogView.findViewById(R.id.btn_dialog);
                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        deleteDialog.dismiss();
                        activity.finish();
                    }
                });


                deleteDialog.show();


//                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
//                builder.setMessage(jobj.getString("rest_open_close_msg").toString()+"\n"+"Resturant Open Time"+jobj.getString("rest_timing_from").toString()+"\t Restuarant close Time"+jobj.getString("rest_timing_to").toString())
//                        .setCancelable(false)
//                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int id) {
//
//                                dialog.cancel();
//                            }
//                        })
//                        //Set your icon here
//                        .setTitle("Island Soul")
//                        .setIcon(R.drawable.ic_launcher);
//                AlertDialog alert = builder.create();
//                alert.show();//showing the dialog

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
