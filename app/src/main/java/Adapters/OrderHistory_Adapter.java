package Adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.omkarsoft.chopmobile.Checkout;
import com.omkarsoft.chopmobile.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import Model.ORDER_HISTORY;

/**
 * Created by user1 on 12-Apr-17.
 */

public class OrderHistory_Adapter extends RecyclerView.Adapter<OrderHistory_Adapter.ViewHolder> {

    Activity activity;
    private ArrayList<ORDER_HISTORY> speakers_arr;

    // Provide a suitable constructor
    public OrderHistory_Adapter(Activity activity, ArrayList<ORDER_HISTORY> speakers_arr) {
        this.activity = activity;
        this.speakers_arr = speakers_arr;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_history_adapter, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element


        ORDER_HISTORY speakers_order = speakers_arr.get(position);

//        holder.reorder.setBackgroundColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_background_color,"")));
//        holder.reorder.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_text_color,"")));
//        holder.history_back_adapter.setBackgroundColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_background_color,"")));
//        holder.order_id.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_text_color,"")));
//        holder.order_type.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_text_color,"")));
//        holder.delivery_address.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_text_color,"")));
//        holder.Order_date.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_text_color,"")));
//        holder.order_id.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_text_color,"")));
//        holder.pickup_status.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_text_color,"")));
//        holder.pickup_text.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_text_color,"")));
//        holder.pickup_time.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_text_color,"")));
//        holder.pickup_text1.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_text_color,"")));

//        GradientDrawable gd = new GradientDrawable();
//        gd.setColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_background_color,""))); // Changes this drawbale to use a single color instead of a gradient
//        gd.setCornerRadius(5);
//        gd.setStroke(5,Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_text_color,"")));
//
//        holder.reorder.setBackgroundDrawable(gd);

//        holder.order_id.setTextColor();
//        holder.order_type.setText();
//        holder.delivery_address.setText();
//        holder.Order_date.setText();

        System.out.println("orderidprint---"+speakers_order.getOrder_id());
        holder.order_id.setText("Order Id: "+speakers_order.getOrder_id());
        holder.order_type.setText("Delivery Time: "+speakers_order.getPickup_time());
        holder.delivery_address.setText("TotalPrice: $"+speakers_order.getPrice());
        holder.order_status.setText("Order Status: "+speakers_order.getPickup_status());

        try {
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date date = (Date) format.parse(speakers_order.getOrder_date().substring(0,10).toString());
            holder.Order_date.setText("Order Date: "+new SimpleDateFormat("MMMM dd yyyy").format(date));
        }
        catch (Exception e){
            e.printStackTrace();
        }
//        holder.Order_date.setText("Date: "+speakers_order.getOrder_date().substring(0,10));

//        final int N = 10; // total number of textviews to add

        final TextView[] myTextViews = new TextView[speakers_order.getProducts().size()]; // create an empty array;

//        System.out.println("Menuname--"+speakers_order.getProducts().get(position).getMenu_name());
        System.out.println("Menunamesize--"+speakers_order.getProducts().size());


        for (int i = 0; i < speakers_order.getProducts().size(); i++) {
            // create a new textview
            final TextView rowTextView = new TextView(activity);

            // set some properties of rowTextView or something
            rowTextView.setText(speakers_order.getProducts().get(i).getMenuitem_quantity()+"\t\t" + speakers_order.getProducts().get(i).getMenu_name());

            // add the textview to the linearlayout
            holder.linear.addView(rowTextView);

            // save a reference to the textview for later
            myTextViews[i] = rowTextView;
        }
        speakers_order.getProducts().clear();
//        holder.product_name.setText(speakers_order.getProducts().get(position).getMenu_name());
//        holder.product_quantity_text.setText(speakers_order.getProducts().get(position).getMenu_total());

//        if(speakers_order.getPickup_status().toString()!=null&&!speakers_order.getPickup_status().toString().isEmpty()) {
//            if (speakers_order.getPickup_status().equals("Pending")) {
//                holder.pickup_status.setText(speakers_order.getPickup_status().toString());
////                holder.pickup_status.setTextColor((Color.parseColor("#FF0000")));
//            } else if (speakers_order.getPickup_status().equals("Ready")) {
//                holder.pickup_status.setText(speakers_order.getPickup_status().toString());
////                holder.pickup_status.setTextColor((Color.parseColor("#008000")));
//            }
//            else if (speakers_order.getPickup_status().equals("Completed")) {
//                holder.pickup_status.setText(speakers_order.getPickup_status().toString());
////                holder.pickup_status.setTextColor((Color.parseColor("#008000")));
//            }
//        }

//        if(speakers_order.getPickup_time().toString()!=null&&!speakers_order.getPickup_time().toString().isEmpty()) {
//            if(speakers_order.getPickup_status().equals("Pending")) {
//                holder.pickup_time.setText(speakers_order.getPickup_time().toString());
////                holder.pickup_time.setTextColor((Color.parseColor("#FF0000")));
//            }
//            else if(speakers_order.getPickup_status().equals("Ready")){
//                holder.pickup_time.setText(speakers_order.getPickup_time().toString());
////                holder.pickup_time.setTextColor((Color.parseColor("#008000")));
//            }
//            else if(speakers_order.getPickup_status().equals("Completed")){
//                holder.pickup_time.setText(speakers_order.getPickup_time().toString());
////                holder.pickup_time.setTextColor((Color.parseColor("#008000")));
//            }
//        }
        onclick_listeners(holder,position);
    }


    private void onclick_listeners(final ViewHolder holder, final int position) {

        holder.reorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i =new Intent(activity, Checkout.class);
                i.putExtra("ORDER_ID",speakers_arr.get(position).getOrder_id());
                activity.startActivity(i);
            }
        });
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }
    @Override
    public int getItemCount() {
        return speakers_arr.size();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        TextView order_id, Order_date,order_type,delivery_address,pickup_status,pickup_time,pickup_text,pickup_text1,product_quantity_text,product_name,order_status;
        RelativeLayout whole_relative;
        Button reorder;
        CardView history_back_adapter;
        LinearLayout linear;
        public ViewHolder(View v) {
            super(v);

            history_back_adapter= (CardView) v.findViewById(R.id.history_back_adapter);
            order_id = (TextView) v.findViewById(R.id.order_id);
            Order_date = (TextView) v.findViewById(R.id.order_type);
            order_type= (TextView) v.findViewById(R.id.order_date);
            delivery_address=(TextView) v.findViewById(R.id.order_address);
            reorder=(Button)v.findViewById(R.id.reorder);
            product_quantity_text= (TextView) v.findViewById(R.id.order_date);
            product_name=(TextView) v.findViewById(R.id.order_date);
            linear=(LinearLayout) v.findViewById(R.id.linear);
            order_status=(TextView) v.findViewById(R.id.order_status);


//            pickup_status=(TextView) v.findViewById(R.id.textView10);
//            pickup_text=(TextView) v.findViewById(R.id.pickup_stauts);
//            pickup_time=(TextView) v.findViewById(R.id.pickup_time1);
//            pickup_text1=(TextView) v.findViewById(R.id.pickup_time);
        }


    }
}
