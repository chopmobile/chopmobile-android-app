package com.omkarsoft.chopmobile;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Toast;

import com.pixplicity.easyprefs.library.Prefs;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import HellpersClass.CHOPMOBILE_CONSTANTS;
import HellpersClass.CONNECTIONDETECTOR;
import HellpersClass.Utils;


/**
 * Created by user1 on 2/3/2017.
 */

public class RegisterActivity extends AppCompatActivity implements ActivityCompat.OnRequestPermissionsResultCallback {

    AutoCompleteTextView auto_mail;
    private static final Pattern EMAIL_PATTERN = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$", Pattern.CASE_INSENSITIVE);
    String EMAIL, PASSWORD, NAME, MOBILENO, LASTNAME, LOCATION, PINCODE, ADDRESS, USER_STATUS;
    //  TextView _textview_reg;
    EditText First_Name, Email, Password, Mobile_Number, LastName, Address, Pincode;
    CONNECTIONDETECTOR cd;
    Boolean isInternetPresent = false;
    String login_url;
    private ProgressDialog pDialog;
    Button register_button;
    String jsonUser_id, jsonName, jsonEmail, jsonSkypeid, jsonMobile, jsonPassword;
    String result11, json_result;
    String mPhoneNumber;
    ScrollView login_back_image;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_register);


        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);

        cd = new CONNECTIONDETECTOR(getApplicationContext());

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
//        _textview_reg = (TextView) findViewById(R.id.text_reg);
//        _textview_reg.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent _intent_login = new Intent(getApplication(), LoginActivity.class);
//                startActivity(_intent_login);
//            }
//        });

        auto_mail = (AutoCompleteTextView) findViewById(R.id.autoemail);
        addAdapterToViews();

        First_Name = (EditText) findViewById(R.id.edit1_reg);
        Email = (EditText) findViewById(R.id.autoemail);
        Password = (EditText) findViewById(R.id.edit3_reg);
        Mobile_Number = (EditText) findViewById(R.id.edit4_reg);
        LastName = (EditText) findViewById(R.id.edit9_reg);
//        Address = (EditText) findViewById(R.id.edit5_reg);
      //  Pincode = (EditText) findViewById(R.id.edit7_reg);
//        Location = (EditText) findViewById(R.id.edit6_reg);
//        Userstatus = (EditText) findViewById(R.id.edit8_reg);
        register_button = (Button) findViewById(R.id.register_button);
        login_back_image= (ScrollView) findViewById(R.id.login_back_image);

//        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
//            TelephonyManager tMgr = (TelephonyManager) RegisterActivity.this.getSystemService(Context.TELEPHONY_SERVICE);
//            mPhoneNumber = tMgr.getLine1Number();
//            System.out.println("mobilenofetch" + tMgr);
//            if (!mPhoneNumber.isEmpty() && mPhoneNumber != null) {
//                System.out.println();
//                Mobile_Number.setText(mPhoneNumber);
//            }
//        }

        //background colr change
        login_back_image.setBackgroundColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.background_app_color,"")));
        First_Name.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")));
        Email.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")));
        Password.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")));
        Mobile_Number.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")));
        LastName.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")));
        register_button.setBackgroundColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_background_color,"")));
        register_button.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_text_color,"")));


        register_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EMAIL = Email.getText().toString().trim();
                PASSWORD = Password.getText().toString().trim();
                NAME = First_Name.getText().toString().trim();
//                TelephonyManager tMgr = (TelephonyManager)RegisterActivity.this.getSystemService(Context.TELEPHONY_SERVICE);
//                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
//                    String mPhoneNumber = tMgr.getLine1Number();
//                    System.out.println("mobilenofetch" + tMgr);
//                    if (!mPhoneNumber.isEmpty() && mPhoneNumber != null) {
//                        System.out.println();
//                        MOBILENO = mPhoneNumber;
//                    }
//                }
//                else {
                MOBILENO = Mobile_Number.getText().toString().trim();

                LASTNAME = LastName.getText().toString().trim();
//                LOCATION = Location.getText().toString().trim();
//                ADDRESS = Address.getText().toString().trim();
//                PINCODE = Pincode.getText().toString().trim();
                // USER_STATUS=Userstatus.getText().toString().trim();
                if (NAME.equals("")) {
                    Toast.makeText(getApplicationContext(), "Name is Required", Toast.LENGTH_SHORT).show();

                } else if (MOBILENO.equals("")) {
                    Toast.makeText(getApplicationContext(), "Mobile Number is Required", Toast.LENGTH_SHORT).show();

                } else if (EMAIL.equals("")) {
                    Toast.makeText(getApplicationContext(), "Email ID is Required", Toast.LENGTH_SHORT).show();
                } else if (PASSWORD.equals("")) {
                    Toast.makeText(getApplicationContext(), "Password is Required", Toast.LENGTH_SHORT).show();
                } else if (!emailValidator(EMAIL)) {
                    Toast.makeText(getApplicationContext(), "Email Id Invalid", Toast.LENGTH_SHORT).show();
                }
//                else if (ADDRESS.equals("")) {
//                    Toast.makeText(getApplicationContext(), "Address is Required", Toast.LENGTH_SHORT).show();
//                }
//                else if (Location.equals("")) {
//                    Toast.makeText(getApplicationContext(), "Location is Required", Toast.LENGTH_SHORT).show();
//                }
//                else if (PINCODE.equals("")) {
//                    Toast.makeText(getApplicationContext(), "Pincode is Required", Toast.LENGTH_SHORT).show();
//                }
                else if (LASTNAME.equals("")) {
                    Toast.makeText(getApplicationContext(), "Lastname is Required", Toast.LENGTH_SHORT).show();
                } else {
                    isInternetPresent = cd.isConnectingToInternet();
                    // check for Internet status
                    if (isInternetPresent) {
                        // Internet Connection is Present
                        // make HTTP requests
                        showProgressDialog();
                        new Login_user().execute();
                    } else {
                        // Internet connection is not present
                        // Ask user to connect to Internet
//                        showAlertDialog(AndroidDetectInternetConnectionActivity.this, "No Internet Connection",
//                                "You don't have internet connection.", false);
                        AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
                        builder.setMessage("No Internet Connection Please Check Your Internet connection.")
                                .setCancelable(false)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {

                                        dialog.cancel();
                                    }
                                })
                                //Set your icon here
                                .setTitle("ChopMobile")
                                .setIcon(R.drawable.ic_launcher);
                        AlertDialog alert = builder.create();
                        alert.show();//showing the dialog
                    }
                }

            }
        });

    }

    private void addAdapterToViews() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.GET_ACCOUNTS) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Account[] accounts = AccountManager.get(this).getAccounts();
        Set<String> emailSet = new HashSet<String>();
        for (Account account : accounts) {
            if (EMAIL_PATTERN.matcher(account.name).matches()) {
                emailSet.add(account.name);
            }
        }
        auto_mail.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, new ArrayList<String>(emailSet)));

    }

    public boolean emailValidator(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    private class Login_user extends AsyncTask<String, Void, String> {

        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... URL) {

            try {

                login_url = Utils.DOMAIN_NAME;
                HttpClient httpclient = new DefaultHttpClient();
                System.out.println("post url:-" + login_url);
                HttpPost httppost = new HttpPost(login_url);
                System.out.println("post namevaluepair name" + EMAIL);
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                nameValuePairs.add(new BasicNameValuePair("page_name", "register"));
                nameValuePairs.add(new BasicNameValuePair("email", EMAIL));
                nameValuePairs.add(new BasicNameValuePair("password", PASSWORD));
                nameValuePairs.add(new BasicNameValuePair("first_name", NAME));
                nameValuePairs.add(new BasicNameValuePair("contact", MOBILENO));
//                nameValuePairs.add(new BasicNameValuePair("address", ADDRESS));
                nameValuePairs.add(new BasicNameValuePair("location", LOCATION));
//                nameValuePairs.add(new BasicNameValuePair("pincode", PINCODE));
                nameValuePairs.add(new BasicNameValuePair("last_name", LASTNAME));
                nameValuePairs.add(new BasicNameValuePair(" user_status", "0"));
                nameValuePairs.add(new BasicNameValuePair("device", "ANDROID"));
                nameValuePairs.add(new BasicNameValuePair("device_id", Utils.getDeviceId(RegisterActivity.this)));
                nameValuePairs.add(new BasicNameValuePair("rest",Prefs.getString(CHOPMOBILE_CONSTANTS.Restaurant_Id,"")));

                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                System.out.println("post all namevalues" + nameValuePairs);

                // Execute HTTP Post Request
                org.apache.http.HttpResponse response = httpclient.execute(httppost);

                BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                sb.append(reader.readLine() + "\n");
                String line = "0";
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                reader.close();
                result11 = sb.toString();
                System.out.println("post result19:-" + result11);
                // parsing data

            } catch (Exception e) {
                e.printStackTrace();

            }
            return result11;
        }

        protected void onPostExecute(String getResult) {
            super.onPostExecute(getResult);
            System.out.println("logininside post");
            System.out.println("login post result:-" + getResult);
            if (getResult != null) {
                JSONObject jObject = null;
                try {
                    jObject = new JSONObject(getResult);
                    System.out.println("loginjson object" + jObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    json_result = jObject.getString("success");
                    System.out.println("loginjson reg" + json_result);
                    if (json_result.equals("1")) {
                        System.out.println("inside json success");
                        hideProgressDialog();

//                        System.out.println("jstring--"+jObject.getString("userId"));
//                        jsonUser_id = jObject.getString("userId");
//                        jsonName = jObject.getString("userName");
//                        System.out.println( "inside success"+jsonUser_id);

                        Toast.makeText(RegisterActivity.this, jObject.getString("message").toString(), Toast.LENGTH_SHORT).show();
//                        Prefs.putString(INKTALK_APP_CONSTANT.shared_user_id, jsonUser_id);
//                        Prefs.putString(INKTALK_APP_CONSTANT.shared_user_name, jsonName);

                        Prefs.putString(CHOPMOBILE_CONSTANTS.shared_user_id, jObject.getString("user_id"));
                        Prefs.putString(CHOPMOBILE_CONSTANTS.profile_full_name, jObject.getString("full_name"));
                        Prefs.putString(CHOPMOBILE_CONSTANTS.profile_email, jObject.getString("email"));
                        Intent home_int = new Intent(RegisterActivity.this, HomeActivity.class);
                        home_int.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(home_int);

                        finish();

                    } else if (json_result.equals("0")) {
                        hideProgressDialog();
                        Toast.makeText(getApplicationContext(), jObject.getString("message").toString(), Toast.LENGTH_SHORT).show();
                    } else {
                        hideProgressDialog();
                        Toast.makeText(getApplicationContext(), "Sorry! There is a Server Issue. please Try After Sometime", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } else {
                hideProgressDialog();
                System.out.println("inside no result");
                System.out.println("no result details:" + getResult);
                AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
                builder.setMessage("Somethings went wrong please try after Sometime")
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                dialog.cancel();
                            }
                        })
                        //Set your icon here
                        .setTitle("ChopMobile")
                        .setIcon(R.drawable.ic_launcher);
                AlertDialog alert = builder.create();
                alert.show();//showing the dialog
            }
        }
    }

    private void showProgressDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideProgressDialog() {
        if (pDialog.isShowing())
            pDialog.hide();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        pDialog.dismiss();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
