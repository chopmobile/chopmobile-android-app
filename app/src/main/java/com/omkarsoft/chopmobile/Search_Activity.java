package com.omkarsoft.chopmobile;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import Adapters.Search_Adapter;
import HellpersClass.CHOPMOBILE_CONSTANTS;
import HellpersClass.Utils;
import Model.PRODUCT_DETAILS;
import other.AppController;

/**
 * Created by user1 on 7/25/2016.
 */
public class Search_Activity extends Activity {
    ImageView search;
    EditText edit_search;
    String SEARCH_TEXT;
    RecyclerView recyclerView;
    ArrayList<PRODUCT_DETAILS> video_list = new ArrayList<PRODUCT_DETAILS>();
    Search_Adapter redesadapter;
    FloatingActionButton fab;

    private ProgressDialog pDialog;
    private String TAG = Search_Activity.class.getSimpleName();
    private Button btnJsonObj, btnJsonArray;
    private TextView msgResponse;
    JSONArray odr_his_jArray;
    // These tags will be used to cancel the requests
    private String tag_json_obj = "jobj_req", tag_json_arry = "jarray_req";
    ImageView abtus_back_imgvw;
    private Tracker mTracker;
    TextView not_found;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_layout);


        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);

        not_found=(TextView)findViewById(R.id.not_found);
        abtus_back_imgvw = (ImageView) findViewById(R.id.abtus_back_imgvw);

        abtus_back_imgvw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.serch_recycle);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        search = (ImageView) findViewById(R.id.imageView2);
        edit_search = (EditText) findViewById(R.id.actionbar_text);

        edit_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                SEARCH_TEXT = edit_search.getText().toString();
                if(SEARCH_TEXT.length()>3) {
                    makeJsonObjReq();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        edit_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                View view = Search_Activity.this.getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    SEARCH_TEXT = edit_search.getText().toString();
                    makeJsonObjReq();
                    return true;
                }
                return false;
            }
        });

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View view1 = Search_Activity.this.getCurrentFocus();
                if (view1 != null) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view1.getWindowToken(), 0);
                }
                SEARCH_TEXT = edit_search.getText().toString();
                makeJsonObjReq();

            }
        });


    }

    /**
     * Making json object request
     */
    private void makeJsonObjReq() {
        showProgressDialog();
        //+"&location_id="+ Prefs.getString(CHOPMOBILE_CONSTANTS.location_id,"")
        String url = Utils.DOMAIN_NAME  + "?page_name=searchmenu&keyword=" + SEARCH_TEXT+"&rest="+ Prefs.getString(CHOPMOBILE_CONSTANTS.Restaurant_Id,"");
        System.out.println("search_url" + url);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());
                        System.out.println("jsonresponssevideo" + response.toString());
                        aie_fetch_address_details(response);

                        //msgResponse.setText(response.toString());
                        hideProgressDialog();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                System.out.println("inside error" + error.getMessage());
                hideProgressDialog();
            }
        }) {

            /**
             * Passing some request headers
             * */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                return headers;
            }


        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq,
                tag_json_obj);

        // Cancelling request
        // ApplicationController.getInstance().getRequestQueue().cancelAll(tag_json_obj);
    }

    private void aie_fetch_address_details(JSONObject jsonObject) {
        Gson gson = new Gson();
        try {

            if(jsonObject.getString("success").equals("1")) {
                if (!jsonObject.getString("allmenus").equals("")) {
                    System.out.println("jsonvaluearraya" + jsonObject.getString("allmenus"));
                    odr_his_jArray = jsonObject.getJSONArray("allmenus");
                    System.out.println("video_jArray.length()" + odr_his_jArray.length());
                    video_list.clear();
                    for (int histlistidx = 0; histlistidx < odr_his_jArray.length(); histlistidx++) {
                        this.video_list.add((PRODUCT_DETAILS) gson.fromJson(this.odr_his_jArray.getJSONObject(histlistidx).toString(), PRODUCT_DETAILS.class));
                    }
                        not_found.setVisibility(View.GONE);
                        redesadapter = new Search_Adapter(Search_Activity.this, video_list);

                        recyclerView.setAdapter(redesadapter);

                } else {
                    video_list.clear();
                    redesadapter.notifyDataSetChanged();
                }
            }
            else {
                video_list.clear();
                not_found.setVisibility(View.VISIBLE);
                not_found.setText("Products not Found");

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
        pDialog.dismiss();
    }
    private void showProgressDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideProgressDialog() {
        if (pDialog.isShowing())
            pDialog.hide();
    }

}
