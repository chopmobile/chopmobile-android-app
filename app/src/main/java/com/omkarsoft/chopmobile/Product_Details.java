package com.omkarsoft.chopmobile;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.pixplicity.easyprefs.library.Prefs;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Adapters.Addon_Adapter;
import HellpersClass.CHOPMOBILE_CONSTANTS;
import HellpersClass.CheckBoxGroupView;
import HellpersClass.Utils;
import Model.PRODUCT_DETAILS;
import Model.Prouduct_Addons;
import Model.SubProduct;
import other.AppController;

import static com.omkarsoft.chopmobile.HomeActivity.navItemIndex;

/**
 * Created by user1 on 01-Mar-17.
 */

public class Product_Details extends AppCompatActivity {
    String PROUCT_ID;
    JSONArray odr_his_jArray;
    ArrayList<Prouduct_Addons> product_addons = new ArrayList<Prouduct_Addons>();
    PRODUCT_DETAILS product_det;
    CheckBoxGroupView checkbox1;
    ImageView product_image, fav_image;
    TextView product_description, product_price, count, cart_qty, OfferPrice;
    ImageView plus, minus;
    SubProduct product;
    RecyclerView catagory_list;
    LinearLayoutManager mLayoutManager;
    Addon_Adapter redesadapter;
    private ProgressDialog pDialog;
    public String PRODUCT_QUANTITY = "null";
    String result11, json_result;
    String PAGE_NAME;
    ArrayList<String> selectedStrings = new ArrayList<String>();
    ImageView check_out;
    Boolean add_fave = true;
     private static int Tab_Value =0;
            int offer_value;
    MenuItem textView;
    RelativeLayout details_main_back,details_back_down,layout3;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_details);

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);

        Bundle extras = getIntent().getExtras();

        if (extras != null) {
                Tab_Value = Integer.parseInt(extras.getString("Tabvalue"));
                // and get whatever type user account id is
                System.out.println("tabvalueintTab_Value---" + Tab_Value);
                if(extras.getString("Offers_Value")!=null) {
                    offer_value = Integer.parseInt(extras.getString("Offers_Value"));
                }
    }

        product_image = (ImageView) findViewById(R.id.product_image);
        product_description = (TextView) findViewById(R.id.description);
        product_price = (TextView) findViewById(R.id.price);
        plus = (ImageView) findViewById(R.id.textView3);
        minus = (ImageView) findViewById(R.id.textView);
        count = (TextView) findViewById(R.id.textView2);
        cart_qty = (TextView) findViewById(R.id.textView5);
        check_out = (ImageView) findViewById(R.id.cart_icon);
        fav_image = (ImageView) findViewById(R.id.fav_image);
        OfferPrice = (TextView) findViewById(R.id.textView11);
        details_main_back=(RelativeLayout) findViewById(R.id.details_main_back);
        details_back_down=(RelativeLayout) findViewById(R.id.details_back_down);
        layout3=(RelativeLayout) findViewById(R.id.layout3);
        //background colr
        details_main_back.setBackgroundColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.background_app_color,"")));
        details_back_down.setBackgroundColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.actionbar_color,"")));
        layout3.setBackgroundColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.background_app_color,"")));
        product_description.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")));

//        Drawable myIcon1 = getResources().getDrawable( R.drawable.ic_favorite_white_18dp);
//            ColorFilter filter1 = new LightingColorFilter( Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_background_color,"")),  Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_background_color,"")) );
//            myIcon1.setColorFilter(filter1);
//            fav_image.setImageDrawable(myIcon1);


        catagory_list = (RecyclerView) findViewById(R.id.checkbox1);
        mLayoutManager = new LinearLayoutManager(Product_Details.this);
        catagory_list.setLayoutManager(mLayoutManager);


        product_det = new PRODUCT_DETAILS();

        product = new SubProduct();


        PROUCT_ID = getIntent().getStringExtra("Product_ID");


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.actionbar_color,""))));
       // if(!getIntent().getStringExtra("Product_Name").toString().equals(null)&&!getIntent().getStringExtra("Product_Name").toString().isEmpty()) {
            getSupportActionBar().setTitle(getIntent().getStringExtra("Product_Name"));
//        }
//        else {
//            getSupportActionBar().setTitle("ProductDetails");
//        }

        plus.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                PAGE_NAME = "menu_with_addon_addcart";

                int value = product.getqty_count();
                value = value + 1;

                product.setqty_count(value);
                System.out.println("valueplaus---" + value);
                product.setqty_count(value);

                new AsyCartAdd().execute(PROUCT_ID, Integer.toString(value));
                Prefs.putString(CHOPMOBILE_CONSTANTS.product_quantity,Integer.toString(value));
                count.setText("Qty:" + Integer.toString(value));

//                        int cnt_val = -1;
//
//                        for (int m = 0; m < Utils.Cart_checklist.getcartdata().size(); m++) {
//                            if (Utils.Cart_checklist.getcartdata().get(m).getitem_ID()
//                                    .equals(product.getitem_ID())) {
//                                cnt_val = m;
//                                break;
//                            }
//
//                        }
//                        if (cnt_val > -1) {
//
//                            Utils.Cart_checklist.getcartdata().set(cnt_val, product);
//                        } else {
//                            product.setitem_in_cart("YES");
//                            Utils.Cart_checklist.getcartdata().add(product);
//                        }
                //mListener.onCartUpdate(Utils.Cart_checklist.getcartdata(), null, "plus", product);
//                    }
//                } else {
//                    int value = product.getqty_count();
//                    value = value + 1;
//
//                    //product.setqty_count(value);
//                    product.setqty_count(value);
//
//
//                    count.setText(Integer.toString(value));
//
//                    int cnt_val = -1;
//
//                    for (int m = 0; m < Utils.Cart_checklist.getcartdata().size(); m++) {
//                        if (Utils.Cart_checklist.getcartdata().get(m).getitem_ID()
//                                .equals(product.getitem_ID())) {
//                            cnt_val = m;
//                            break;
//                        }
//
//                    }
//                    if (cnt_val > -1) {
//
//                        Utils.Cart_checklist.getcartdata().set(cnt_val, product);
//                    } else {
//                        product.setitem_in_cart("YES");
//                        Utils.Cart_checklist.getcartdata().add(product);
//                    }
//                    mListener.onCartUpdate(Utils.Cart_checklist.getcartdata(), null, "plus", product);


            }
        });
        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PAGE_NAME = "menu_with_addon_removecart";

                int value = product.getqty_count();
                if (value > 0) {
                    value = value - 1;

                    product.setqty_count(value);
                    System.out.println("valueplaus---" + value);
                    product.setqty_count(value);

                    new AsyCartRemove().execute(PROUCT_ID, Integer.toString(value));
                    Prefs.putString(CHOPMOBILE_CONSTANTS.product_quantity,Integer.toString(value));
                    count.setText("Qty:" + Integer.toString(value));
                }
//                 if(value==0){
//                    redesadapter.notifyDataSetChanged();
//                }
            }
        });

        check_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(cart_qty.getText().toString().equals("0")){
                    Toast.makeText(Product_Details.this,"Cart is Empty",Toast.LENGTH_LONG).show();
                }
                else {
                    Intent i = new Intent(Product_Details.this, Checkout.class);
                    startActivity(i);
                    //finish();

                }
            }
        });
        fav_image.setImageDrawable(getBaseContext().getResources().getDrawable(R.drawable.favrte_draw));
        //set the click listener
//        fav_image.setOnClickListener(new View.OnClickListener() {
//
//            public void onClick(View button) {
//                //Set the button's appearance
//                button.setSelected(!button.isSelected());
//
//                if (button.isSelected()) {
//                    //Handle selected state change
//                    PAGE_NAME = "add_favourite";
//                    fav_image.setBackgroundColor(Color.parseColor("#ffa500"));
//                    Addto_favriote(Product_Details.this);
//                } else {
//                    //Handle de-select state change
//                    PAGE_NAME = "remove_favourite";
//                    fav_image.setBackgroundResource(R.drawable.ic_favorite_white_18dp);
//                    Addto_favriote(Product_Details.this);
//                }
//
//            }
//
//        });
        fav_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (add_fave) {
                    PAGE_NAME = "add_favourite";
                    fav_image.setImageResource(R.drawable.ic_favorite_18pt_2x);
                    Addto_favriote(Product_Details.this);
                    add_fave = false;
                } else {
                    PAGE_NAME = "remove_favourite";
                    fav_image.setImageResource(R.drawable.ic_favorite_white_18dp);
                    Addto_favriote(Product_Details.this);
                    add_fave = true;
                }
            }
        });

        Fetch_Product(Product_Details.this);
    }

    public void Fetch_Product(Context context) {
        // mPostCommentResponse.requestStarted();
        showProgressDialog();
        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest sr = new StringRequest(Request.Method.POST, Utils.DOMAIN_NAME, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //mPostCommentResponse.requestCompleted();
                System.out.println("responsestring123---" + response.toString());
                hideProgressDialog();
                aie_fetch_event_details(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // mPostCommentResponse.requestEndedWithError(error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("page_name", "get_menuby_id");
                params.put("menu_id", PROUCT_ID);
                params.put("user_id", Prefs.getString(CHOPMOBILE_CONSTANTS.shared_user_id, ""));
               // params.put("location_id",Prefs.getString(location_id,""));
                params.put("rest",Prefs.getString("",Prefs.getString(CHOPMOBILE_CONSTANTS.Restaurant_Id,"")));
                System.out.println("allparams--" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(sr);
        sr.setShouldCache(false);
    }

    public void aie_fetch_event_details(String jsonObject) {
        Gson gson = new Gson();
        try {
            JSONObject jobj = new JSONObject(jsonObject);
            System.out.println("favevalueplus--"+jobj.getString("isfavourite"));
            if(jobj.getString("isfavourite").equals("no")){
                fav_image.setImageResource(R.drawable.ic_favorite_white_18dp);
                add_fave = true;
            }
            else if(jobj.getString("isfavourite").equals("yes")){
//                fav_image.setBackgroundColor(Color.parseColor("#ffa500"));
                fav_image.setImageResource(R.drawable.ic_favorite_18pt_2x);
                add_fave = false;
            }
            System.out.println("josnobjectfromserver---" + jobj.getJSONObject("data_result"));

            JSONObject prod = jobj.getJSONObject("data_result");
            System.out.println("productdescription"+prod.getString("menu_description").toString());
            if (!prod.getString("menu_description").toString().isEmpty() && prod.getString("menu_description").toString() != null && !prod.getString("menu_description").toString().equals("0"))
                product_description.setText(prod.getString("menu_description").toString());
            ImageLoader imageLoader = AppController.getInstance().getImageLoader();
            Picasso.with(getApplicationContext())
                    .load(prod.getString("menu_image").toString())
                    .placeholder(R.drawable.no_product)
                    .error(R.drawable.no_product)
                    .into(product_image);



            //product_image.setImageUrl(prod.getString("menu_image").toString(), imageLoader);
            product_price.setText("$" + prod.getString("menu_price").toString());
            count.setText("Qty:" + prod.getString("menu_quantity").toString());
            Prefs.putString(CHOPMOBILE_CONSTANTS.product_quantity,prod.getString("menu_quantity").toString());
            cart_qty.setText(jobj.getString("cart_quantity").toString());
            textView.setTitle( jobj.getString("loyalty_points"));
            product.setqty_count(Integer.parseInt(prod.getString("menu_quantity")));
            System.out.println("offerpricde---" + prod.get("offer_price").toString());
            String checkvalue = prod.get("offer_price").toString();
            if (checkvalue != null && !checkvalue.equals("null") && !checkvalue.isEmpty()) {
                OfferPrice.setVisibility(View.VISIBLE);
                OfferPrice.setText("Offer:$" + checkvalue);
                OfferPrice.setPaintFlags(OfferPrice.getPaintFlags());
//                | Paint.STRIKE_THRU_TEXT_FLAG);
            }
            // System.out.println("addonsize---"+(jobj.getJSONObject("data_result").getJSONArray("addon_details")));
            JSONArray the_json_array = prod.getJSONArray("addon_details");
            System.out.println("------" + the_json_array.toString());

            for (int i = 0; i < prod.getJSONArray("addon_details").length(); i++) {
                System.out.println("addonwhiolearray---" + prod.getJSONArray("addon_details").getJSONObject(i).length());
                System.out.println("addonwhiolearray123---" + prod.getJSONArray("addon_details").getJSONObject(i).toString());
                int sd = prod.getJSONArray("addon_details").getJSONObject(i).length();

                System.out.println("lengthofaddon--" + sd);

                product_addons.add((Prouduct_Addons) gson.fromJson(prod.getJSONArray("addon_details").getJSONObject(i).toString(), Prouduct_Addons.class));
                System.out.println("addonname---" + product_addons.get(i).getAddon_name());

                redesadapter = new Addon_Adapter(Product_Details.this, product_addons, selectedStrings,PROUCT_ID,count.getText().toString().replace("Qty:",""));
                redesadapter.notifyDataSetChanged();
                catagory_list.setAdapter(redesadapter);
//                if(prod.getJSONArray("addon_details").getJSONObject(i).length()>0) {
//                    for (int k = 0; k < sd; k++) {
//                        try {
//
//
//                            System.out.println("menudetails_name_addon" + prod.getJSONArray("addon_details").getJSONObject(k).getString("addon_name").toString());
////                            checkbox1.setText(jobj.getJSONObject("data_result").getJSONArray("addon_details").getJSONObject(k).get("addon_name").toString());
//
//                            CheckBox cb = new CheckBox(Product_Details.this);
//                            cb.setTag(1);
//                            cb.setText("Banana");
//                            checkbox1.put(cb);
//                            checkbox1.getCheckedIds().toString();
//                    }
//                        catch (Exception e){
//                            e.printStackTrace();
//                        }
//
//
//                    }
//                }

            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    void updateQty() {
        int cnt_val = -1;
        for (int m = 0; m < Utils.Cart_checklist.getcartdata().size(); m++) {
            if (Utils.Cart_checklist.getcartdata().get(m).getitem_ID()
                    .equals(product.getitem_ID())) {

                cnt_val = m;

                break;
            }

        }

        if (cnt_val > -1) {
            product.setqty_count(Utils.Cart_checklist.getcartdata().get(cnt_val).getqty_count());


        } else {

            product.setqty_count(0);

        }
        count.setText(Integer.toString(product
                .getqty_count()));


    }

    private class AsyCartAdd extends AsyncTask<String, Void, String> {

        Dialog dialog;

        protected void onPreExecute() {
            showProgressDialog();
        }

        @Override
        protected String doInBackground(String... place) {
            String productId = place[0];
            String v = place[1];

            Log.d("TAG", "Quantity  " + v);
            return cartRequest(productId, v);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            hideProgressDialog();
            if (result != null) {
                JSONObject jObject = null;
                try {
                    jObject = new JSONObject(result);
                    System.out.println("loginjson object" + jObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    json_result = jObject.getString("success");
                    if (json_result.equals("1")) {
                        Prefs.putString(CHOPMOBILE_CONSTANTS.cart_show_value, String.valueOf(jObject.get("cart_quantity")));
                        cart_qty.setText(Prefs.getString(CHOPMOBILE_CONSTANTS.cart_show_value, "0"));
                        System.out.println("menuquantity--" + jObject.get("menu_quantity").toString());
                        product_price.setText("$" + jObject.get("menudetail_total").toString());
                        PRODUCT_QUANTITY = jObject.get("menu_quantity").toString();


                        product.setProduct_quantity(PRODUCT_QUANTITY);

//                        Toast.makeText(Product_Details.this, jObject.getString("message"), Toast.LENGTH_LONG).show();
                        Toast.makeText(Product_Details.this, "Cart Successfully Updated", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.d("TAG", "Result " + result.toString());
            }

        }

    }

    //addcartrequest
    public String cartRequest(String productId, String v) {

        String result = null;
        String URL = Utils.DOMAIN_NAME;

        try {

            List<NameValuePair> pairs = new ArrayList<NameValuePair>();
            //pairs.add(new BasicNameValuePair("user_id", UID));
//            pairs.add(new BasicNameValuePair("device_id", Utils
//                    .getDeviceId(Product_Details.this)));
            pairs.add(new BasicNameValuePair("page_name", PAGE_NAME));
            pairs.add(new BasicNameValuePair("menu_id", productId));
            pairs.add(new BasicNameValuePair("device", "ANDROID"));
            String listString = TextUtils.join(", ", getSelectedString());
            System.out.println("selectedaddonfromadapter--" + TextUtils.join(", ", getSelectedString()));
            pairs.add(new BasicNameValuePair("addon_id", TextUtils.join(", ", getSelectedString()))); //Integer.toString(attrvalue)
            pairs.add(new BasicNameValuePair("quantity", v));  //Integer.toString(value)
            pairs.add(new BasicNameValuePair("user_id", Prefs.getString(CHOPMOBILE_CONSTANTS.shared_user_id, "")));
           // pairs.add(new BasicNameValuePair("location_id",Prefs.getString(location_id,"")));
            pairs.add(new BasicNameValuePair("rest",Prefs.getString(CHOPMOBILE_CONSTANTS.Restaurant_Id,"")));

            HttpClient httpClient = new DefaultHttpClient();
            Log.d("TAG", "Values add to cart" + pairs);
            HttpPost httpPost = new HttpPost(URL);
            httpPost.setEntity(new UrlEncodedFormEntity(pairs));
            HttpResponse httpResponse = httpClient.execute(httpPost);
            result = EntityUtils.toString(httpResponse.getEntity());
            Log.d("TAG", "Added " + v);
            Log.d("TAG", "Result " + result);
            //Log.d("TAG","HTTP Entity : " + convertStreamToString(httpPost.getEntity().getContent()));
        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }

        return result;

    }

    private class AsyCartRemove extends AsyncTask<String, Void, String> {

        Dialog dialog;

        protected void onPreExecute() {
            showProgressDialog();
        }

        @Override
        protected String doInBackground(String... place) {
            String productId = place[0];
            String v = place[1];
            Log.d("TAG", "Quantity  " + v);

            return cartRequest(productId, v);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            hideProgressDialog();
            if (result != null) {
                JSONObject jObject = null;
                try {
                    jObject = new JSONObject(result);
                    System.out.println("loginjson object" + jObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    json_result = jObject.getString("success");
                    if (json_result.equals("1")) {
                        Prefs.putString(CHOPMOBILE_CONSTANTS.cart_show_value, String.valueOf(jObject.get("cart_quantity")));
                        cart_qty.setText(String.valueOf(jObject.get("cart_quantity")));
                        PRODUCT_QUANTITY = jObject.get("menu_quantity").toString();

                        product_price.setText("$"+jObject.get("menudetail_total").toString());
                        product.setProduct_quantity(PRODUCT_QUANTITY);

                        Toast.makeText(Product_Details.this, "Removed from Cart", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(Product_Details.this, jObject.getString("message").toString(), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.d("TAG", "Result " + result.toString());

            }
        }

    }

    public void Addto_favriote(Context context) {
        // mPostCommentResponse.requestStarted();
        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest sr = new StringRequest(Request.Method.POST, Utils.DOMAIN_NAME, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //mPostCommentResponse.requestCompleted();
                System.out.println("addfavresponsestring123---" + response.toString());
                aie_fetch_delete_details(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // mPostCommentResponse.requestEndedWithError(error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("page_name", PAGE_NAME);
                params.put("menuid", PROUCT_ID);
                params.put("userid", Prefs.getString(CHOPMOBILE_CONSTANTS.shared_user_id, ""));
                //params.put("location_id",Prefs.getString(CHOPMOBILE_CONSTANTS.location_id,""));
                params.put("rest",Prefs.getString("",Prefs.getString(CHOPMOBILE_CONSTANTS.Restaurant_Id,"")));
                System.out.println("allparams--" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(sr);
        sr.setShouldCache(false);
    }

    public void aie_fetch_delete_details(String jsonObject) {
        Gson gson = new Gson();
        hideProgressDialog();
        try {
            JSONObject jobj = new JSONObject(jsonObject);
            System.out.println("josobjectfromser--" + jobj);
            if (jobj.getString("success").equals("1")) {
                Toast.makeText(Product_Details.this, jobj.getString("message").toString(), Toast.LENGTH_LONG).show();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void showProgressDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideProgressDialog() {
        if (pDialog.isShowing())
            pDialog.hide();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        pDialog.dismiss();

    }

    ArrayList<String> getSelectedString() {
        return selectedStrings;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //Toast.makeText(getApplicationContext(),"Password Reset Success", Toast.LENGTH_SHORT).show();
                onBackPressed();

                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if(offer_value==1){
            super.onBackPressed();
            Intent z=new Intent(Product_Details.this,Offers.class);
            startActivity(z);
            finish();
        }
        else if(offer_value==2){
            Intent y=new Intent(Product_Details.this,Favroite_List.class);
            startActivity(y);
            finish();
        }
        else if (offer_value==3){
            super.onBackPressed();
            finish();
        }
        else {
            super.onBackPressed();
            Intent i = new Intent(Product_Details.this, Product_TabPage.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            System.out.println("tabvaluenback---"+getIntent().getStringExtra("Tabvalue"));
            i.putExtra("POSITION_VALUE", getIntent().getStringExtra("Tabvalue"));
            startActivity(i);
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        // show menu only when home fragment is selected
        if (navItemIndex == 0) {
            getMenuInflater().inflate(R.menu.product_detail, menu);
            textView = (MenuItem) menu.findItem(R.id.action_logout);
//            LayerDrawable icon = (LayerDrawable) textView.getIcon();
//            // Update LayerDrawable's BadgeDrawable
//            Utils2.setBadgeCount(this, icon, Integer.parseInt(Prefs.getString(CHOPMOBILE_CONSTANTS.loyal_points,"0")));
            setMenuBackground();
//            textView.setIcon(R.drawable.list_background_rounded);
        }

        // when fragment is notifications, load the menu created for notifications
        if (navItemIndex == 6) {
            getMenuInflater().inflate(R.menu.notifications, menu);
        }
        return true;
    }

    protected void setMenuBackground() {
        // Log.d(TAG, "Enterting setMenuBackGround");
        Field field = null;
        LayoutInflater layoutInflater = getLayoutInflater();
        final LayoutInflater.Factory existingFactory = layoutInflater.getFactory();
        try {
            field = LayoutInflater.class.getDeclaredField("mFactorySet");
            field.setAccessible(true);
            field.setBoolean(layoutInflater, false);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        getLayoutInflater().setFactory(new LayoutInflater.Factory() {
            public View onCreateView(String name, Context context, AttributeSet attrs) {
                if (name.equalsIgnoreCase("com.android.internal.view.menu.IconMenuItemView")) {
                    try { // Ask our inflater to create the view
                        LayoutInflater f = getLayoutInflater();
                        final View view = f.createView(name, null, attrs);
                        /* The background gets refreshed each time a new item is added the options menu.
                        * So each time Android applies the default background we need to set our own
                        * background. This is done using a thread giving the background change as runnable
                        * object */
                        new Handler().post(new Runnable() {
                            public void run() {
                                // sets the background color
                                view.setBackgroundResource(R.color.orange);
                                // sets the text color
                                ((TextView) view).setTextColor(Color.BLACK);
                                // sets the text size
                                ((TextView) view).setTextSize(18);
                            }
                        });
                        return view;
                    } catch (InflateException e) {
                    } catch (ClassNotFoundException e) {
                    }
                }
                return null;
            }
        });
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        finish();
        startActivity(getIntent());
}

}
