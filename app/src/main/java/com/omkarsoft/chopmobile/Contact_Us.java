package com.omkarsoft.chopmobile;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import HellpersClass.CHOPMOBILE_CONSTANTS;
import HellpersClass.Utils;
import other.AppController;

import static android.Manifest.permission.CALL_PHONE;

/**
 * Created by user1 on 12-Apr-17.
 */

public class Contact_Us extends AppCompatActivity {

    private ProgressDialog pDialog;
    TextView contact_name, contact_address, contact_no, contact_email, call_text,textView35,textView36,textView37,textView38;
    ImageView call_button;
LinearLayout contact_back_image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact_us);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Contact Us");


        pDialog = new ProgressDialog(Contact_Us.this);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);

        contact_back_image=(LinearLayout)findViewById(R.id.contact_back_image);
        contact_name = (TextView) findViewById(R.id.editText);
        contact_address = (TextView) findViewById(R.id.editText3);
        contact_no = (TextView) findViewById(R.id.editText2);
        contact_email = (TextView) findViewById(R.id.editText1);
        call_button = (ImageView) findViewById(R.id.call_buttonimage);
        call_text=(TextView)findViewById(R.id.call_text);
        textView35=(TextView)findViewById(R.id.textView35);
        textView36=(TextView)findViewById(R.id.textView36);
        textView37=(TextView)findViewById(R.id.textView37);
        textView38=(TextView)findViewById(R.id.textView38);


        //background
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.actionbar_color,""))));
        contact_back_image.setBackgroundColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.background_app_color,"")));
        contact_name.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")));
        contact_address.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")));
        contact_no.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")));
        contact_email.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")));
        call_text.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")));
        textView35.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")));
        textView36.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")));
        textView37.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")));
        textView38.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")));


        Drawable myIcon1 = getResources().getDrawable( R.drawable.ic_call);
            ColorFilter filter1 = new LightingColorFilter( Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")),  Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")) );
            myIcon1.setColorFilter(filter1);
        call_button.setImageDrawable(myIcon1);

        call_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCall();
            }
        });
        call_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCall();
            }
        });
        LoadPrivacy(Contact_Us.this);
    }

    public void LoadPrivacy(Context context) {
        // mPostCommentResponse.requestStarted();
        showProgressDialog();
        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest sr = new StringRequest(Request.Method.POST, Utils.DOMAIN_NAME, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //mPostCommentResponse.requestCompleted();
                System.out.println("deleteresponsestring123---" + response.toString());
                aie_fetch_delete_details(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // mPostCommentResponse.requestEndedWithError(error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("page_name", "contact");
                //params.put("location_id",Prefs.getString(CHOPMOBILE_CONSTANTS.location_id,""));
                params.put("rest",Prefs.getString("",Prefs.getString(CHOPMOBILE_CONSTANTS.Restaurant_Id,"")));
                // params.put("quantity", menu_quantity);

                System.out.println("addparamscheckout---" + params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(sr);
    }

    public void aie_fetch_delete_details(String jsonObject) {
        Gson gson = new Gson();
        hideProgressDialog();
        try {
            JSONObject jobj = new JSONObject(jsonObject);
            System.out.println("josobjectfromser--" + jobj);
            if (jobj.getString("success").equals("1")) {
                contact_name.setText(jobj.getString("restaurant_name").toString());
                contact_email.setText(jobj.getString("email_id").toString());
                contact_no.setText(jobj.getString("phone_number").toString());
                contact_address.setText(jobj.getString("address").toString());

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void showProgressDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideProgressDialog() {
        if (pDialog.isShowing())
            pDialog.hide();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        pDialog.dismiss();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //Toast.makeText(getApplicationContext(),"Password Reset Success", Toast.LENGTH_SHORT).show();
                onBackPressed();

                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        AppController.getInstance().clearApplicationData();
//        Intent i = new Intent(Contact_Us.this, HomeActivity.class);
//        startActivity(i);
        finish();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {

            case 123:
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    onCall();
                } else {
                    Log.d("TAG", "Call Permission Not Granted");
                }
                break;

            default:
                break;
        }
    }

    public void onCall() {


        int permissionCheck = ContextCompat.checkSelfPermission(Contact_Us.this, CALL_PHONE);

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    Contact_Us.this,
                    new String[]{CALL_PHONE},
                    123);
        } else {


            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" + contact_no.getText().toString()));
//                if (ActivityCompat.checkSelfPermission(Contact_Us.this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
//                    // TODO: Consider calling
//                    //    ActivityCompat#requestPermissions
//                    // here to request the missing permissions, and then overriding
//                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//                    //                                          int[] grantResults)
//                    // to handle the case where the user grants the permission. See the documentation
//                    // for ActivityCompat#requestPermissions for more details.
//                    return;
//                }
            startActivity(callIntent);
        }

    }
}
