package com.omkarsoft.chopmobile;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.paypal.android.sdk.payments.PayPalAuthorization;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalFuturePaymentActivity;
import com.paypal.android.sdk.payments.PayPalItem;
import com.paypal.android.sdk.payments.PayPalOAuthScopes;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalPaymentDetails;
import com.paypal.android.sdk.payments.PayPalProfileSharingActivity;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.paypal.android.sdk.payments.ShippingAddress;
import com.pixplicity.easyprefs.library.Prefs;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import HellpersClass.CHOPMOBILE_CONSTANTS;
import HellpersClass.Utils;

/**
 * Basic sample using the SDK to make a payment or consent to future payments.
 *
 * For sample mobile backend interactions, see
 * https://github.com/paypal/rest-api-sdk-python/tree/master/samples/mobile_backend
 */
public class SampleActivity extends Activity {
    private static final String TAG = "paymentExample";
    /**
     * - Set to PayPalConfiguration.ENVIRONMENT_PRODUCTION to move real money.
     *
     * - Set to PayPalConfiguration.ENVIRONMENT_SANDBOX to use your test credentials
     * from https://developer.paypal.com
     *
     * - Set to PayPalConfiguration.ENVIRONMENT_NO_NETWORK to kick the tires
     * without communicating to PayPal's servers.
     */
  private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_PRODUCTION;

    // note that these credentials will differ between live & sandbox environments.
   // private static final String CONFIG_CLIENT_ID = "AW2ywQjUT3MfLBWms12C3qptk2Z-f517GZPzZ9ASn5Ur1RS5RvGzn3MT_L1PHqdtECdDx8SJPNtSqtiq";
//sandbox mode //
   // private static final String CONFIG_CLIENT_ID = "AQwzH3LVAGK6jAgS2qCeQzszz-DD9chr2twIUW7W3MpEJHf0rqyh4xXidSKJyhcH6JLmUC65ZmD69giP";
    //production client id
   private static final String CONFIG_CLIENT_ID = "AdKaU1_G3FD5fYH-IbPfgJozhNtLRsWsFV8VqHHKZ0HoazZPR_fI-8kClZXx0XSmMd7XdFsETcExKqLk";
    private static final int REQUEST_CODE_PAYMENT = 1;
    private static final int REQUEST_CODE_FUTURE_PAYMENT = 2;
    private static final int REQUEST_CODE_PROFILE_SHARING = 3;

    private static PayPalConfiguration config = new PayPalConfiguration().environment(CONFIG_ENVIRONMENT)
            .clientId(CONFIG_CLIENT_ID);
                    // The following are only used in PayPalFuturePaymentActivity.
//            .merchantName("Example Merchant")
//            .merchantPrivacyPolicyUri(Uri.parse("http://chopmobile.com/chopadmin/appdata.php"))
//            .merchantUserAgreementUri(Uri.parse("http://chopmobile.com/chopadmin/appdata.php"));


    String payment_status,transaction_id,booking_id;
    String result11;
    public ProgressDialog mProgressDialog;
    String productId,productPrice,productDesc,pay_money_url;
    double new_product_price;
    String PRICE;
    double dollar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.paypal_activity);

//        if (new_product_price>1) {
//            dollar = new_product_price/45.2;
//            System.out.println("the converted value from rupees to dollars is:"+dollar);
//        }
        mProgressDialog = new ProgressDialog(SampleActivity.this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        onBuyPressed();

        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intent);
    }

    public void onBuyPressed() {
        /*
         * PAYMENT_INTENT_SALE will cause the payment to complete immediately.
         * Change PAYMENT_INTENT_SALE to
         *   - PAYMENT_INTENT_AUTHORIZE to only authorize payment and capture funds later.
         *   - PAYMENT_INTENT_ORDER to create a payment for authorization and capture
         *     later via calls from your server.
         *
         * Also, to include additional payment details and an item list, see getStuffToBuy() below.
         */
        PayPalPayment thingToBuy = getThingToBuy(PayPalPayment.PAYMENT_INTENT_SALE);
//        PayPalPayment payment = new PayPalPayment(new BigDecimal("1.75"), "USD", "sample item",
//                PayPalPayment.PAYMENT_INTENT_SALE);

        /*
         * See getStuffToBuy(..) for examples of some available payment options.
         */
        Intent intent = new Intent(SampleActivity.this, PaymentActivity.class);

        // send the same configuration for restart resiliency
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);

        startActivityForResult(intent, REQUEST_CODE_PAYMENT);
    }

    private PayPalPayment getThingToBuy(String paymentIntent) {

        Bundle bundle = getIntent().getExtras();
//        booking_id = bundle.getString("booking_id");
        productId ="1";
      productPrice = bundle.getString("total_amount");
        productDesc = "TotalAmount";

//        System.out.println("productidget_product_id" + productId);
//
//
        System.out.println("printproductprice"+productPrice);
        new_product_price =Double.parseDouble(productPrice.replace(",",""));
        System.out.println(""+new_product_price);
        if (new_product_price>1) {
            dollar = new_product_price/67.22;
            System.out.println("the converted value from rupees to dollars is:"+dollar);
        }
        PRICE = String.valueOf(dollar);
        return new PayPalPayment(new BigDecimal(new_product_price), "USD", productDesc,
                paymentIntent);
    }

    /*
     * This method shows use of optional payment details and item list.
     */
    private PayPalPayment getStuffToBuy(String paymentIntent) {
        //--- include an item list, payment amount details
        PayPalItem[] items =
                {
                        new PayPalItem("sample item #1", 2, new BigDecimal("87.50"), "USD",
                                "sku-12345678"),
                        new PayPalItem("free sample item #2", 1, new BigDecimal("0.00"),
                                "USD", "sku-zero-price"),
                        new PayPalItem("sample item #3 with a longer name", 6, new BigDecimal("37.99"),
                                "USD", "sku-33333")
                };
        BigDecimal subtotal = PayPalItem.getItemTotal(items);
        BigDecimal shipping = new BigDecimal("7.21");
        BigDecimal tax = new BigDecimal("4.67");
        PayPalPaymentDetails paymentDetails = new PayPalPaymentDetails(shipping, subtotal, tax);
        BigDecimal amount = subtotal.add(shipping).add(tax);
        PayPalPayment payment = new PayPalPayment(amount, "USD", "sample item", paymentIntent);
        payment.items(items).paymentDetails(paymentDetails);

        //--- set other optional fields like invoice_number, custom field, and soft_descriptor
        payment.custom("This is text that will be associated with the payment that the app can use.");

        return payment;
    }

    /*
     * Add app-provided shipping address to payment
     */
    private void addAppProvidedShippingAddress(PayPalPayment paypalPayment) {
        ShippingAddress shippingAddress =
                new ShippingAddress().recipientName("Mom Parker").line1("52 North Main St.")
                        .city("Austin").state("TX").postalCode("78729").countryCode("US");
        paypalPayment.providedShippingAddress(shippingAddress);
    }

    /*
     * Enable retrieval of shipping addresses from buyer's PayPal account
     */
    private void enableShippingAddressRetrieval(PayPalPayment paypalPayment, boolean enable) {
        paypalPayment.enablePayPalShippingAddressesRetrieval(enable);
    }

    public void onFuturePaymentPressed(View pressed) {
        Intent intent = new Intent(SampleActivity.this, PayPalFuturePaymentActivity.class);

        // send the same configuration for restart resiliency
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

        startActivityForResult(intent, REQUEST_CODE_FUTURE_PAYMENT);
    }

    public void onProfileSharingPressed(View pressed) {
        Intent intent = new Intent(SampleActivity.this, PayPalProfileSharingActivity.class);

        // send the same configuration for restart resiliency
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

        intent.putExtra(PayPalProfileSharingActivity.EXTRA_REQUESTED_SCOPES, getOauthScopes());

        startActivityForResult(intent, REQUEST_CODE_PROFILE_SHARING);
    }

    private PayPalOAuthScopes getOauthScopes() {
        /* create the set of required scopes
         * Note: see https://developer.paypal.com/docs/integration/direct/identity/attributes/ for mapping between the
         * attributes you select for this app in the PayPal developer portal and the scopes required here.
         */
        Set<String> scopes = new HashSet<String>(
                Arrays.asList(PayPalOAuthScopes.PAYPAL_SCOPE_EMAIL, PayPalOAuthScopes.PAYPAL_SCOPE_ADDRESS) );
        return new PayPalOAuthScopes(scopes);
    }
    protected void displayResultText(String result) {
        ((TextView)findViewById(R.id.txtResult)).setText("Result : " + result);
        Toast.makeText(
                getApplicationContext(),
                result, Toast.LENGTH_LONG)
                .show();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm =
                        data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null) {
                    try {
                        Log.i(TAG, confirm.toJSONObject().toString(4));
                        Log.i(TAG, confirm.getPayment().toJSONObject().toString(4));
                        System.out.println("responsepayfull" + confirm.toJSONObject().toString(4));
                        transaction_id=confirm.toJSONObject().getJSONObject("response").getString("id");
                        payment_status=confirm.toJSONObject().getJSONObject("response").getString("state");
                        System.out.println("responsepayjsonaaaray" + confirm.toJSONObject().getJSONObject("response"));
                        System.out.println("responsepay1id" + confirm.toJSONObject().getJSONObject("response").getString("id"));
                        // System.out.println("responsepay2" + confirm.toJSONObject().getString("id"));

                        new TALK_TO_SERVER().execute();
                        /**
                         *  TODO: send 'confirm' (and possibly confirm.getPayment() to your server for verification
                         * or consent completion.
                         * See https://developer.paypal.com/webapps/developer/docs/integration/mobile/verify-mobile-payment/
                         * for more details.
                         *
                         *
                         * For sample mobile backend interactions, see
                         * https://github.com/paypal/rest-api-sdk-python/tree/master/samples/mobile_backend
                         */
                        displayResultText("Payment Successfully Received.");
                    } catch (JSONException e) {
                        Log.e(TAG, "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i(TAG, "Payment Canceled");
                displayResultText("Payment Canceled");
                finish();
//                Intent i=new Intent(SampleActivity.this, Online_Payment_Gateway.class);
//                startActivity(i);

            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(
                        TAG,
                        "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
                displayResultText("An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");

            }
        } else if (requestCode == REQUEST_CODE_FUTURE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PayPalAuthorization auth =
                        data.getParcelableExtra(PayPalFuturePaymentActivity.EXTRA_RESULT_AUTHORIZATION);
                if (auth != null) {
                    try {
                        Log.i("FuturePaymentExample", auth.toJSONObject().toString(4));

                        String authorization_code = auth.getAuthorizationCode();
                        Log.i("FuturePaymentExample", authorization_code);

                        sendAuthorizationToServer(auth);
                        displayResultText("Future Payment code received from PayPal");
                    } catch (JSONException e) {
                        Log.e("FuturePaymentExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("FuturePaymentExample", "Payment Canceled");
            } else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(
                        "FuturePaymentExample",
                        "Probably the attempt to previously start the PayPalService had an invalid PayPalConfiguration. Please see the docs.");
            }
        } else if (requestCode == REQUEST_CODE_PROFILE_SHARING) {
            if (resultCode == Activity.RESULT_OK) {
                PayPalAuthorization auth =
                        data.getParcelableExtra(PayPalProfileSharingActivity.EXTRA_RESULT_AUTHORIZATION);
                if (auth != null) {
                    try {
                        Log.i("ProfileSharingExample", auth.toJSONObject().toString(4));

                        String authorization_code = auth.getAuthorizationCode();
                        Log.i("ProfileSharingExample", authorization_code);

                        sendAuthorizationToServer(auth);
                        displayResultText("Profile Sharing code received from PayPal");

                    } catch (JSONException e) {
                        Log.e("ProfileSharingExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("ProfileSharingExample", "Payment Canceled");
            } else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(
                        "ProfileSharingExample",
                        "Probably the attempt to previously start the PayPalService had an invalid PayPalConfiguration. Please see the docs.");
            }
        }
    }

    private void sendAuthorizationToServer(PayPalAuthorization authorization) {

        /**
         * TODO: Send the authorization response to your server, where it can
         * exchange the authorization code for OAuth access and refresh tokens.
         *
         * Your server must then store these tokens, so that your server code
         * can execute payments for this user in the future.
         *
         * A more complete example that includes the required app-server to
         * PayPal-server integration is available from
         * https://github.com/paypal/rest-api-sdk-python/tree/master/samples/mobile_backend
         */

    }
    public void onFuturePaymentPurchasePressed(View pressed) {
        // Get the Client Metadata ID from the SDK
        String metadataId = PayPalConfiguration.getClientMetadataId(this);

        Log.i("FuturePaymentExample", "Client Metadata ID: " + metadataId);

        // TODO: Send metadataId and transaction details to your server for processing with
        // PayPal...
        displayResultText("Client Metadata Id received from SDK");
    }

   @Override
    public void onBackPressed(){

       this.finish();
    }
    @Override
    public void onDestroy() {
        // Stop service when done
        stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
    }

    private class TALK_TO_SERVER extends AsyncTask<String, Void, String> {

        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog.show();
        }
        @Override
        protected String doInBackground(String...URL) {
            try {
                System.out.println("print_product_ur_id" +productId);
                pay_money_url= Utils.DOMAIN_NAME;
                //pay_money_url = "http://maithreyaa.com/app/index.php?page=payment_process&product_id="+productId+"&product_name="+productDesc+"&product_price="+PRICE+"&payment_type=3&user_id="+ Prefs.getString(CHOPMOBILE_CONSTANTS.shared_user_id,"")+"&transcation_id="+transaction_id+"&payment_status=approved";

                HttpClient httpclient = new DefaultHttpClient();
                System.out.println("sampletimepost_url:-"+ pay_money_url);
                HttpPost httppost = new HttpPost(pay_money_url);
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);

                System.out.println("print_product_ur_id all namevalues" + nameValuePairs);
                nameValuePairs.add(new BasicNameValuePair("page_name","orders"));
                nameValuePairs.add(new BasicNameValuePair("userid", Prefs.getString(CHOPMOBILE_CONSTANTS.shared_user_id,"")));
                nameValuePairs.add(new BasicNameValuePair("device_id",Utils.getDeviceId(SampleActivity.this)));
                nameValuePairs.add(new BasicNameValuePair("delivery_address",Prefs.getString(CHOPMOBILE_CONSTANTS.customer_delivery_address,"")));
                Bundle bundle = getIntent().getExtras();
                nameValuePairs.add(new BasicNameValuePair("delivery_type",bundle.getString("delivery_type")));
               // nameValuePairs.add(new BasicNameValuePair("location_id",Prefs.getString(CHOPMOBILE_CONSTANTS.location_id,"")));
                nameValuePairs.add(new BasicNameValuePair("loyalty_status",Prefs.getString(CHOPMOBILE_CONSTANTS.loyality_status,"")));
                nameValuePairs.add(new BasicNameValuePair("rest",Prefs.getString(CHOPMOBILE_CONSTANTS.Restaurant_Id,"")));

                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                System.out.println("final check values"+nameValuePairs);
                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                System.out.println("print_response" + response);
                BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                sb.append(reader.readLine() + "\n");
                String line = "0";
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                reader.close();
                result11 = sb.toString();
                System.out.println("samplefuelpost result19:-" + result11);
                // parsing data

            } catch (Exception e) {
                e.printStackTrace();

            }
            return result11;
        }
        protected void onPostExecute(String getResult) {
            super.onPostExecute(getResult);
            System.out.println("inside post");
            System.out.println("sampletime post result:-" + getResult);

            if(getResult!=null) {
                mProgressDialog.dismiss();
                System.out.println("cuming cuminginised sample post");



                //timeparseResult(getResult);

//                AlertDialog.Builder builder = new AlertDialog.Builder(SampleActivity.this);
//                builder.setMessage("Your order has been placed successfully")
//                        .setCancelable(false)
//                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int id) {
//
//                                dialog.cancel();
                                Prefs.putString(CHOPMOBILE_CONSTANTS.customer_delivery_address,"");
                                Intent zz=new Intent(SampleActivity.this,HomeActivity.class);
                                //zz.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                                zz.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(zz);
//                            AppController.getInstance().clearApplicationData();
//                               finish();
//                            }
//                        })
//                        //Set your icon here
//                        .setTitle("ChopMobile")
//                        .setIcon(R.drawable.am_logo);
//                AlertDialog alert = builder.create();
//                alert.show();//showing the dialog

            }else{
                mProgressDialog.dismiss();
                System.out.println("inside no result");
                System.out.println("number result details:"+getResult);
                AlertDialog.Builder builder = new AlertDialog.Builder(SampleActivity.this);
                builder.setMessage("Somethings went wrong please try after Sometime")
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                dialog.cancel();
                            }
                        })
                                //Set your icon here
                        .setTitle("ChopMobile")
                        .setIcon(R.drawable.ic_launcher);
                AlertDialog alert = builder.create();
                alert.show();//showing the dialog
            }
        }
    }//Async Task Ends
}