package com.omkarsoft.chopmobile;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import fragments.Fragment1;


/**
 * Created by user1 on 28-Feb-17.
 */

public class TabLayoutUsage extends AppCompatActivity {
    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    private CheesePagerAdapter mPagerAdapter;
    private MyFragmentPageAdapter mPageAdapter;
    ArrayList<String> categories;
    private FloatingActionButton fab;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.design_tabs_viewpager);
        // Retrieve the Toolbar from our content view, and set it as the action bar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

//        List<Fragment> fragments = buildFragments();
//        categories = new ArrayList<String>();
//        categories.add("Category1");
//        categories.add("Category2");
//
        mTabLayout = (TabLayout) findViewById(R.id.tabs);
        mViewPager = (ViewPager) findViewById(R.id.tabs_viewpager);
        mPagerAdapter = new CheesePagerAdapter();
        mViewPager.setAdapter(mPagerAdapter);
//        //  mViewPager.setOnPageChangeListener(mTabLayout.createOnPageChangeListener());
       mTabLayout.setOnTabSelectedListener(mTabListener);


//
//        mPageAdapter = new MyFragmentPageAdapter(this,getSupportFragmentManager(), fragments, categories);
//        mViewPager.setAdapter(mPageAdapter);

        addRandomTab();
        fab = (FloatingActionButton) findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(TabLayoutUsage.this,Product_Details.class);
                startActivity(i);
            }
        });

    }

    private void addRandomTab() {
//        Random r = new Random();
//        String cheese = Cheeses.sCheeseStrings[r.nextInt(Cheeses.sCheeseStrings.length)];
        for (int i = 0; i < Cheeses.sCheeseStrings.length; i++) {
            mTabLayout.addTab(mTabLayout.newTab().setText(Cheeses.sCheeseStrings[i].toString()));
            mPagerAdapter.addTab(Cheeses.sCheeseStrings[i].toString());
        }
    }
    private List<android.support.v4.app.Fragment> buildFragments() {
        List<android.support.v4.app.Fragment> fragments = new ArrayList<android.support.v4.app.Fragment>();
        for(int i = 0; i<categories.size(); i++) {
            Bundle b = new Bundle();
            b.putInt("position", i);
            fragments.add(Fragment.instantiate(this, Fragment1.class.getName(),b));
        }

        return fragments;
    }
    private final TabLayout.OnTabSelectedListener
            mTabListener = new TabLayout.OnTabSelectedListener() {
        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            mViewPager.setCurrentItem(tab.getPosition());
        }

        @Override
        public void onTabUnselected(TabLayout.Tab tab) {
            // no-op
        }

        @Override
        public void onTabReselected(TabLayout.Tab tab) {
            // no-op
        }
    };

    private static class CheesePagerAdapter extends PagerAdapter {
        private final ArrayList<CharSequence> mCheeses = new ArrayList<>();

        public void addTab(String title) {
            mCheeses.add(title);
            notifyDataSetChanged();
        }

        public void removeTab() {
            if (!mCheeses.isEmpty()) {
                mCheeses.remove(mCheeses.size() - 1);
                notifyDataSetChanged();
            }
        }

        @Override
        public int getCount() {
            return mCheeses.size();
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            TextView tv = new TextView(container.getContext());
            tv.setText(getPageTitle(position));
            tv.setGravity(Gravity.CENTER);
            tv.setTextAppearance(tv.getContext(), R.style.TextAppearance_AppCompat_Title);
            container.addView(tv, ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            return tv;
        }
        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mCheeses.get(position);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }
    }

}
