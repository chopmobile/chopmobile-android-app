package com.omkarsoft.chopmobile;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import Adapters.Restaurant_Adapter;
import HellpersClass.CHOPMOBILE_CONSTANTS;
import HellpersClass.Utils;
import Model.SELECT_RESTURANT;
import other.Res;

/**
 * Created by user1 on 13-Jun-17.
 */

public class Selelct_Restauant extends AppCompatActivity{
    ProgressDialog pDialog;
    ArrayList<SELECT_RESTURANT> speakers_list = new ArrayList<SELECT_RESTURANT>();
    Restaurant_Adapter redesadapter;
    JSONArray odr_his_jArray;
    RecyclerView recyclerView;
    TextView not_found;
    LinearLayoutManager mLayoutManager;
    private Res res;
    @Override public Resources getResources() {
        if (res == null) {
            res = new Res(super.getResources());

        }
        return res;
    }
    Button submit;
    TextView textView15;
    CheckBox radiogroup;
    LinearLayout selelct_rest_backgound;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_restaurant);

        submit = (Button)findViewById(R.id.rest_submit);
        textView15= (TextView) findViewById(R.id.textView15);
        radiogroup=(CheckBox)findViewById(R.id.radiogroup);
        selelct_rest_backgound=(LinearLayout) findViewById(R.id.selelct_rest_backgound);


        pDialog = new ProgressDialog(Selelct_Restauant.this);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);

//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setTitle("Select Restaurant");

        not_found=(TextView)findViewById(R.id.not_found);
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);


        recyclerView = (RecyclerView) findViewById(R.id.rest_list);
        mLayoutManager = new LinearLayoutManager(Selelct_Restauant.this);
        recyclerView.setLayoutManager(mLayoutManager);

       // addRadioButtons(6);
        LoadPrivacy(Selelct_Restauant.this);

        ColorStateList colorStateList = new ColorStateList(
                new int[][]{

                        new int[]{-android.R.attr.state_enabled}, //disabled
                        new int[]{android.R.attr.state_enabled} //enabled
                },
                new int[] {

                        Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")) //disabled
                        ,Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")) //enabled

                }
        );

        //Change background colr
        submit.setBackgroundColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_background_color,"")));
        submit.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_text_color,"")));
        textView15.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")));
        selelct_rest_backgound.setBackgroundColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.background_app_color,"")));
        radiogroup.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")));
        radiogroup.setButtonTintList(colorStateList);


    }
//    public void addRadioButtons(int number) {
//
//        for (int row = 0; row < 1; row++) {
//            RadioGroup ll = new RadioGroup(this);
//            ll.setOrientation(LinearLayout.VERTICAL);
//
//            for (int i = 1; i <= number; i++) {
//                RadioButton rdbtn = new RadioButton(this);
//                rdbtn.setId((row * 2) + i);
//                rdbtn.setText("Radio kfnakfnakfna akdnksjnfks aksndskjdnks kasdnksjdnsk kandfskfnksj skdjskdnsjk skdjnskjfskjfieaja sdijfhdkjfhskj" + rdbtn.getId());
//                rdbtn.setBackgroundResource(R.color.orange);
//                rdbtn.setPadding(5,5,5,5);
//                ll.addView(rdbtn);
//            }
//            ((ViewGroup) findViewById(R.id.radiogroup)).addView(ll);
//        }
//
//    }
    public void LoadPrivacy(Context context) {
        // mPostCommentResponse.requestStarted();
        showProgressDialog();
        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest sr = new StringRequest(Request.Method.POST, Utils.DOMAIN_NAME, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //mPostCommentResponse.requestCompleted();
                System.out.println("deleteresponsestring123---" + response.toString());
                aie_fetch_speakers_details(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // mPostCommentResponse.requestEndedWithError(error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("page_name", "all_locations");
                params.put("rest",Prefs.getString("",Prefs.getString(CHOPMOBILE_CONSTANTS.Restaurant_Id,"")));

                // params.put("quantity", menu_quantity);

                System.out.println("stripeaddparamscheckout---" + params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(sr);
    }

    private void aie_fetch_speakers_details(String jsonObject) {
        Gson gson = new Gson();
        try {
            JSONObject jobj = new JSONObject(jsonObject);
            hideProgressDialog();
            if(jobj.getString("success").equals("1")) {
                this.odr_his_jArray = jobj.getJSONArray("location");
                System.out.println("speaker_jArray.length()" + this.odr_his_jArray.length());
                for (int histlistidx = 0; histlistidx < this.odr_his_jArray.length(); histlistidx++) {
                    this.speakers_list.add((SELECT_RESTURANT) gson.fromJson(this.odr_his_jArray.getJSONObject(histlistidx).toString(), SELECT_RESTURANT.class));
                }
                Prefs.putString(CHOPMOBILE_CONSTANTS.select_resturant_size,String.valueOf(speakers_list.size()));

                if(speakers_list.size()>1) {
                    selelct_rest_backgound.setVisibility(View.VISIBLE);
                    redesadapter = new Restaurant_Adapter(Selelct_Restauant.this, speakers_list);
                    redesadapter.notifyDataSetChanged();
                    recyclerView.setAdapter(redesadapter);
                }
                else {
                   Prefs.putString(CHOPMOBILE_CONSTANTS.location_id,speakers_list.get(0).getId());
                    Intent i=new Intent(Selelct_Restauant.this,HomeActivity.class);
                    startActivity(i);
                    finish();
                }
            }
            else{
                not_found.setVisibility(View.VISIBLE);
                not_found.setText(jobj.getString("message").toString());
//                Toast.makeText(OrderHistory.this,jobj.getString("message").toString(),Toast.LENGTH_LONG).show();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void showProgressDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideProgressDialog() {
        if (pDialog.isShowing())
            pDialog.hide();
        pDialog.dismiss();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }



}
