package com.omkarsoft.chopmobile;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import Adapters.CheckOut_Adapter;
import HellpersClass.CHOPMOBILE_CONSTANTS;
import HellpersClass.Utils;
import Model.Products1;
import other.AppController;

import static com.omkarsoft.chopmobile.HomeActivity.navItemIndex;

/**
 * Created by user1 on 03-Mar-17.
 */

public class Checkout extends AppCompatActivity {

    RecyclerView recyclerView;
    ArrayList<Products1> event_list = new ArrayList<Products1>();
    CheckOut_Adapter redesadapter;
    RelativeLayout abtus_back_imgvw;
    TextView actionbar_text;
    private ProgressDialog pDialog;
    private String tag_json_obj = "jobj_req";
    private String TAG = Checkout.class.getSimpleName();
    JSONArray odr_his_jArray;
    private int previousTotal = 0;
    private boolean loading = true;
    private int visibleThreshold = 10;
    int firstVisibleItem, visibleItemCount, totalItemCount;
    LinearLayoutManager mLayoutManager;
    private Tracker mTracker;
    TextView checkout_tototelitems, checkout_totalprice, taxt_precntage, Taxamount,total_qtycheck,youpay_text;
    static TextView you_pay;
    Button button1, button2, button3, checck_out;
    String TOTAL_PRICE;
    String DELIVERY_TYPE;
    Boolean clicked = false;
    String order_id;
    TextView loyalpoints;
    CheckBox loyal_check;
    String url2,Phone_Number;
    MenuItem textView,action_search;
    ScrollView checkout_back_main;
    LinearLayout checkout_back_main2;
    RelativeLayout checkout_back_main3,checkout_back;
    int takeout_status,delivery_status;
    int TOTAL_ITEM;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // getSupportActionBar().hide();
        setContentView(R.layout.checkout);

        checkout_totalprice = (TextView) findViewById(R.id.total_amount);
        Taxamount = (TextView) findViewById(R.id.tax_pay);
        taxt_precntage = (TextView) findViewById(R.id.taxt_precntage);
        checkout_tototelitems = (TextView) findViewById(R.id.checkout_totalqtyt);
        you_pay = (TextView) findViewById(R.id.you_pay);
        total_qtycheck= (TextView) findViewById(R.id.total_qtycheck);
        youpay_text= (TextView) findViewById(R.id.youpay_text);

        button2 = (Button) findViewById(R.id.button2);
        button3 = (Button) findViewById(R.id.button3);
        loyalpoints = (TextView) findViewById(R.id.loyal_points);
        loyal_check = (CheckBox) findViewById(R.id.layal_text);
//        checkout_back_main=(ScrollView)findViewById(R.id.checkout_back_main);
        checck_out = (Button) findViewById(R.id.checck_out);
        checkout_back_main2=(LinearLayout)findViewById(R.id.checkout_back_main2);
        checkout_back_main3=(RelativeLayout) findViewById(R.id.checkout_back_main3);
        checkout_back=(RelativeLayout) findViewById(R.id.checkout_back_button);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            order_id = extras.getString("ORDER_ID");
        }

        button2.setBackgroundColor(Color.parseColor("#2B3034"));
        button3.setBackgroundColor(Color.parseColor("#2B3034"));

//        button1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                button1.setBackgroundColor(Color.parseColor("#EE2142"));
//                button2.setBackgroundColor(Color.parseColor("#CCCCCC"));
//                button3.setBackgroundColor(Color.parseColor("#CCCCCC"));
//            }
//        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DELIVERY_TYPE = "1";
                clicked = true;
                button2.setBackgroundColor(Color.parseColor("#EE2142"));
                button3.setBackgroundColor(Color.parseColor("#2B3034"));
                // button1.setBackgroundColor(Color.parseColor("#CCCCCC"));
            }
        });
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DELIVERY_TYPE = "2";
                clicked = true;
                button3.setBackgroundColor(Color.parseColor("#EE2142"));
                // button1.setBackgroundColor(Color.parseColor("#CCCCCC"));
                button2.setBackgroundColor(Color.parseColor("#2B3034"));
            }
        });

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Cart");

        ColorStateList colorStateList = new ColorStateList(
                new int[][]{

                        new int[]{-android.R.attr.state_enabled}, //disabled
                        new int[]{android.R.attr.state_enabled} //enabled
                },
                new int[] {

                        Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")) //disabled
                        ,Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")) //enabled

                }
        );

        //change color
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.actionbar_color,""))));
//        checkout_back_main.setBackgroundColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.background_app_color,"")));
        checkout_back_main2.setBackgroundColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.background_app_color,"")));
        checkout_back_main3.setBackgroundColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.background_app_color,"")));
        checck_out.setBackgroundColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_background_color,"")));
        checck_out.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_text_color,"")));
        checkout_back.setBackgroundColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.background_app_color,"")));

        checkout_tototelitems.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")));
        checkout_totalprice.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")));
        taxt_precntage.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")));
        Taxamount.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")));
        total_qtycheck.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")));
        youpay_text.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")));
        you_pay.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")));
        loyal_check.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")));
        loyalpoints.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")));
        loyal_check.setButtonTintList(colorStateList);

        recyclerView = (RecyclerView) findViewById(R.id.check_list);
        recyclerView.setBackgroundColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.background_app_color,"")));
        mLayoutManager = new LinearLayoutManager(Checkout.this);
        recyclerView.setLayoutManager(mLayoutManager);


        checck_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(loyal_check.isChecked()){
                    Prefs.putString(CHOPMOBILE_CONSTANTS.loyality_status,"1");
                }
                else {
                    Prefs.putString(CHOPMOBILE_CONSTANTS.loyality_status,"0");
                }

                if (clicked) {
                    System.out.println("TOTAL_ITEMprint---"+TOTAL_ITEM);
                    if(TOTAL_ITEM>0) {
                        Intent i = new Intent(Checkout.this, Home_Delivery.class);
                        i.putExtra("total_amount", TOTAL_PRICE);
                        i.putExtra("delivery_type", DELIVERY_TYPE);
                        Prefs.putString(CHOPMOBILE_CONSTANTS.delivery_type, DELIVERY_TYPE);
                        startActivity(i);
//                    finish();
                    }
                    else {
                        Toast.makeText(Checkout.this, "Please add product to Cart", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(Checkout.this, "Please select delivery type", Toast.LENGTH_LONG).show();
                }

            }
        });
        String url1 = null;
        if (order_id != null && !order_id.isEmpty()) {
            url1 = Utils.DOMAIN_NAME + "?page_name=" + "reorder" + "&userid=" + Prefs.getString(CHOPMOBILE_CONSTANTS.shared_user_id, "") + "&order_id=" + order_id+"&rest="+Prefs.getString(CHOPMOBILE_CONSTANTS.Restaurant_Id,"");
        } else {
            url1 = Utils.DOMAIN_NAME + "?page_name=" + "show_cart" + "&user_id=" + Prefs.getString(CHOPMOBILE_CONSTANTS.shared_user_id, "")+"&rest="+Prefs.getString(CHOPMOBILE_CONSTANTS.Restaurant_Id,"");
        }

        loyal_check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (loyal_check.isChecked()) {
                    System.out.println("cuming inside laoyal");
                    if (order_id != null && !order_id.isEmpty()) {
                        url2 = Utils.DOMAIN_NAME + "?page_name=" + "pay_with_loyalty_points" + "&user_id=" + Prefs.getString(CHOPMOBILE_CONSTANTS.shared_user_id, "") + "&check_value=" + "1" + "&from_where=" + "reorder" + "&order_id=" + order_id+"&rest="+Prefs.getString(CHOPMOBILE_CONSTANTS.Restaurant_Id,"");
                    } else {
                        url2 = Utils.DOMAIN_NAME + "?page_name=" + "pay_with_loyalty_points" + "&user_id=" + Prefs.getString(CHOPMOBILE_CONSTANTS.shared_user_id, "") + "&check_value=" + "1" + "&from_where=" + "cart" + "&order_id=" + "null"+"&rest="+Prefs.getString(CHOPMOBILE_CONSTANTS.Restaurant_Id,"");
                    }
                    fetchloyal_method(url2);
                } else {
                    System.out.println("cuming inside uncheck");
                    if (order_id != null && !order_id.isEmpty()) {
                        url2 = Utils.DOMAIN_NAME + "?page_name=" + "pay_with_loyalty_points" + "&user_id=" + Prefs.getString(CHOPMOBILE_CONSTANTS.shared_user_id, "") + "&check_value=" + "0" + "&from_where=" + "reorder" + "&order_id=" + order_id+"&rest="+Prefs.getString(CHOPMOBILE_CONSTANTS.Restaurant_Id,"");
                    } else {
                        url2 = Utils.DOMAIN_NAME + "?page_name=" + "pay_with_loyalty_points" + "&user_id=" + Prefs.getString(CHOPMOBILE_CONSTANTS.shared_user_id, "") + "&check_value=" + "0" + "&from_where=" + "cart" + "&order_id=" + "null"+"&rest="+Prefs.getString(CHOPMOBILE_CONSTANTS.Restaurant_Id,"");
                    }
                    fetchloyal_method(url2);
                }
            }
        });

        fetchevent_method(url1);
        System.out.println("eventlisturl123" + url1);

    }

    public void fetchevent_method(String url) {

        // String url = Utils.DOMAIN_NAME+Utils.EVENT_LIST;
        System.out.println("eventlisturl" + url);
        showProgressDialog();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());
                        System.out.println("jsonresponssevideo" + response.toString());
                        aie_fetch_event_details(response);

                        //msgResponse.setText(response.toString());
                        hideProgressDialog();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                System.out.println("inside error" + error.getMessage());
                hideProgressDialog();
            }
        }) {

            /**
             * Passing some request headers
             * */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                return headers;
            }


        };
        jsonObjReq.setShouldCache(false);
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq,
                tag_json_obj);

    }

    private void showProgressDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideProgressDialog() {
        if (pDialog.isShowing())
            pDialog.hide();
    }

    private void aie_fetch_event_details(JSONObject jsonObject) {
        Gson gson = new Gson();
        try {
            TOTAL_ITEM=Integer.parseInt(jsonObject.getString("total_items"));
            checkout_tototelitems.setText("YOU HAVE " + jsonObject.getString("total_items") + " ITEMS IN YOUR CART");
            checkout_totalprice.setText("$" + jsonObject.getString("total"));
            taxt_precntage.setText("Tax" + "  (" + jsonObject.getString("sales_tax").toString() + "%" + ")");
            Taxamount.setText("$"+jsonObject.getString("sales_tax_amt").toString());
            you_pay.setText("$" + jsonObject.getString("mytotal").toString());
            loyalpoints.setText(jsonObject.getString("loyalty_points").toString()+".00");
            textView.setTitle(jsonObject.getString("loyalty_points").toString());
            takeout_status=Integer.parseInt(jsonObject.getString("takeout").toString());
            delivery_status=Integer.parseInt(jsonObject.getString("delivery").toString());


            if(takeout_status==1){
                button2.setVisibility(View.GONE);
            }
            if(delivery_status==1){
                button3.setVisibility(View.GONE);
            }

//            Phone_Number=jsonObject.getString("phone_number").toString();
            System.out.println("Phone_Number---"+Phone_Number);
            TOTAL_PRICE = jsonObject.getString("mytotal");
            this.odr_his_jArray = jsonObject.getJSONArray("user_cart_details");
            System.out.println("speaker_jArray.length()" + this.odr_his_jArray.length());
            for (int histlistidx = 0; histlistidx < this.odr_his_jArray.length(); histlistidx++) {
                this.event_list.add((Products1) gson.fromJson(this.odr_his_jArray.getJSONObject(histlistidx).toString(), Products1.class));
            }
            redesadapter = new CheckOut_Adapter(Checkout.this, event_list);
            recyclerView.setAdapter(redesadapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        pDialog.dismiss();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //Toast.makeText(getApplicationContext(),"Password Reset Success", Toast.LENGTH_SHORT).show();
                onBackPressed();

                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();

    }

    public void fetchloyal_method(String url) {

        // String url = Utils.DOMAIN_NAME+Utils.EVENT_LIST;
        System.out.println("loyaleventlisturl" + url);
        showProgressDialog();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());
                        System.out.println("jsonresponssevideo" + response.toString());
                        aie_fetch_checkbox_details(response);

                        //msgResponse.setText(response.toString());
                        hideProgressDialog();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                System.out.println("inside error" + error.getMessage());
                hideProgressDialog();
            }
        }) {

            /**
             * Passing some request headers
             * */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                return headers;
            }


        };
        jsonObjReq.setShouldCache(false);
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq,
                tag_json_obj);

    }

    public void aie_fetch_checkbox_details(JSONObject jsonObject) {
        Gson gson = new Gson();
        hideProgressDialog();
        try {
            System.out.println("loyaljosobjectfromser--" + jsonObject);
            if (jsonObject.getString("success").equals("1")) {
                loyalpoints.setText(jsonObject.getString("loyalty_points").toString());

                you_pay.setText("$" + jsonObject.getString("youpay").toString());
                TOTAL_PRICE = jsonObject.getString("youpay").toString();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        // show menu only when home fragment is selected
        if (navItemIndex == 0) {
            getMenuInflater().inflate(R.menu.checkout_main, menu);
            textView = (MenuItem) menu.findItem(R.id.action_logout);

//            LayerDrawable icon = (LayerDrawable) textView.getIcon();
//            // Update LayerDrawable's BadgeDrawable
//            Utils2.setBadgeCount(this, icon, 356);
//            textView.setIcon(R.drawable.list_background_rounded);
        }

        // when fragment is notifications, load the menu created for notifications
        if (navItemIndex == 6) {
            getMenuInflater().inflate(R.menu.notifications, menu);
        }
        return true;
    }

}
