package com.omkarsoft.chopmobile;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.pixplicity.easyprefs.library.Prefs;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import HellpersClass.CHOPMOBILE_CONSTANTS;
import HellpersClass.CONNECTIONDETECTOR;
import HellpersClass.Utils;
import other.AppController;

/**
 * Created by user1 on 02-May-17.
 */

public class Settings_Page extends AppCompatActivity{
    private ProgressDialog pDialog;
    String push_status;
    EditText Full_Name,Email,Password,Address,Location,Mobileno,Lastname,Pincode;
    Button Submit;
    String EMAIL, PASSWORD, NAME, MOBILENO, LASTNAME, LOCATION, PINCODE, ADDRESS,USER_STATUS;
    Boolean isInternetPresent = false;
    CONNECTIONDETECTOR cd;
    String login_url,url2;
    String result11, json_result;
    private String tag_json_obj = "jobj_req";
    private String TAG = Checkout.class.getSimpleName();
    LinearLayout settings_back_lay;
    TextView notification_title,profile_settings;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_setting);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Settings");

        pDialog = new ProgressDialog(Settings_Page.this);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);

        cd = new CONNECTIONDETECTOR(getApplicationContext());

        settings_back_lay=(LinearLayout)findViewById(R.id.settings_back_lay);
        Full_Name=(EditText)findViewById(R.id.full_name);
        Email=(EditText)findViewById(R.id.email_id);
        Password=(EditText)findViewById(R.id.change_password);
        Address=(EditText)findViewById(R.id.Delivery_Address);
        Location=(EditText)findViewById(R.id.Address2);
        Mobileno=(EditText)findViewById(R.id.phone_number);
        Submit=(Button) findViewById(R.id.checck_out);
        Lastname=(EditText)findViewById(R.id.last_name);
        Pincode=(EditText)findViewById(R.id.pincode);
        notification_title=(TextView)findViewById(R.id.notification_title);
        profile_settings=(TextView)findViewById(R.id.profile_settings);

        //background
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.actionbar_color,""))));
        settings_back_lay.setBackgroundColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.background_app_color,"")));
        Submit.setBackgroundColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_background_color,"")));
        Submit.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_text_color,"")));
        Full_Name.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")));
        Email.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")));
        Password.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")));
        Address.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")));
        Location.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")));
        Mobileno.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")));
        Lastname.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")));
        notification_title.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")));
        Pincode.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")));
        profile_settings.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")));

        Full_Name.setHintTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")));
        Email.setHintTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")));
        Password.setHintTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")));
        Address.setHintTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")));
        Location.setHintTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")));
        Mobileno.setHintTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")));
        Lastname.setHintTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")));
        Pincode.setHintTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")));

        Switch mySwitch = (Switch) findViewById(R.id.myswitch);
        mySwitch.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")));
        if(Prefs.getString(CHOPMOBILE_CONSTANTS.push_status,"0").equals("0")) {
            mySwitch.setChecked(true);
        }
        if(Prefs.getString(CHOPMOBILE_CONSTANTS.push_status,"0").equals("1")) {
            mySwitch.setChecked(false);
        }
        mySwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // do something, the isChecked will be
                // true if the switch is in the On position
                if(isChecked){
                    System.out.println("cuming inside if");
                    LoadPrivacy(Settings_Page.this);
                    push_status="0";
                    Prefs.putString(CHOPMOBILE_CONSTANTS.push_status,"0");

                }
                else {
                    System.out.println("cuming inside else");
                    LoadPrivacy(Settings_Page.this);
                    push_status="1";
                    Prefs.putString(CHOPMOBILE_CONSTANTS.push_status,"1");
                }
            }
        });

        Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EMAIL = Email.getText().toString().trim();
                PASSWORD = Password.getText().toString().trim();
                NAME = Full_Name.getText().toString().trim();
//                TelephonyManager tMgr = (TelephonyManager)RegisterActivity.this.getSystemService(Context.TELEPHONY_SERVICE);
//                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
//                    String mPhoneNumber = tMgr.getLine1Number();
//                    System.out.println("mobilenofetch" + tMgr);
//                    if (!mPhoneNumber.isEmpty() && mPhoneNumber != null) {
//                        System.out.println();
//                        MOBILENO = mPhoneNumber;
//                    }
//                }
//                else {
                MOBILENO = Mobileno.getText().toString().trim();
                LOCATION = Location.getText().toString().trim();
                ADDRESS = Address.getText().toString().trim();
                LASTNAME=Lastname.getText().toString().trim();
                PINCODE = Pincode.getText().toString().trim();
                // USER_STATUS=Userstatus.getText().toString().trim();
                if (NAME.equals("")) {
                    Toast.makeText(getApplicationContext(), "Name is Required", Toast.LENGTH_SHORT).show();

                }
//                else if (MOBILENO.equals("")) {
//                    Toast.makeText(getApplicationContext(), "Mobile Number is Required", Toast.LENGTH_SHORT).show();
//
//                }
//                else if (EMAIL.equals("")) {
//                    Toast.makeText(getApplicationContext(), "Email ID is Required", Toast.LENGTH_SHORT).show();
//                }
                else if (PASSWORD.equals("")) {
                    Toast.makeText(getApplicationContext(), "Password is Required", Toast.LENGTH_SHORT).show();
                }  else if (ADDRESS.equals("")) {
                    Toast.makeText(getApplicationContext(), "Address is Required", Toast.LENGTH_SHORT).show();
                }
                else if (Location.equals("")) {
                    Toast.makeText(getApplicationContext(), "Location is Required", Toast.LENGTH_SHORT).show();
                }
                else if (PINCODE.equals("")) {
                    Toast.makeText(getApplicationContext(), "Pincode is Required", Toast.LENGTH_SHORT).show();
                }
                else if (LASTNAME.equals("")) {
                    Toast.makeText(getApplicationContext(), "Lastname is Required", Toast.LENGTH_SHORT).show();
                }
                else {
                    isInternetPresent = cd.isConnectingToInternet();
                    // check for Internet status
                    if (isInternetPresent) {
                        // Internet Connection is Present
                        // make HTTP requests
                        showProgressDialog();
                        new Login_user().execute();
                    } else {
                        // Internet connection is not present
                        // Ask user to connect to Internet
//                        showAlertDialog(AndroidDetectInternetConnectionActivity.this, "No Internet Connection",
//                                "You don't have internet connection.", false);
                        AlertDialog.Builder builder = new AlertDialog.Builder(Settings_Page.this);
                        builder.setMessage("No Internet Connection Please Check Your Internet connection.")
                                .setCancelable(false)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {

                                        dialog.cancel();
                                    }
                                })
                                //Set your icon here
                                .setTitle("ChopMobile")
                                .setIcon(R.drawable.ic_launcher);
                        AlertDialog alert = builder.create();
                        alert.show();//showing the dialog
                    }
                }
            }
        });
        url2 = Utils.DOMAIN_NAME + "?page_name=" + "get_profileby_userid" + "&user_id=" + Prefs.getString(CHOPMOBILE_CONSTANTS.shared_user_id, "")+ "&rest=" + Prefs.getString(CHOPMOBILE_CONSTANTS.Restaurant_Id, "");
        fetchevent_method(url2);
    }
    public void LoadPrivacy(Context context) {
        // mPostCommentResponse.requestStarted();

        showProgressDialog();
        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest sr = new StringRequest(Request.Method.POST, Utils.DOMAIN_NAME, new Response.Listener<String>() {
                @Override
            public void onResponse(String response) {
                //mPostCommentResponse.requestCompleted();
                System.out.println("deleteresponsestring123---" + response.toString());
                aie_fetch_delete_details(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // mPostCommentResponse.requestEndedWithError(error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("page_name", "push_settings");
                params.put("user_id", Prefs.getString(CHOPMOBILE_CONSTANTS.shared_user_id,""));
                params.put("push_status",push_status);
                params.put("rest",Prefs.getString("",Prefs.getString(CHOPMOBILE_CONSTANTS.Restaurant_Id,"")));
                // params.put("quantity", menu_quantity);

                System.out.println("addparamscheckout---" + params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(sr);
    }

    public void aie_fetch_delete_details(String jsonObject) {
        Gson gson = new Gson();
        hideProgressDialog();
        try {
            JSONObject jobj = new JSONObject(jsonObject);
            System.out.println("josobjectfromser--"+jobj);
            if(jobj.getString("success").equals("1")) {

                Toast.makeText(Settings_Page.this,jobj.getString("message"),Toast.LENGTH_LONG).show();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void showProgressDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideProgressDialog() {
        if (pDialog.isShowing())
            pDialog.hide();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        pDialog.dismiss();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //Toast.makeText(getApplicationContext(),"Password Reset Success", Toast.LENGTH_SHORT).show();
                onBackPressed();

                return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        AppController.getInstance().clearApplicationData();
//        Intent i=new Intent(Settings_Page.this,HomeActivity.class);
//        startActivity(i);
        finish();
    }

    private class Login_user extends AsyncTask<String, Void, String> {

        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... URL) {

            try {

                login_url = Utils.DOMAIN_NAME;
                HttpClient httpclient = new DefaultHttpClient();
                System.out.println("post url:-" + login_url);
                HttpPost httppost = new HttpPost(login_url);
                System.out.println("post namevaluepair name" + EMAIL);
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                nameValuePairs.add(new BasicNameValuePair("page_name", "profile_update"));
                nameValuePairs.add(new BasicNameValuePair("email", EMAIL));
                nameValuePairs.add(new BasicNameValuePair("password", PASSWORD));
                nameValuePairs.add(new BasicNameValuePair("firstname", NAME));
                nameValuePairs.add(new BasicNameValuePair("phone", MOBILENO));
               // nameValuePairs.add(new BasicNameValuePair("address", ADDRESS));
              //  nameValuePairs.add(new BasicNameValuePair("location", LOCATION));
              //  nameValuePairs.add(new BasicNameValuePair("pincode", PINCODE));
                nameValuePairs.add(new BasicNameValuePair("lastname", LASTNAME));
                nameValuePairs.add(new BasicNameValuePair("user_id", Prefs.getString(CHOPMOBILE_CONSTANTS.shared_user_id,"")));
                nameValuePairs.add(new BasicNameValuePair("rest",Prefs.getString(CHOPMOBILE_CONSTANTS.Restaurant_Id,"")));

                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                System.out.println("post all namevalues" + nameValuePairs);

                // Execute HTTP Post Request
                org.apache.http.HttpResponse response = httpclient.execute(httppost);

                BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                sb.append(reader.readLine() + "\n");
                String line = "0";
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                reader.close();
                result11 = sb.toString();
                System.out.println("post result19:-" + result11);
                // parsing data

            } catch (Exception e) {
                e.printStackTrace();

            }
            return result11;
        }

        protected void onPostExecute(String getResult) {
            super.onPostExecute(getResult);
            System.out.println("logininside post");
            System.out.println("login post result:-" + getResult);
            if (getResult != null) {
                JSONObject jObject = null;
                try {
                    jObject = new JSONObject(getResult);
                    System.out.println("loginjson object" + jObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    json_result = jObject.getString("success");
                    System.out.println("loginjson reg" + json_result);
                    if (json_result.equals("1")) {
                        System.out.println("inside json success");
                        hideProgressDialog();

//                        System.out.println("jstring--"+jObject.getString("userId"));
//                        jsonUser_id = jObject.getString("userId");
//                        jsonName = jObject.getString("userName");
//                        System.out.println( "inside success"+jsonUser_id);

                        Toast.makeText(Settings_Page.this, jObject.getString("message").toString(), Toast.LENGTH_SHORT).show();
//                        Prefs.putString(INKTALK_APP_CONSTANT.shared_user_id, jsonUser_id);
//                        Prefs.putString(INKTALK_APP_CONSTANT.shared_user_name, jsonName);


                        Intent home_int = new Intent(Settings_Page.this, Settings_Page.class);
                        startActivity(home_int);
                        finish();

                    } else if (json_result.equals("0")) {
                        hideProgressDialog();
                        Toast.makeText(getApplicationContext(), jObject.getString("message").toString(), Toast.LENGTH_SHORT).show();
                    } else {
                        hideProgressDialog();
                        Toast.makeText(getApplicationContext(), "Sorry! There is a Server Issue. please Try After Sometime", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } else {
                hideProgressDialog();
                System.out.println("inside no result");
                System.out.println("no result details:" + getResult);
                AlertDialog.Builder builder = new AlertDialog.Builder(Settings_Page.this);
                builder.setMessage("Somethings went wrong please try after Sometime")
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                dialog.cancel();
                            }
                        })
                        //Set your icon here
                        .setTitle("InkTalks")
                        .setIcon(R.drawable.ic_launcher);
                AlertDialog alert = builder.create();
                alert.show();//showing the dialog
            }
        }
    }
    public void fetchevent_method(String url) {

        // String url = Utils.DOMAIN_NAME+Utils.EVENT_LIST;
        System.out.println("eventlisturl" + url);
        showProgressDialog();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());
                        System.out.println("jsonresponssevideo" + response.toString());
                        aie_fetch_event_details(response);

                        //msgResponse.setText(response.toString());
                        hideProgressDialog();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                System.out.println("inside error" + error.getMessage());
                hideProgressDialog();
            }
        }) {

            /**
             * Passing some request headers
             * */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                return headers;
            }


        };
        jsonObjReq.setShouldCache(false);
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq,
                tag_json_obj);

    }


    private void aie_fetch_event_details(JSONObject jsonObject) {
        Gson gson = new Gson();
        try {
            Full_Name.setText(jsonObject.getString("firstname"));
            Lastname.setText(jsonObject.getString("lastname"));
            Email.setText(jsonObject.getString("email"));
            System.out.println("user_address"+jsonObject.getString("user_address").toString());
            if(jsonObject.getString("user_address").toString()!=null&&!(jsonObject.getString("user_address").toString()).isEmpty()&&!(jsonObject.getString("user_address").toString()).equals("null")) {
                Address.setText(jsonObject.getString("user_address"));
            }
            if((jsonObject.getString("location").toString())!= null && !(jsonObject.getString("location").toString()).isEmpty()&&!(jsonObject.getString("location").toString()).equals("null")) {
                Location.setText(jsonObject.getString("location"));
            }
            if((jsonObject.getString("phone").toString())!= null && !(jsonObject.getString("phone").toString()).isEmpty()&&!(jsonObject.getString("phone").toString()).equals("null")) {
                Mobileno.setText(jsonObject.getString("phone"));
            }
            if((jsonObject.getString("pincode").toString())!= null && !(jsonObject.getString("pincode").toString()).isEmpty()&&!(jsonObject.getString("pincode").toString()).equals("0")) {
                Pincode.setText(jsonObject.getString("pincode"));
            }
            Password.setText(jsonObject.getString("password").toString());
            //Password.setText(Base64.encodeToString(jsonObject.getString("password").getBytes(), Base64.DEFAULT ));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }




}
