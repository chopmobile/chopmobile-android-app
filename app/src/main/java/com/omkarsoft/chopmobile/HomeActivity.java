package com.omkarsoft.chopmobile;


import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.pixplicity.easyprefs.library.Prefs;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Adapters.HomeList_Adapter;
import HellpersClass.CHOPMOBILE_CONSTANTS;
import HellpersClass.Utils;
import Model.CATEGORY;
import app.Config;
import fragments.Fragment1;
import other.CircleTransform;
import other.NotificationUtils;
import other.Res;


/**
 * Created by user1 on 2/6/2017.
 */

public class HomeActivity extends AppCompatActivity implements ActivityCompat.OnRequestPermissionsResultCallback {

    private static final int SIMPLE_NOTFICATION_ID_A = 0;
    private NavigationView navigationView;
    private DrawerLayout drawer;
    private View navHeader;
    private ImageView imgNavHeaderBg, imgProfile;
    private TextView txtName, txtWebsite;
    private Toolbar toolbar;
    private FloatingActionButton fab;

    // urls to load navigation header background image
    // and profile image
    private static final String urlNavHeaderBg = "http://api.androidhive.info/images/nav-menu-header-bg.jpg";
    private static final String urlProfileImg = "https://lh3.googleusercontent.com/eCtE_G34M9ygdkmOpYvCag1vBARCmZwnVS6rS5t4JLzJ6QgQSBquM0nuTsCpLhYbKljoyS-txg";

    // index to identify current nav menu itemI
    public static int navItemIndex = 0;

    // tags used to attach the fragments
    private static final String TAG_HOME = "Categories";
    private static final String TAG_PHOTOS = "Profile";
    private static final String TAG_MOVIES = "My Orders";
    private static final String TAG_MYFAV = "My Fav";
    private static final String TAG_PAYHistory = "Pay History";
    private static final String TAG_LOYALITYPOINT = "Loyality Point";
    private static final String TAG_NOTIFICATIONS = "notifications";
    private static final String TAG_SETTINGS = "settings";
    public static String CURRENT_TAG = TAG_HOME;

    // toolbar titles respected to selected nav menu item
    private String[] activityTitles;

    // flag to load home fragment when user presses back key
    private boolean shouldLoadHomeFragOnBackPress = true;
    private Handler mHandler;
    RecyclerView catagory_list;
    HomeList_Adapter main_adapter;
    ProgressDialog pDialog;
    private String TAG = HomeActivity.class.getSimpleName();
    public Integer[] mThumbIds;
    // Tag used to cancel the request
    String tag_json_obj = "json_obj_req";
    ArrayList<CATEGORY> Category_List = new ArrayList<CATEGORY>();
    JSONArray odr_his_jArray;
    private int itemCount = 0;
    private BigDecimal checkoutAmount = new BigDecimal(BigInteger.ZERO);
    MenuItem textView;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private TextView txtRegId, txtMessage, footer_text;
    final Context c = this;
    boolean doubleBackToExitPressedOnce = false;
    private Res res;
    LinearLayout home_linear_background;
    NotificationManager mNotificationManager;
    SharedPreferences mPrefs;
    final String welcomeScreenShownPref = "welcomeScreenShown";
    int resturant_array_size;
    String pay_money_url;
    String result11;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);


        //hasPermissionInManifest(HomeActivity.this,CALL_PHONE);

        txtRegId = (TextView) findViewById(R.id.txt_reg_id);
        txtMessage = (TextView) findViewById(R.id.txt_push_message);
        footer_text = (TextView) findViewById(R.id.footer_text);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.actionbar_color, ""))));
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("Categories");

        resturant_array_size = Integer.parseInt(Prefs.getString(CHOPMOBILE_CONSTANTS.select_resturant_size, "0"));


        mHandler = new Handler();

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
//        fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.hide();

        // Navigation view header
        navHeader = navigationView.getHeaderView(0);
        txtName = (TextView) navHeader.findViewById(R.id.name);
        txtWebsite = (TextView) navHeader.findViewById(R.id.website);
        imgNavHeaderBg = (ImageView) navHeader.findViewById(R.id.img_header_bg);
        imgProfile = (ImageView) navHeader.findViewById(R.id.img_profile);
        home_linear_background = (LinearLayout) findViewById(R.id.home_linear_background);

        //hide sidemenu item
//        if (resturant_array_size <= 1) {
//            navigationView.getMenu().findItem(R.id.select_restuarant).setVisible(false);
//        }


        //changing background color
        home_linear_background.setBackgroundColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.background_app_color, "")));
        navigationView.setBackgroundColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.background_app_color, "")));
        navigationView.setItemTextColor(ColorStateList.valueOf(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color, ""))));
        navigationView.setItemIconTintList(ColorStateList.valueOf(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_background_color, ""))));
        txtName.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color, "")));
        txtWebsite.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color, "")));
        footer_text.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color, "")));

        // load toolbar titles from string resources
        activityTitles = getResources().getStringArray(R.array.nav_item_activity_titles);


        // load nav menu header data
        loadNavHeader();

        // initializing navigation menu
        setUpNavigationView();

        if (savedInstanceState == null) {
            navItemIndex = 0;
            CURRENT_TAG = TAG_HOME;
            System.out.println("cuming inside saveinstance");
            loadHomeFragment();
        }
       // postNewComment(HomeActivity.this);

        catagory_list = (RecyclerView) findViewById(R.id.category_list);
        GridLayoutManager grid1 = new GridLayoutManager(HomeActivity.this, 2);
        catagory_list.setLayoutManager(grid1);

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    System.out.println("cume inside once");
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);

                    displayFirebaseRegId();

                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    // new push notification is received
                    System.out.println("cume inside always");
                    String message = intent.getStringExtra("message");

                    Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();

                    txtMessage.setText(message);
                }
            }
        };
        mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        // second argument is the default to use if the preference can't be found
        Boolean welcomeScreenShown = mPrefs.getBoolean(welcomeScreenShownPref, false);

        // comment for time being
//        if (!welcomeScreenShown) {
//            SendRegid(Utils.DOMAIN_NAME);
//            displayFirebaseRegId();
//
//            SharedPreferences.Editor editor = mPrefs.edit();
//            editor.putBoolean(welcomeScreenShownPref, true);
//            editor.commit(); // Very important to save the preference
//        }
        SendRegid(Utils.DOMAIN_NAME);
        displayFirebaseRegId();

    }

    /***
     * Load navigation menu header information
     * like background image, profile image
     * name, website, notifications action view (dot)
     */
    private void loadNavHeader() {
        // name, website
        System.out.println("fullname---" + Prefs.getString(CHOPMOBILE_CONSTANTS.profile_full_name, ""));
        txtName.setText(Prefs.getString(CHOPMOBILE_CONSTANTS.profile_full_name, ""));
        txtWebsite.setText(Prefs.getString(CHOPMOBILE_CONSTANTS.profile_email, ""));

        // loading header background image
//        Glide.with(this).load(urlNavHeaderBg)
//                .crossFade()
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .into(imgNavHeaderBg);
        imgNavHeaderBg.setBackgroundColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_background_color, "")));

        // Loading profile image
        Glide.with(this).load(Prefs.getString(CHOPMOBILE_CONSTANTS.application_logo, ""))
                .crossFade()
                .thumbnail(0.5f)
                .bitmapTransform(new CircleTransform(this))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imgProfile);

        // showing dot next to notifications label
//        navigationView.getMenu().getItem(3).setActionView(R.layout.menu_dot);
    }

    /***
     * Returns respected fragment that user
     * selected from navigation menu
     */
    private void loadHomeFragment() {
        // selecting appropriate nav menu item
        // selectNavMenu();

        // set toolbar title
        setToolbarTitle();

        // if user select the current navigation menu again, don't do anything
        // just close the navigation drawer
        if (getSupportFragmentManager().findFragmentByTag(CURRENT_TAG) != null) {
            //  drawer.closeDrawers();

            // show or hide the fab button
            toggleFab();
            return;
        }

        // Sometimes, when fragment has huge data, screen seems hanging
        // when switching between navigation menus
        // So using runnable, the fragment is loaded with cross fade effect
        // This effect can be seen in GMail app
        Runnable mPendingRunnable = new Runnable() {
            @Override
            public void run() {
                // update the main content by replacing fragments
                Fragment fragment = getHomeFragment();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                        android.R.anim.fade_out);
                fragmentTransaction.replace(R.id.frame, fragment, CURRENT_TAG);
                fragmentTransaction.commitAllowingStateLoss();
            }
        };

        // If mPendingRunnable is not null, then add to the message queue
        if (mPendingRunnable != null) {
            mHandler.post(mPendingRunnable);
        }

        // show or hide the fab button
        toggleFab();

        //Closing drawer on item click
        drawer.closeDrawers();

        // refresh toolbar menu
        invalidateOptionsMenu();

        new STRIPE_TALK_TO_SERVER().execute();
//        try {
//            AppController.getInstance().clearApplicationData();
        //    postNewComment(HomeActivity.this);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    public Fragment getHomeFragment() {
        switch (navItemIndex) {
//            case 0:
//                // home
//                Fragment1 homeFragment = new Fragment1();
//                return homeFragment;
//            case 1:
//                // photos
//                Fragment2 photosFragment = new Fragment2();
//                return photosFragment;
//            case 2:
//                // movies fragment
//                Fragment3 moviesFragment = new Fragment3();
//                return moviesFragment;
//
//            case 3:
//                // movies fragment
//                Intent i=new Intent(HomeActivity.this,OrderHistory.class);
//                startActivity(i);
//
//            case 4:
//                // movies fragment
//                Intent j=new Intent(HomeActivity.this,Contact_Us.class);
//                startActivity(j);
//
//            case 5:
//                // movies fragment
//                Fragment8 loyalityPointFragment = new Fragment8();
//                return loyalityPointFragment;
//
//
//            case 6:
//                // notifications fragment
//                Fragment4 notificationsFragment = new Fragment4();
//                return notificationsFragment;
//
//            case 7:
//                // settings fragment
//                Fragment5 settingsFragment = new Fragment5();
//                return settingsFragment;
            default:
                return new Fragment1();
        }
    }

    private void setToolbarTitle() {
        getSupportActionBar().setTitle(activityTitles[navItemIndex]);
    }

    private void selectNavMenu() {
        navigationView.getMenu().getItem(navItemIndex).setChecked(true);
    }

    private void setUpNavigationView() {

        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                //Check to see which item was being clicked and perform appropriate action
                switch (menuItem.getItemId()) {
                    //Replacing the main content with ContentFragment Which is our Inbox View;
                    case R.id.nav_home:
//                        navItemIndex = 0;
//                        CURRENT_TAG = TAG_HOME;
                        Intent k = new Intent(HomeActivity.this, HomeActivity.class);
                        startActivity(k);
                        // drawer.closeDrawers();
                        finish();
                        break;
                    case R.id.nav_photos:
                        Intent j = new Intent(HomeActivity.this, Offers.class);
                        startActivity(j);
                        //drawer.closeDrawers();
                        //finish();
                        break;


                    case R.id.nav_movies:
//                        navItemIndex = 3;
//                        CURRENT_TAG = TAG_MYFAV;
                        Intent f = new Intent(HomeActivity.this, OrderHistory.class);
                        startActivity(f);
                        // drawer.closeDrawers();
                        //  finish();
                        break;
                    case R.id.nav_history:
                        Intent g = new Intent(HomeActivity.this, Contact_Us.class);
                        startActivity(g);
                        //drawer.closeDrawers();
//                        finish();
                        break;

                    case R.id.nav_notifications:
                        Intent c = new Intent(HomeActivity.this, Favroite_List.class);
                        startActivity(c);
//                        finish();
                        break;
                    case R.id.nav_settings:
                        Intent z = new Intent(HomeActivity.this, Settings_Page.class);
                        startActivity(z);
                        break;
                    case R.id.nav_privacy_policy:
                        // launch new intent instead of loading fragment
                        Intent v = new Intent(HomeActivity.this, PrivacyPolicyActivity.class);
                        startActivity(v);
                        //drawer.closeDrawers();
                        break;
                    case R.id.logout:
                        Toast.makeText(getApplicationContext(), "Successfully Logout !", Toast.LENGTH_LONG).show();
                        Prefs.putString(CHOPMOBILE_CONSTANTS.shared_user_id, "");
                        Prefs.putString(CHOPMOBILE_CONSTANTS.remeber_check, "false");
                        Prefs.putString(CHOPMOBILE_CONSTANTS.forget_email, "");
                        Prefs.putString(CHOPMOBILE_CONSTANTS.loyal_points, "");
                        Prefs.putString(CHOPMOBILE_CONSTANTS.profile_full_name, "");
                        Prefs.putString(CHOPMOBILE_CONSTANTS.profile_email, "");
                        //  Prefs.putString(CHOPMOBILE_CONSTANTS.location_id, "0");
                        Prefs.putString(CHOPMOBILE_CONSTANTS.use_location_future, "0");
                        Intent az = new Intent(HomeActivity.this, LoginActivity.class);
                        startActivity(az);
                        finish();
                        break;

//                    case R.id.select_restuarant:
//                        // launch new intent instead of loading fragment
//                        startActivity(new Intent(HomeActivity.this, Selelct_Restauant.class));
//                        //drawer.closeDrawers();
//                        break;
                    default:
                        navItemIndex = 0;
                }

                // Checking if the item is in checked state or not, if not make it in checked state
                if (menuItem.isChecked()) {
                    menuItem.setChecked(false);
                } else {
                    menuItem.setChecked(true);
                }
                menuItem.setChecked(true);

                loadHomeFragment();

                return true;
            }
        });


        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer layout
        drawer.setDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();
    }

//    @Override
//    public void onBackPressed() {
//        if (drawer.isDrawerOpen(GravityCompat.START)) {
//            drawer.closeDrawers();
//            return;
//        }
//
//        // This code loads home fragment when back key is pressed
//        // when user is in other fragment than home
//        if (shouldLoadHomeFragOnBackPress) {
//            // checking if user is on other navigation menu
//            // rather than home
//            if (navItemIndex != 0) {
//                navItemIndex = 0;
//                CURRENT_TAG = TAG_HOME;
//                loadHomeFragment();
//                return;
//            }
//        }
//
//        finish();
//    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        // show menu only when home fragment is selected
        if (navItemIndex == 0) {
            getMenuInflater().inflate(R.menu.main, menu);
            textView = (MenuItem) menu.findItem(R.id.action_logout);

//            MenuItem select_location = (MenuItem) menu.findItem(R.id.select_location);
//            if (resturant_array_size <= 1) {
//                select_location.setVisible(false);
//            }
//            Drawable myIcon1 = getResources().getDrawable( R.drawable.ic_edit_location_white_2x);
//            ColorFilter filter1 = new LightingColorFilter( Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_background_color,"")),  Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_background_color,"")) );
//            myIcon1.setColorFilter(filter1);
//            select_location.setIcon(myIcon1);


            MenuItem star = (MenuItem) menu.findItem(R.id.action_search);
//            Drawable myIcon = getResources().getDrawable( R.drawable.loyal_image );
//            ColorFilter filter = new LightingColorFilter( Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_background_color,"")),  Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_background_color,"")) );
//            myIcon.setColorFilter(filter);
//            star.setIcon(myIcon);


//            select_location.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
//                @Override
//                public boolean onMenuItemClick(MenuItem menuItem) {
//                    Intent i = new Intent(HomeActivity.this, Selelct_Restauant.class);
//                    startActivity(i);
////                    finish();
//                    return false;
//                }
//            });
//            textView.setIcon(R.drawable.list_background_rounded);
        }

        // when fragment is notifications, load the menu created for notifications
        if (navItemIndex == 6) {
            getMenuInflater().inflate(R.menu.notifications, menu);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_logout) {
//
//            return true;
//        }

        // user is in notifications fragment
        // and selected 'Mark all as Read'
        if (id == R.id.action_mark_all_read) {
            Toast.makeText(getApplicationContext(), "All notifications marked as read!", Toast.LENGTH_LONG).show();
        }

        // user is in notifications fragment
        // and selected 'Clear All'
        if (id == R.id.action_clear_notifications) {
            Toast.makeText(getApplicationContext(), "Clear all notifications!", Toast.LENGTH_LONG).show();
        }
        if (id == android.R.id.home) {
            // app icon in action bar clicked; go home
            this.finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // show or hide the fab
    private void toggleFab() {
//        if (navItemIndex == 0)
//           // fab.show();
//        else
//            fab.hide();
    }


    private void showProgressDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideProgressDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

//    public void postNewComment(Context context) {
//        // mPostCommentResponse.requestStarted();
//        showProgressDialog();
//        RequestQueue queue = Volley.newRequestQueue(context);
//
//        HashMap<String, SoftReference<Bitmap>> cache =
//                new HashMap<String, SoftReference<Bitmap>>();
//        StringRequest sr = new StringRequest(Request.Method.POST, Utils.DOMAIN_NAME, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                //mPostCommentResponse.requestCompleted();
//                System.out.println("responsestring123---" + response.toString());
//              //  aie_fetch_event_details(response);
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//
//                System.out.println("volleyerorcate---" + error);
//                // mPostCommentResponse.requestEndedWithError(error);
//            }
//        })
//
//        {
//            @Override
//            protected Map<String, String> getParams() {
//                Map<String, String> params = new HashMap<String, String>();
//                params.put("page_name", "category");
//                params.put("user_id", Prefs.getString(CHOPMOBILE_CONSTANTS.shared_user_id, ""));
//                // params.put("location_id", Prefs.getString(CHOPMOBILE_CONSTANTS.location_id, ""));
//                params.put("rest", Prefs.getString("", Prefs.getString(CHOPMOBILE_CONSTANTS.Restaurant_Id, "")));
//
//                System.out.println("categoryparams---" + params);
//                return params;
//            }
//
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                Map<String, String> params = new HashMap<String, String>();
//                params.put("Content-Type", "application/x-www-form-urlencoded");
//                return params;
//            }
//
//        };
//        RetryPolicy policy = new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//        sr.setRetryPolicy(policy);
//        queue.add(sr);
//        sr.setShouldCache(false);
//    }

//    public void aie_fetch_event_details(String jsonObject) {
//        hideProgressDialog();
//        Gson gson = new Gson();
//        try {
//            JSONObject jobj = new JSONObject(jsonObject);
//            this.odr_his_jArray = jobj.getJSONArray("category");
//            System.out.println("catjsonitem--" + jobj);
//            System.out.println("loyalfromserver--" + jobj.getString("loyalty_points").toString());
//            textView.setTitle("       " + jobj.getString("loyalty_points").toString());
//
//
////            CharSequence menuTitle = textView.getTitle();
////            SpannableString styledMenuTitle = new SpannableString(menuTitle);
////            styledMenuTitle.setSpan(new ForegroundColorSpan(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_background_color,""))), 0, menuTitle.length(), 0);
////            textView.setTitle(styledMenuTitle);
//
////            LayerDrawable icon = (LayerDrawable) textView.getIcon();
////            // Update LayerDrawable's BadgeDrawable
////            System.out.println("loyal_sharevalue--"+jobj.getString("loyalty_points").toString());
////            Utils2.setBadgeCount(this, icon, Integer.parseInt(jobj.getString("loyalty_points").toString()));
//
//            Prefs.putString(CHOPMOBILE_CONSTANTS.loyal_points, jobj.getString("loyalty_points").toString());
//            System.out.println("speaker_jArray.length()" + this.odr_his_jArray.length());
//            for (int histlistidx = 0; histlistidx < this.odr_his_jArray.length(); histlistidx++) {
//                this.Category_List.add((CATEGORY) gson.fromJson(this.odr_his_jArray.getJSONObject(histlistidx).toString(), CATEGORY.class));
//            }
//
//
//            main_adapter = new HomeList_Adapter(HomeActivity.this, Category_List);
//            catagory_list.setAdapter(main_adapter);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//    }

    public void updateCheckOutAmount(BigDecimal amount, boolean increment) {

        if (increment) {
            checkoutAmount = checkoutAmount.add(amount);
        } else {
            if (checkoutAmount.signum() == 1)
                checkoutAmount = checkoutAmount.subtract(amount);
        }

        // checkOutAmount.setText(Money.rupees(checkoutAmount).toString());
    }

    public void updateItemCount(boolean ifIncrement) {
        if (ifIncrement) {
            itemCount++;
            //itemCountTextView.setText(String.valueOf(itemCount));

        } else {

            // itemCountTextView.setText(String.valueOf(itemCount <= 0 ? 0
            //    : --itemCount));
        }

        // toggleBannerVisibility();
    }

    public int getItemCount() {
        return itemCount;
    }

    // Fetches reg id from shared preferences
    // and displays on the screen
    private void displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);


        Log.e(TAG, "Firebase reg id: " + regId);

        if (!TextUtils.isEmpty(regId))
            txtRegId.setText("Firebase Reg Id: " + regId);
        else
            txtRegId.setText("Firebase Reg Id is not received yet!");
    }

    @Override
    protected void onResume() {
        super.onResume();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    public void SendRegid(String url) {
        // mPostCommentResponse.requestStarted();
        RequestQueue queue = Volley.newRequestQueue(HomeActivity.this);
        StringRequest sr = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //mPostCommentResponse.requestCompleted();
                System.out.println("regidresponsestring123---" + response.toString());


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // mPostCommentResponse.requestEndedWithError(error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("page_name", "app_regid");
                params.put("user_id", Prefs.getString(CHOPMOBILE_CONSTANTS.shared_user_id, ""));
                SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
                params.put("reg_id", pref.getString("regId", null));
                params.put("rest", Prefs.getString("", Prefs.getString(CHOPMOBILE_CONSTANTS.Restaurant_Id, "")));

                System.out.println("reidparams" + params);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(sr);
        sr.setShouldCache(false);

    }

    //    public boolean hasPermissionInManifest(Context context, String permissionName) {
//        final String packageName = context.getPackageName();
//        try {
//            System.out.println("cuming inside permission---");
//            final PackageInfo packageInfo = context.getPackageManager()
//                    .getPackageInfo(packageName, PackageManager.GET_PERMISSIONS);
//            final String[] declaredPermisisons = packageInfo.requestedPermissions;
//            if (declaredPermisisons != null && declaredPermisisons.length > 0) {
//                for (String p : declaredPermisisons) {
//                    if (p.equals(permissionName)) {
//                        return true;
//                    }
//                }
//            }
//        } catch (PackageManager.NameNotFoundException e) {
//
//        }
//        return false;
//    }
    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            android.os.Process.killProcess(android.os.Process.myPid());
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    @Override
    public Resources getResources() {
        if (res == null) {
            res = new Res(super.getResources());
        }
        return res;
    }

    //    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//
//        try {
//            trimCache(this);
//        } catch (Exception e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
//    }
    public static void trimCache(Context context) {
        try {
            File dir = context.getCacheDir();
            if (dir != null && dir.isDirectory()) {
                deleteDir(dir);
            }
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }

        // The directory is now empty so delete it
        return dir.delete();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        pDialog.dismiss();
    }


    private class STRIPE_TALK_TO_SERVER extends AsyncTask<String, Void, String> {

        protected void onPreExecute() {
            super.onPreExecute();
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... URL) {
            try {

                pay_money_url = Utils.DOMAIN_NAME;

                HttpClient httpclient = new DefaultHttpClient();
                System.out.println("sampletimepost_url:-" + pay_money_url);
                HttpPost httppost = new HttpPost(pay_money_url);
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);

                nameValuePairs.add(new BasicNameValuePair("page_name", "category"));
                nameValuePairs.add(new BasicNameValuePair("user_id", Prefs.getString(CHOPMOBILE_CONSTANTS.shared_user_id, "")));
                nameValuePairs.add(new BasicNameValuePair("rest", Prefs.getString("", Prefs.getString(CHOPMOBILE_CONSTANTS.Restaurant_Id, ""))));


                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                System.out.println("stripe final check valuescategoryparams---" + nameValuePairs);
                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                System.out.println("print_response" + response);
                BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                sb.append(reader.readLine() + "\n");
                String line = "0";
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                reader.close();
                result11 = sb.toString();
                System.out.println("samplefuelpost result19:-" + result11);
                // parsing data

            } catch (Exception e) {
                e.printStackTrace();

            }
            return result11;
        }

        protected void onPostExecute(String getResult) {
            super.onPostExecute(getResult);
            System.out.println("inside post");
            System.out.println("sampletime post result:-" + getResult);

            if (getResult != null) {
                 pDialog.dismiss();
                Gson gson = new Gson();
                try {
                    JSONObject jobj = new JSONObject(getResult);
                    odr_his_jArray = jobj.getJSONArray("category");
                    System.out.println("catjsonitem--" + jobj);
                    System.out.println("loyalfromserver--" + jobj.getString("loyalty_points").toString());
                    textView.setTitle("       " + jobj.getString("loyalty_points").toString());



                    Prefs.putString(CHOPMOBILE_CONSTANTS.loyal_points, jobj.getString("loyalty_points").toString());
                    System.out.println("speaker_jArray.length()" + odr_his_jArray.length());
                    for (int histlistidx = 0; histlistidx < odr_his_jArray.length(); histlistidx++) {
                        Category_List.add((CATEGORY) gson.fromJson(odr_his_jArray.getJSONObject(histlistidx).toString(), CATEGORY.class));
                    }


                    main_adapter = new HomeList_Adapter(HomeActivity.this, Category_List);
                    catagory_list.setAdapter(main_adapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                pDialog.dismiss();
                System.out.println("inside no result");
                System.out.println("number result details:" + getResult);
                AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
                builder.setMessage("Somethings went wrong please try after Sometime")
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                dialog.cancel();
                            }
                        })
                        //Set your icon here
                        .setTitle("ChopMobile")
                        .setIcon(R.drawable.biz_logo);
                AlertDialog alert = builder.create();
                alert.show();//showing the dialog
            }
        }
    }//Async Task Ends

}