package com.omkarsoft.chopmobile;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpStack;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.gson.Gson;
import com.pixplicity.easyprefs.library.Prefs;
import com.squareup.picasso.Picasso;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;

import HellpersClass.CHOPMOBILE_CONSTANTS;
import HellpersClass.CONNECTIONDETECTOR;
import HellpersClass.TLSSocketFactory;
import HellpersClass.Utils;
import other.Res;

import static com.omkarsoft.chopmobile.R.id.autoemail_login;
import static com.omkarsoft.chopmobile.R.id.login_button;


/**
 * Created by user1 on 2/3/2017.
 */

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener, ActivityCompat.OnRequestPermissionsResultCallback {

    /*----------------Facebook variables defination-----------------*/
    private static final String TAG = "";
    LoginButton loginButton;
    CallbackManager callbackManager;
    Uri imageuri;
    AccessTokenTracker accessTokenTracker;
    String username, firstname, lastname;
    TextView tv_profile_name;
    ImageView iv_profile_pic;

    /*----------------Gmail plus variable defination----------------*/
    TextView tv_username;
    GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN = 9001;

    /*-----------------Standard email authntication variables------------------*/
    TextView _forgotpassword_textview, signup_here;
    final Context c = this;
    AutoCompleteTextView _auto_email;
    EditText _password_editText;
    String email_string;
    String password_string;
    Button login_butt;
    Boolean isInternetPresent = false;
    // Connection detector class
    CONNECTIONDETECTOR cd;
    private ProgressDialog pDialog;
    private static String login_url;
    String jsonUser_id, jsonName, jsonEmail, jsonSkypeid, jsonMobile, jsonPassword;
    String result11, json_result;
    String EMAIL, PASSWORD, NAME, MOBILENO, LASTNAME, LOCATION, PINCODE, ADDRESS;
    public final static int PERM_REQUEST_CODE_DRAW_OVERLAYS = 1234;
    String LOGIN_TYPE, SOCIAL_EMAIL, SOCIAL_FIRSTNAME, SOCIAL_LASTNAME;
    CheckBox checkbox_rember;
    private Res res;
    ScrollView login_scroll_background;
    TextInputLayout email_text_input,password_text_input;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());

        setContentView(R.layout.activity_login);
        System.out.println("checlvalueshae---" + Prefs.getString(CHOPMOBILE_CONSTANTS.remeber_check, ""));



        if (Prefs.getString(CHOPMOBILE_CONSTANTS.remeber_check, "false").equals("true")) {
            if (!Prefs.getString(CHOPMOBILE_CONSTANTS.shared_user_id, "").isEmpty() && Prefs.getString(CHOPMOBILE_CONSTANTS.shared_user_id, "") != null) {
               // if (Prefs.getString(CHOPMOBILE_CONSTANTS.use_location_future, "").equals("1")){
//no need code                        && Prefs.getString(CHOPMOBILE_CONSTANTS.rest_open_status, "").equals("1")) {
                    Intent i = new Intent(LoginActivity.this, HomeActivity.class);
                    startActivity(i);
                    finish();
//                } else {
//                    Intent i = new Intent(LoginActivity.this, Selelct_Restauant.class);
//                    startActivity(i);
//                    finish();
//                }
            }
        }

        cd = new CONNECTIONDETECTOR(getApplicationContext());

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        EditText editText22 = (EditText) findViewById(R.id.edit_pass_login);
        login_butt = (Button) findViewById(R.id.login_butt);
        signup_here = (TextView) findViewById(R.id.signup_here);
        checkbox_rember = (CheckBox) findViewById(R.id.checkbox_rember);
        loginButton = (LoginButton) findViewById(login_button);
//        email_text_input=(TextInputLayout)findViewById(R.id.email_text_input);
//        password_text_input=(TextInputLayout)findViewById(R.id.text_input);

        signup_here.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(i);
            }
        });
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);

/*-------------------Forgot Text View with Dialogbox-------------------------------------*/
        _forgotpassword_textview = (TextView) findViewById(R.id.text_view_forgot_password);

//editText22.setBackgroundColor();
        _forgotpassword_textview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LayoutInflater layoutInflaterAndroid = LayoutInflater.from(c);
                View mView = layoutInflaterAndroid.inflate(R.layout.user_input_dialogbox_forgotpassword, null);
                AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(c);
                // ImageView image = (ImageView) findViewById(R.id.image);
                // image.setImageResource(R.drawable.ic_launcher);
                alertDialogBuilderUserInput.setView(mView);

                final EditText userInputDialogEditText = (EditText) mView.findViewById(R.id.userInputDialog);
                alertDialogBuilderUserInput
                        .setCancelable(false)
                        .setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {
                                // ToDo get user input here
                                Submit_Email(LoginActivity.this, userInputDialogEditText.getText().toString());
                                Prefs.putString(CHOPMOBILE_CONSTANTS.forget_email, userInputDialogEditText.getText().toString());
                            }
                        })

                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialogBox, int id) {
                                        dialogBox.cancel();
                                    }
                                });

                AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
                alertDialogAndroid.show();
            }
        });

        _auto_email = (AutoCompleteTextView) findViewById(autoemail_login);
        _password_editText = (EditText) findViewById(R.id.edit_pass_login);

//        //changeing color
        _auto_email.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")));
        _password_editText.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")));
        _forgotpassword_textview.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")));
        signup_here.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")));
        checkbox_rember.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")));
        login_butt.setBackgroundColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_background_color,"")));
        login_butt.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_text_color,"")));
//        //changing color setting
        login_scroll_background=(ScrollView)findViewById(R.id.login_scroll_background);
        login_scroll_background.setBackgroundColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.background_app_color,"")));
//        email_text_input.setHintTextAppearance(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.transparent_text_color,"")));
//        password_text_input.setHintTextAppearance(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.transparent_text_color,"")));
//        password_text_input.getEditText().setHighlightColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.transparent_text_color,"")));


//        _auto_email.setText("adam.smith");
//        _password_editText.setText("admin");
        _password_editText.setImeActionLabel("Done", KeyEvent.KEYCODE_ENTER);

        _password_editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    Login_Method();
                }
                return false;
            }
        });

        login_butt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent i=new Intent(LoginActivity.this,HomeActivity.class);
//                startActivity(i);
                Login_Method();

            }
        });


        tv_profile_name = (TextView) findViewById(R.id.tv_profile_name);
        iv_profile_pic = (ImageView) findViewById(R.id.iv_profile_pic);




        loginButton.setReadPermissions(Arrays.asList("public_profile, email, user_birthday, user_friends"));


        callbackManager = CallbackManager.Factory.create();

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                LOGIN_TYPE = "Facebook";
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {


                                try {
                                    SOCIAL_EMAIL = object.getString("email");
                                    String birthday = object.getString("birthday");
                                    String id = object.getString("id");
                                    SOCIAL_FIRSTNAME = object.getString("name");
                                    tv_profile_name.setText(SOCIAL_FIRSTNAME);

                                    Prefs.putString(CHOPMOBILE_CONSTANTS.profile_full_name, SOCIAL_FIRSTNAME);
                                    Prefs.putString(CHOPMOBILE_CONSTANTS.profile_email, SOCIAL_EMAIL);


                                    String imageurl = "https://graph.facebook.com/" + id + "/picture?type=large";

                                    Picasso.with(LoginActivity.this).load(imageurl).into(iv_profile_pic);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                new Login_user1().execute();
                            }
                        });


                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender, birthday");
                request.setParameters(parameters);
                request.executeAsync();


/**
 * AccessTokenTracker to manage logout
 */
                accessTokenTracker = new AccessTokenTracker() {
                    @Override
                    protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken,
                                                               AccessToken currentAccessToken) {
                        // updateWithToken(currentAccessToken);

                        if (currentAccessToken == null) {
                            tv_profile_name.setText("");
                            iv_profile_pic.setImageResource(R.drawable.ic_launcher);

                        }


                        Log.i(TAG, "-------------log out ");
                    }
                };


            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });

/*-------------Gmail plus code goes here*------------------*/
        tv_username = (TextView) findViewById(R.id.tv_username);

        //Register both button and add click listener
        findViewById(R.id.sign_in_button).setOnClickListener(this);
        findViewById(R.id.btn_logout).setOnClickListener(this);


        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        //new Login_user().execute();

    }

   /* @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);


     *//* *//**//* G plus onActivityresult method content*//*
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }

    }*/


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.sign_in_button:

                signIn();

                break;
            case R.id.btn_logout:

                signOut();

                break;
        }

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void signOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        tv_username.setText("");
                    }
                });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);


     /* *//* G plus onActivityresult method content*/
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }

    }

   /* @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }*/

    private void handleSignInResult(GoogleSignInResult result) {

        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            LOGIN_TYPE = "Googleplus";
            //Toast.makeText(LoginActivity.this,acct.getEmail()+acct.getDisplayName()+acct.getPhotoUrl()+acct.getId()+acct.getGivenName(),Toast.LENGTH_LONG).show();
            SOCIAL_FIRSTNAME = acct.getDisplayName();
            SOCIAL_EMAIL = acct.getEmail();
            SOCIAL_LASTNAME = acct.getGivenName();
            Prefs.putString(CHOPMOBILE_CONSTANTS.profile_full_name, SOCIAL_FIRSTNAME + SOCIAL_LASTNAME);
            Prefs.putString(CHOPMOBILE_CONSTANTS.profile_email, SOCIAL_EMAIL);
            new Login_user1().execute();
            tv_username.setText(getString(R.string.signed_in_fmt, acct.getDisplayName()));

        } else {
            // Signed out, show unauthenticated UI.
            // updateUI(false);
        }
    }

    public boolean emailValidator(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }
    private void showProgressDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }
    private void hideProgressDialog() {
        if (pDialog.isShowing())
            pDialog.hide();
    }

    private class Login_user extends AsyncTask<String, Void, String> {

        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... URL) {
            HttpStack stack = null;
            try {
                stack = new HurlStack(null, new TLSSocketFactory());

                login_url = Utils.DOMAIN_NAME;
                //  String login_url = Utils.GIFT_PRODUCTS_NEW;
                System.out.println("producturl" + login_url);
                HttpClient httpclient = new DefaultHttpClient();
                System.out.println("post url:-" + login_url);
                HttpPost httppost = new HttpPost(login_url);
                System.out.println("post namevaluepair name" + email_string);
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                nameValuePairs.add(new BasicNameValuePair("page_name", "login"));
                nameValuePairs.add(new BasicNameValuePair("username", email_string));
                nameValuePairs.add(new BasicNameValuePair("password", password_string));
                nameValuePairs.add(new BasicNameValuePair("device", "ANDROID"));
                nameValuePairs.add(new BasicNameValuePair("device_id",Utils.getDeviceId(LoginActivity.this)));
                nameValuePairs.add(new BasicNameValuePair("rest",Prefs.getString(CHOPMOBILE_CONSTANTS.Restaurant_Id,"")));

                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                System.out.println("post all namevalueslogin--" + nameValuePairs);

                // Execute HTTP Post Request
                org.apache.http.HttpResponse response = httpclient.execute(httppost);

                BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                sb.append(reader.readLine() + "\n");
                String line = "0";
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                reader.close();
                result11 = sb.toString();
                System.out.println("post result19:-" + result11);
                // parsing data

            } catch (Exception e) {
                e.printStackTrace();

            }
            return result11;
        }

        protected void onPostExecute(String getResult) {
            super.onPostExecute(getResult);
            System.out.println("logininside post");
            System.out.println("login post result:-" + getResult);
            if (getResult != null) {
                JSONObject jObject = null;
                try {
                    jObject = new JSONObject(getResult);
                    System.out.println("loginjson object" + jObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    json_result = jObject.getString("success");
                    System.out.println("loginjson reg" + json_result);
                    if (json_result.equals("1")) {
                        System.out.println("inside json success");
                        hideProgressDialog();
//                        System.out.println("jinteger--" + jObject.getInt("user_id"));
//                        System.out.println("jstring--" + jObject.getString("user_id"));
//                        jsonUser_id = jObject.getString("user_id");
//                        jsonName = jObject.getString("user_name");
//                        jsonEmail = _auto_email.getText().toString();
//                        System.out.println( "inside success mail"+jsonEmail);
                        System.out.println("jsonobjectvalue---" + jObject.getString("userid"));

                        Prefs.putString(CHOPMOBILE_CONSTANTS.shared_user_id, jObject.getString("userid"));
                        if (checkbox_rember.isChecked()) {
                            System.out.println("cumming inside of chedck");
                            Prefs.putString(CHOPMOBILE_CONSTANTS.remeber_check, "true");
                        }
                        Toast.makeText(LoginActivity.this, "Login Successful", Toast.LENGTH_SHORT).show();
//                        Prefs.putString(CHOPMOBILE_CONSTANTS.shared_user_id, jsonUser_id);
//                        Prefs.putString(CHOPMOBILE_CONSTANTS.shared_user_name, jsonName);
                        Prefs.putString(CHOPMOBILE_CONSTANTS.profile_full_name, jObject.getString("full_name"));
                        Prefs.putString(CHOPMOBILE_CONSTANTS.profile_email, email_string);


                        Intent home_int = new Intent(LoginActivity.this, HomeActivity.class);
                        startActivity(home_int);
                        finish();

                    } else if (json_result.equals("0")) {
                        hideProgressDialog();
                        Toast.makeText(getApplicationContext(), "Invalid Email or Password", Toast.LENGTH_SHORT).show();
                    } else {
                        hideProgressDialog();
                        Toast.makeText(getApplicationContext(), "Sorry! There is a Server Issue. please Try After Sometime", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } else {
                hideProgressDialog();
                System.out.println("inside no result");
                System.out.println("no result details:" + getResult);
                AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                builder.setMessage("Somethings went wrong please try after Sometime")
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                dialog.cancel();
                            }
                        })
                        //Set your icon here
                        .setTitle("ChopMobile")
                        .setIcon(R.drawable.ic_launcher);
                AlertDialog alert = builder.create();
                alert.show();//showing the dialog
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        pDialog.dismiss();
    }

    private class Login_user1 extends AsyncTask<String, Void, String> {

        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... URL) {

            try {
                HostnameVerifier hostnameVerifier = org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;

                DefaultHttpClient client = new DefaultHttpClient();

                SchemeRegistry registry = new SchemeRegistry();
                SSLSocketFactory socketFactory = SSLSocketFactory.getSocketFactory();
                socketFactory.setHostnameVerifier((X509HostnameVerifier) hostnameVerifier);
                registry.register(new Scheme("https", socketFactory, 443));
                SingleClientConnManager mgr = new SingleClientConnManager(client.getParams(), registry);
                DefaultHttpClient httpClient = new DefaultHttpClient(mgr, client.getParams());

// Set verifier
                HttpsURLConnection.setDefaultHostnameVerifier(hostnameVerifier);

                login_url = Utils.DOMAIN_NAME;
                HttpClient httpclient = new DefaultHttpClient();
                System.out.println("post url:-" + login_url);
                HttpPost httppost = new HttpPost(login_url);
                System.out.println("post namevaluepair name" + EMAIL);
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                nameValuePairs.add(new BasicNameValuePair("page_name", "do_login"));
                nameValuePairs.add(new BasicNameValuePair("device", "ANDROID"));
                nameValuePairs.add(new BasicNameValuePair("device_id", Utils.getDeviceId(LoginActivity.this)));
                nameValuePairs.add(new BasicNameValuePair("login_type", LOGIN_TYPE));
                nameValuePairs.add(new BasicNameValuePair("email", SOCIAL_EMAIL));
                nameValuePairs.add(new BasicNameValuePair("first_name", SOCIAL_FIRSTNAME));
                nameValuePairs.add(new BasicNameValuePair("last_name", SOCIAL_LASTNAME));
                nameValuePairs.add(new BasicNameValuePair("rest",Prefs.getString(CHOPMOBILE_CONSTANTS.Restaurant_Id,"")));

                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                System.out.println("post all namevalues" + nameValuePairs);

                // Execute HTTP Post Request
                org.apache.http.HttpResponse response = httpclient.execute(httppost);

                BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                sb.append(reader.readLine() + "\n");
                String line = "0";
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                reader.close();
                result11 = sb.toString();
                System.out.println("post result19:-" + result11);
                // parsing data

            } catch (Exception e) {
                e.printStackTrace();

            }
            return result11;
        }

        protected void onPostExecute(String getResult) {
            super.onPostExecute(getResult);
            System.out.println("logininside post");
            System.out.println("login post result:-" + getResult);
            if (getResult != null) {
                JSONObject jObject = null;
                try {
                    jObject = new JSONObject(getResult);
                    System.out.println("loginjson object" + jObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    json_result = jObject.getString("success");
                    System.out.println("loginjson reg" + json_result);
                    if (json_result.equals("1")) {
                        System.out.println("inside json success" + jObject.getJSONArray("user_details").getJSONObject(0).getString("user_id").toString());

                        Prefs.putString(CHOPMOBILE_CONSTANTS.shared_user_id, jObject.getJSONArray("user_details").getJSONObject(0).getString("user_id").toString());
                        Prefs.putString(CHOPMOBILE_CONSTANTS.profile_full_name, jObject.getJSONArray("user_details").getJSONObject(0).getString("full_name").toString());
                        Prefs.putString(CHOPMOBILE_CONSTANTS.profile_email, SOCIAL_EMAIL);
                        Prefs.putString(CHOPMOBILE_CONSTANTS.remeber_check, "true");
//                        Intent i = new Intent(LoginActivity.this, HomeActivity.class);
//                        startActivity(i);
//                        finish();
                        hideProgressDialog();

//                        System.out.println("jstring--"+jObject.getString("userId"));
//                        jsonUser_id = jObject.getString("userId");
//                        jsonName = jObject.getString("userName");
//                        System.out.println( "inside success"+jsonUser_id);

                        Toast.makeText(LoginActivity.this, "Login Success", Toast.LENGTH_SHORT).show();

//                        Prefs.putString(INKTALK_APP_CONSTANT.shared_user_name, jsonName);


                        Intent home_int1 = new Intent(LoginActivity.this, HomeActivity.class);
                        startActivity(home_int1);
                        finish();
                    } else if (json_result.equals("0")) {
                        hideProgressDialog();
                        Toast.makeText(getApplicationContext(), "Invalid UserId or Password", Toast.LENGTH_SHORT).show();
                    } else {
                        hideProgressDialog();
                        Toast.makeText(getApplicationContext(), "Sorry! There is a Server Issue. please Try After Sometime", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            } else {
                hideProgressDialog();
                System.out.println("inside no result");
                System.out.println("no result details:" + getResult);
                AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                builder.setMessage("Somethings went wrong please try after Sometime")
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                dialog.cancel();
                            }
                        })
                        //Set your icon here
                        .setTitle("InkTalks")
                        .setIcon(R.drawable.ic_launcher);
                AlertDialog alert = builder.create();
                alert.show();//showing the dialog
            }
        }
    }

    public void Login_Method() {
        email_string = _auto_email.getText().toString();
        password_string = _password_editText.getText().toString();
        if (email_string.equals("")) {
            Toast.makeText(getApplicationContext(), "Email ID is Required", Toast.LENGTH_SHORT).show();
        } else if (password_string.equals("")) {
            Toast.makeText(getApplicationContext(), "Password is Required", Toast.LENGTH_SHORT).show();
        }
// else if (!emailValidator(email_string)) {
//                    Toast.makeText(getApplicationContext(), "Email Id Invalid", Toast.LENGTH_SHORT).show();
//                }
        else {
            isInternetPresent = cd.isConnectingToInternet();
            // check for Internet status
            if (isInternetPresent) {
                // Internet Connection is Present
                // make HTTP requests
                showProgressDialog();
                new Login_user().execute();
            } else {
                // Internet connection is not present
                // Ask user to connect to Internet
//                        showAlertDialog(AndroidDetectInternetConnectionActivity.this, "No Internet Connection",
//                                "You don't have internet connection.", false);
                AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                builder.setMessage("No Internet Connection Please Check Your Internet connection.")
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                dialog.cancel();
                            }
                        })
                        //Set your icon here
                        .setTitle("ChopMobile")
                        .setIcon(R.drawable.ic_launcher);
                AlertDialog alert = builder.create();
                alert.show();//showing the dialog
            }
        }
    }

    //Permission Request for Marshmallow
    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG, "Permission is granted");
                return true;
            } else {

                Log.v(TAG, "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG, "Permission is granted");
            return true;
        }
    }

    public void Submit_Email(Context context, final String email) {
        // mPostCommentResponse.requestStarted();
        showProgressDialog();
        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest sr = new StringRequest(Request.Method.POST, Utils.DOMAIN_NAME, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //mPostCommentResponse.requestCompleted();
                System.out.println("deleteresponsestring123---" + response.toString());
                aie_fetch_delete_details(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // mPostCommentResponse.requestEndedWithError(error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("page_name", "identity_forgot_pass");
                params.put("email", email);
                params.put("rest",Prefs.getString("",Prefs.getString(CHOPMOBILE_CONSTANTS.Restaurant_Id,"")));

                // params.put("quantity", menu_quantity);

                System.out.println("addparamscheckout---" + params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(sr);
    }

    public void aie_fetch_delete_details(String jsonObject) {
        Gson gson = new Gson();
        hideProgressDialog();
        try {
            JSONObject jobj = new JSONObject(jsonObject);
            System.out.println("josobjectfromser--" + jobj);
            if (jobj.getString("success").equals("1")) {
                Toast.makeText(LoginActivity.this, jobj.getString("message").toString(), Toast.LENGTH_LONG).show();

                LayoutInflater layoutInflaterAndroid = LayoutInflater.from(c);
                View mView = layoutInflaterAndroid.inflate(R.layout.reset_password, null);
                AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(c);
                // ImageView image = (ImageView) findViewById(R.id.image);
                // image.setImageResource(R.drawable.ic_launcher);
                alertDialogBuilderUserInput.setView(mView);

                final EditText random_string = (EditText) mView.findViewById(R.id.otp_edit);
                final EditText new_password = (EditText) mView.findViewById(R.id.password_edit);
                alertDialogBuilderUserInput
                        .setCancelable(false)
                        .setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {
                                // ToDo get user input here
                                Reset_Password(LoginActivity.this, random_string.getText().toString(), new_password.getText().toString());
                            }
                        })

                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialogBox, int id) {
                                        dialogBox.cancel();
                                    }
                                });

                AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
                alertDialogAndroid.show();
            } else {
                Toast.makeText(LoginActivity.this, jobj.getString("message").toString(), Toast.LENGTH_LONG).show();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void Reset_Password(Context context, final String email, final String new_password) {
        // mPostCommentResponse.requestStarted();
        showProgressDialog();
        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest sr = new StringRequest(Request.Method.POST, Utils.DOMAIN_NAME, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //mPostCommentResponse.requestCompleted();
                System.out.println("deleteresponsestring123---" + response.toString());
                aie_fetch_delete_details1(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // mPostCommentResponse.requestEndedWithError(error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("page_name", "update_app_password");
                params.put("pass_ran", email);
                params.put("new_pass", new_password);
                params.put("email", Prefs.getString(CHOPMOBILE_CONSTANTS.forget_email, ""));
                params.put("rest",Prefs.getString("",Prefs.getString(CHOPMOBILE_CONSTANTS.Restaurant_Id,"")));
                // params.put("quantity", menu_quantity);

                System.out.println("addparamscheckout---" + params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(sr);
    }

    public void aie_fetch_delete_details1(String jsonObject) {
        Gson gson = new Gson();
        hideProgressDialog();
        try {
            JSONObject jobj = new JSONObject(jsonObject);
            System.out.println("josobjectfromser--" + jobj);
            if (jobj.getString("success").equals("1")) {
                Toast.makeText(LoginActivity.this, jobj.getString("message").toString(), Toast.LENGTH_LONG).show();

            } else {
                Toast.makeText(LoginActivity.this, jobj.getString("message").toString(), Toast.LENGTH_LONG).show();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    @Override public Resources getResources() {
        if (res == null) {
            res = new Res(super.getResources());
        }
        return res;
    }
}
/*
    GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
    GoogleSignInAccount acct = result.getSignInAccount();
    String personName = acct.getDisplayName();
    String personGivenName = acct.getGivenName();
    String personFamilyName = acct.getFamilyName();
    String personEmail = acct.getEmail();
    String personId = acct.getId();
    Uri personPhoto = acct.getPhotoUrl();*/
