package com.omkarsoft.chopmobile;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import Adapters.Offers_Adapter;
import HellpersClass.CHOPMOBILE_CONSTANTS;
import HellpersClass.Utils;
import Model.OFFERS;

/**
 * Created by user1 on 19-Apr-17.
 */

public class Offers extends AppCompatActivity{
    RecyclerView recyclerView;
    ArrayList<OFFERS> speakers_list = new ArrayList<OFFERS>();
    Offers_Adapter redesadapter;
    RelativeLayout abtus_back_imgvw;
    TextView actionbar_text;
    private ProgressDialog pDialog;
    private String tag_json_obj = "jobj_req";
    private String TAG = OrderHistory.class.getSimpleName();
    JSONArray odr_his_jArray;
    private int previousTotal = 0;
    private boolean loading = true;
    private int visibleThreshold = 10;
    int firstVisibleItem, visibleItemCount, totalItemCount;
    LinearLayoutManager mLayoutManager;
    private Tracker mTracker;
    RelativeLayout history_back_image;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.speakers);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Offers");

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);

        history_back_image=(RelativeLayout)findViewById(R.id.history_back_image);
        recyclerView = (RecyclerView) findViewById(R.id.speaker_list);
        mLayoutManager = new LinearLayoutManager(Offers.this);
        recyclerView.setLayoutManager(mLayoutManager);

        //background
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.actionbar_color,""))));
        history_back_image.setBackgroundColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.background_app_color,"")));

        LoadPrivacy(Offers.this);

    }

    public void LoadPrivacy(Context context) {
        // mPostCommentResponse.requestStarted();
        showProgressDialog();
        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest sr = new StringRequest(Request.Method.POST, Utils.DOMAIN_NAME, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //mPostCommentResponse.requestCompleted();
                System.out.println("deleteresponsestring123---" + response.toString());
                aie_fetch_speakers_details(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // mPostCommentResponse.requestEndedWithError(error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("page_name", "promotion");
               // params.put("location_id",Prefs.getString(CHOPMOBILE_CONSTANTS.location_id,""));
                params.put("rest",Prefs.getString("",Prefs.getString(CHOPMOBILE_CONSTANTS.Restaurant_Id,"")));

                // params.put("quantity", menu_quantity);

                System.out.println("addparamscheckout---" + params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(sr);
    }



    private void showProgressDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideProgressDialog() {
        if (pDialog.isShowing())
            pDialog.hide();
    }

    private void aie_fetch_speakers_details(String jsonObject) {
        Gson gson = new Gson();
        try {
            JSONObject jobj = new JSONObject(jsonObject);
            hideProgressDialog();
            if(jobj.getString("success").equals("1")) {
                this.odr_his_jArray = jobj.getJSONArray("promotion");
                System.out.println("speaker_jArray.length()" + this.odr_his_jArray.length());
                for (int histlistidx = 0; histlistidx < this.odr_his_jArray.length(); histlistidx++) {
                    this.speakers_list.add((OFFERS) gson.fromJson(this.odr_his_jArray.getJSONObject(histlistidx).toString(), OFFERS.class));
                }
                redesadapter = new Offers_Adapter(Offers.this, speakers_list);
                recyclerView.setAdapter(redesadapter);
            }
            else{
                Toast.makeText(Offers.this,jobj.getString("message").toString(),Toast.LENGTH_LONG).show();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        pDialog.dismiss();
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //Toast.makeText(getApplicationContext(),"Password Reset Success", Toast.LENGTH_SHORT).show();
                onBackPressed();

                return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed() {
//        AppController.getInstance().clearApplicationData();
//        Intent i=new Intent(Offers.this,HomeActivity.class);
//        startActivity(i);
        finish();
    }
}
