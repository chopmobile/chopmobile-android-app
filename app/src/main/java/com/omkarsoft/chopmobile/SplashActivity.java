package com.omkarsoft.chopmobile;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import HellpersClass.CHOPMOBILE_CONSTANTS;
import Model.Settings;
import other.AppController;

public class SplashActivity extends AppCompatActivity {

    long Delay = 5000;
    String tag_json_obj = "json_obj_req";
    ArrayList<Settings> Settings_List = new ArrayList<Settings>();
    JSONArray odr_his_jArray;
    NetworkImageView splash_image;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splash);

        //Constant Rest Id
        Prefs.putString(CHOPMOBILE_CONSTANTS.Restaurant_Id,"testdemo-restaurant");
      //  Prefs.putString(CHOPMOBILE_CONSTANTS.Restaurant_Id,"");

        splash_image=(NetworkImageView) findViewById(R.id.img1);
        String ur1="https://chopmobile.com/chopadmin/appdata.php";
        LoadSetting(ur1);
       // Splas();


    }
    public  void LoadSetting(String url){
        // mPostCommentResponse.requestStarted();
        System.out.println("cuming inside url"+url);
        RequestQueue queue = Volley.newRequestQueue(SplashActivity.this);
        StringRequest sr = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //mPostCommentResponse.requestCompleted();
                System.out.println("responsestring123---"+response.toString());
                aie_fetch_event_details(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("volleyerrorplus----"+error);
                // mPostCommentResponse.requestEndedWithError(error);
            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("page_name","settings");
                params.put("rest",Prefs.getString("",Prefs.getString(CHOPMOBILE_CONSTANTS.Restaurant_Id,"")));



                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(sr);
        sr.setShouldCache(false);
    }
    public void aie_fetch_event_details(String jsonObject) {
        Gson gson = new Gson();
        try {
            JSONObject jobj=new JSONObject(jsonObject);
            this.odr_his_jArray = jobj.getJSONArray("category");
            System.out.println("speaker_jArray.length()" + this.odr_his_jArray.length());
            for (int histlistidx = 0; histlistidx < this.odr_his_jArray.length(); histlistidx++) {
                this.Settings_List.add((Settings) gson.fromJson(this.odr_his_jArray.getJSONObject(histlistidx).toString(), Settings.class));
                System.out.println("spalshscreenimage--"+Settings_List.get(histlistidx).getFlash_screen());
                ImageLoader imageLoader = AppController.getInstance().getImageLoader();
                splash_image.setImageUrl(Settings_List.get(histlistidx).getFlash_screen(), imageLoader);

//                Prefs.putString(CHOPMOBILE_CONSTANTS.transparent_app_color,Settings_List.get(histlistidx).getTransparent_app_color());
//                Prefs.putString(CHOPMOBILE_CONSTANTS.transparent_text_color,Settings_List.get(histlistidx).getTransparent_text_color());
                Prefs.putString(CHOPMOBILE_CONSTANTS.background_app_color,Settings_List.get(histlistidx).getBackground_app_color());
                Prefs.putString(CHOPMOBILE_CONSTANTS.font_color,Settings_List.get(histlistidx).getApp_text_color());
                Prefs.putString(CHOPMOBILE_CONSTANTS.button_text_color,Settings_List.get(histlistidx).getBackground_button_text_color());
                Prefs.putString(CHOPMOBILE_CONSTANTS.button_background_color,Settings_List.get(histlistidx).getBackground_button_image_color());
                Prefs.putString(CHOPMOBILE_CONSTANTS.background_image,Settings_List.get(histlistidx).getBackground_image());
                Prefs.putString(CHOPMOBILE_CONSTANTS.application_logo,Settings_List.get(histlistidx).getApp_logo());
                Prefs.putString(CHOPMOBILE_CONSTANTS.actionbar_color,Settings_List.get(histlistidx).getApp_topbar_color());


            }

            Splas();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void Splas(){
        Timer RunSplash = new Timer();

        // Task to do when the timer ends
        TimerTask ShowSplash = new TimerTask() {
            @Override
            public void run() {
                // Close SplashScreenActivity.class
                finish();

                // Start MainActivity.class
                Intent myIntent = new Intent(SplashActivity.this,
                        LoginActivity.class);
                startActivity(myIntent);
            }
        };

        // Start the timer
        RunSplash.schedule(ShowSplash, Delay);
    }
}