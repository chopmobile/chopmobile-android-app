package com.omkarsoft.chopmobile;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.pixplicity.easyprefs.library.Prefs;
import com.stripe.wrap.pay.AndroidPayConfiguration;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import HellpersClass.CHOPMOBILE_CONSTANTS;
import HellpersClass.Utils;

import static android.Manifest.permission.CALL_PHONE;
import static com.omkarsoft.chopmobile.HomeActivity.navItemIndex;

/**
 * Created by user1 on 10-Apr-17.
 */

public class Home_Delivery extends AppCompatActivity {
    private ProgressDialog pDialog;
    String DELIVERIY_TYPE;
    LinearLayout takeaway_lay, homedelivery_lay;
    TextView rest_address, full_name, email, address, phonenumer, rest_adess_text, Address2;
    Button pay_now;
    RadioGroup pay_opttion;
    RadioButton paypal, square;
    String TOTAL_PRICE;
    String result11;
    private ProgressDialog mProgressDialog;
    String pay_money_url;
    MenuItem textView, action_search;
    String Phone_number;
    static final int PURCHASE_REQUEST = 37;
    private static final String EXTRA_PRICE_PAID = "EXTRA_PRICE_PAID";
    // Put your publishable key here. It should start with "pk_test_"
    private static final String PUBLISHABLE_KEY =
            Prefs.getString(CHOPMOBILE_CONSTANTS.stripe_publishable_key, "");

    public static Intent createPurchaseCompleteIntent(long amount) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra(EXTRA_PRICE_PAID, amount);
        return returnIntent;
    }

    LinearLayout home_delivery_backgroundcol, home_delivery_backgroundcol2;
    TextView select_payment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment_page);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("You're Almost Done");
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.actionbar_color, ""))));

        mProgressDialog = new ProgressDialog(Home_Delivery.this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        takeaway_lay = (LinearLayout) findViewById(R.id.take_away);
        homedelivery_lay = (LinearLayout) findViewById(R.id.home_delivery);
        rest_address = (TextView) findViewById(R.id.rest_address);
        full_name = (TextView) findViewById(R.id.full_name);
        email = (TextView) findViewById(R.id.email_id);
        address = (TextView) findViewById(R.id.Delivery_Address);
        phonenumer = (TextView) findViewById(R.id.phone_number);
        pay_opttion = (RadioGroup) findViewById(R.id.pay_option);
        paypal = (RadioButton) findViewById(R.id.paypal);
        square = (RadioButton) findViewById(R.id.square);
        pay_now = (Button) findViewById(R.id.pay_now);
        home_delivery_backgroundcol = (LinearLayout) findViewById(R.id.home_delivery_backgroundcol);
        home_delivery_backgroundcol2 = (LinearLayout) findViewById(R.id.home_delivery_backgroundcol2);
        select_payment = (TextView) findViewById(R.id.select_payment);
        rest_adess_text = (TextView) findViewById(R.id.rest_adess_text);
        Address2 = (TextView) findViewById(R.id.Address2);
        //back color
        home_delivery_backgroundcol.setBackgroundColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.background_app_color, "")));
        home_delivery_backgroundcol2.setBackgroundColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.background_app_color, "")));
        pay_now.setBackgroundColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_background_color, "")));
        pay_now.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_text_color, "")));
        full_name.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color, "")));
        email.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color, "")));
        address.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color, "")));
        phonenumer.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color, "")));
        paypal.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color, "")));
        ColorStateList colorStateList = new ColorStateList(
                new int[][]{

                        new int[]{-android.R.attr.state_enabled}, //disabled
                        new int[]{android.R.attr.state_enabled} //enabled
                },
                new int[] {

                        Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")) //disabled
                        ,Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")) //enabled

                }
        );
        paypal.setButtonTintList(colorStateList);
        square.setButtonTintList(colorStateList);
        square.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color, "")));
        rest_address.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color, "")));
        rest_adess_text.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color, "")));
        Address2.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color, "")));

        //stripe
        initAndroidPay();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            DELIVERIY_TYPE = bundle.getString("delivery_type");
            System.out.println("deliver_intent" + DELIVERIY_TYPE);
            TOTAL_PRICE = bundle.getString("total_amount");
            Prefs.putString(CHOPMOBILE_CONSTANTS.stripe_amount, TOTAL_PRICE);
            if (DELIVERIY_TYPE.equals("1")) {
                takeaway_lay.setVisibility(View.VISIBLE);
                homedelivery_lay.setVisibility(View.GONE);
            } else {
                takeaway_lay.setVisibility(View.GONE);
                homedelivery_lay.setVisibility(View.VISIBLE);
            }
        }
        System.out.println("totalprice---" + TOTAL_PRICE);
        if (TOTAL_PRICE.equals("0.00")) {
            paypal.setVisibility(View.GONE);
            square.setVisibility(View.GONE);
            select_payment.setVisibility(View.GONE);
        } else {
        }
        pDialog = new ProgressDialog(Home_Delivery.this);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
        pay_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Float.parseFloat(TOTAL_PRICE) > 0.0) {
                    if (paypal.isChecked()) {
                        Intent i = new Intent(Home_Delivery.this, SampleActivity.class);
                        i.putExtra("total_amount", TOTAL_PRICE);
                        i.putExtra("delivery_type", DELIVERIY_TYPE);
                        startActivity(i);
                    } else if (square.isChecked()) {
                        Intent i = new Intent(Home_Delivery.this, PaymentActivity.class);
                        startActivity(i);
//                        CartManager cartManager = new CartManager();
//                        try {
//                            Cart cart = cartManager.buildCart();
//                            Intent paymentLaunchIntent = PaymentActivity1.createIntent(Home_Delivery.this, cart);
//                            Home_Delivery.this.startActivityForResult(
//                                    paymentLaunchIntent, Home_Delivery.PURCHASE_REQUEST);
//                        } catch (CartContentException unexpected) {
//                            // There shouldn't be any cart content exceptions because we used the CartManager tools.
//                        }
                    } else {
                        Toast.makeText(Home_Delivery.this, "Please select any payment method", Toast.LENGTH_LONG).show();
                    }
                } else {
                    mProgressDialog.show();
                    new TALK_TO_SERVER().execute();

                }
            }
        });

        LoadAddress(Home_Delivery.this);

        LoadPaymentApi(Home_Delivery.this);
    }

    public void LoadAddress(Context context) {
        // mPostCommentResponse.requestStarted();
        showProgressDialog();
        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest sr = new StringRequest(Request.Method.POST, Utils.DOMAIN_NAME, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //mPostCommentResponse.requestCompleted();
                System.out.println("deleteresponsestring123---" + response.toString());
                aie_fetch_delete_details(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // mPostCommentResponse.requestEndedWithError(error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("page_name", "payment");

                params.put("delivery_type", DELIVERIY_TYPE);
                params.put("user_id", Prefs.getString(CHOPMOBILE_CONSTANTS.shared_user_id, ""));
                //params.put("location_id", Prefs.getString(CHOPMOBILE_CONSTANTS.location_id, ""));
                params.put("rest",Prefs.getString("",Prefs.getString(CHOPMOBILE_CONSTANTS.Restaurant_Id,"")));

                System.out.println("addparamscheckout---" + params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(sr);
    }

    public void aie_fetch_delete_details(String jsonObject) {
        Gson gson = new Gson();
        hideProgressDialog();
        try {
            JSONObject jobj = new JSONObject(jsonObject);
            System.out.println("josobjectfromser--" + jobj);
            if (jobj.getString("success").equals("1")) {
                Phone_number = jobj.getString("phone_number").toString();
                if (DELIVERIY_TYPE.equals("1")) {
                    rest_address.setText(jobj.getString("contact_details").replace(",", "\n").toString());
                } else {
                    if (jobj.getString("fullname") != null && !jobj.getString("fullname").isEmpty())
                        full_name.setText(jobj.getString("fullname"));
                    if (jobj.getString("email") != null && !jobj.getString("email").isEmpty())
                        email.setText(jobj.getString("email"));
                    if (jobj.getString("user_address") != null && !jobj.getString("user_address").isEmpty() && !jobj.getString("user_address").equals("null")) {
                        address.setText(jobj.getString("user_address"));
                        Prefs.putString(CHOPMOBILE_CONSTANTS.customer_delivery_address, jobj.getString("user_address"));
                    }
                    if (jobj.getString("phone") != null && !jobj.getString("phone").isEmpty())
                        phonenumer.setText(jobj.getString("phone"));

                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void showProgressDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideProgressDialog() {
        if (pDialog.isShowing())
            pDialog.hide();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        pDialog.dismiss();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //Toast.makeText(getApplicationContext(),"Password Reset Success", Toast.LENGTH_SHORT).show();
                onBackPressed();

                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private class TALK_TO_SERVER extends AsyncTask<String, Void, String> {

        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... URL) {
            try {

                pay_money_url = Utils.DOMAIN_NAME;
                //pay_money_url = "http://maithreyaa.com/app/index.php?page=payment_process&product_id="+productId+"&product_name="+productDesc+"&product_price="+PRICE+"&payment_type=3&user_id="+ Prefs.getString(CHOPMOBILE_CONSTANTS.shared_user_id,"")+"&transcation_id="+transaction_id+"&payment_status=approved";

                HttpClient httpclient = new DefaultHttpClient();
                System.out.println("sampletimepost_url:-" + pay_money_url);
                HttpPost httppost = new HttpPost(pay_money_url);
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);

                System.out.println("print_product_ur_id all namevalues" + nameValuePairs);
                nameValuePairs.add(new BasicNameValuePair("page_name", "orders"));
                nameValuePairs.add(new BasicNameValuePair("userid", Prefs.getString(CHOPMOBILE_CONSTANTS.shared_user_id, "")));
                nameValuePairs.add(new BasicNameValuePair("device_id", Utils.getDeviceId(Home_Delivery.this)));
                nameValuePairs.add(new BasicNameValuePair("delivery_address", address.getText().toString()));
                Bundle bundle = getIntent().getExtras();
                nameValuePairs.add(new BasicNameValuePair("delivery_type", bundle.getString("delivery_type")));
//                nameValuePairs.add(new BasicNameValuePair("location_id", Prefs.getString(CHOPMOBILE_CONSTANTS.location_id, "")));
                nameValuePairs.add(new BasicNameValuePair("loyalty_status", Prefs.getString(CHOPMOBILE_CONSTANTS.loyality_status, "")));
                nameValuePairs.add(new BasicNameValuePair("rest",Prefs.getString(CHOPMOBILE_CONSTANTS.Restaurant_Id,"")));

                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                System.out.println("final check values" + nameValuePairs);
                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                System.out.println("print_response" + response);
                BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                sb.append(reader.readLine() + "\n");
                String line = "0";
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                reader.close();
                result11 = sb.toString();
                System.out.println("samplefuelpost result19:-" + result11);
                // parsing data

            } catch (Exception e) {
                e.printStackTrace();

            }
            return result11;
        }

        protected void onPostExecute(String getResult) {
            super.onPostExecute(getResult);
            System.out.println("inside post");
            System.out.println("sampletime post result:-" + getResult);
//            Toast.makeText(Home_Delivery.this, "Order Success", Toast.LENGTH_LONG).show();

            if (getResult != null) {
                mProgressDialog.dismiss();
                System.out.println("cuming cuminginised sample post");

//                AlertDialog.Builder builder = new AlertDialog.Builder(Home_Delivery.this);
//                builder.setMessage("Your order has been placed successfully")
//                        .setCancelable(false)
//                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int id) {
//
//                                dialog.cancel();

                                Intent zz = new Intent(Home_Delivery.this, HomeActivity.class);
                           //    zz.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                            zz.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(zz);
//                              AppController.getInstance().clearApplicationData();
//                                finish();
//                            }
//                        })
//                        //Set your icon here
//                        .setTitle("ChopMobile")
//                        .setIcon(R.drawable.am_logo);
//                AlertDialog alert = builder.create();
//                alert.show();//showing the dialog


                //timeparseResult(getResult);

            } else {
                mProgressDialog.dismiss();
                System.out.println("inside no result");
                System.out.println("number result details:" + getResult);
                AlertDialog.Builder builder = new AlertDialog.Builder(Home_Delivery.this);
                builder.setMessage("Somethings went wrong please try after Sometime")
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                dialog.cancel();
                            }
                        })
                        //Set your icon here
                        .setTitle("ChopMobile")
                        .setIcon(R.drawable.ic_launcher);
                AlertDialog alert = builder.create();
                alert.show();//showing the dialog
            }
        }
    }//Async Task Ends

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {

            case 123:
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    onCall();
                } else {
                    Log.d("TAG", "Call Permission Not Granted");
                }
                break;

            default:
                break;
        }
    }

    public void onCall() {


        int permissionCheck = ContextCompat.checkSelfPermission(Home_Delivery.this, CALL_PHONE);

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    Home_Delivery.this,
                    new String[]{CALL_PHONE},
                    123);
        } else {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" + Phone_number));
//                if (ActivityCompat.checkSelfPermission(Contact_Us.this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
//                    // TODO: Consider calling
//                    //    ActivityCompat#requestPermissions
//                    // here to request the missing permissions, and then overriding
//                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//                    //                                          int[] grantResults)
//                    // to handle the case where the user grants the permission. See the documentation
//                    // for ActivityCompat#requestPermissions for more details.
//                    return;
//                }
            startActivity(callIntent);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        // show menu only when home fragment is selected
        if (navItemIndex == 0) {
            getMenuInflater().inflate(R.menu.delivery_main, menu);
            action_search = (MenuItem) menu.findItem(R.id.action_search);

            action_search.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {
                    onCall();
                    return false;
                }
            });

//            textView.setIcon(R.drawable.list_background_rounded);
        }

        // when fragment is notifications, load the menu created for notifications
        if (navItemIndex == 6) {
            getMenuInflater().inflate(R.menu.notifications, menu);
        }
        return true;
    }

    private void initAndroidPay() {
        AndroidPayConfiguration payConfiguration =
                AndroidPayConfiguration.init(PUBLISHABLE_KEY, "USD");
        payConfiguration.setPhoneNumberRequired(false);
        payConfiguration.setShippingAddressRequired(true);
    }

    public void LoadPaymentApi(Context context) {
        // mPostCommentResponse.requestStarted();
        showProgressDialog();
        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest sr = new StringRequest(Request.Method.POST, Utils.DOMAIN_NAME, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //mPostCommentResponse.requestCompleted();
                System.out.println("deleteresponsestring123---" + response.toString());
                aie_fetch_api_details(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // mPostCommentResponse.requestEndedWithError(error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("page_name", "payment_gateway_API_details");
                params.put("rest",Prefs.getString("",Prefs.getString(CHOPMOBILE_CONSTANTS.Restaurant_Id,"")));

                System.out.println("addparamscheckout---" + params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(sr);
    }

    public void aie_fetch_api_details(String jsonObject) {
        Gson gson = new Gson();
        hideProgressDialog();
        try {
            JSONObject jobj = new JSONObject(jsonObject);
            System.out.println("josobjectfromser--" + jobj);
            if (jobj.getString("success").equals("1")) {
                Prefs.putString(CHOPMOBILE_CONSTANTS.paypal_client_id, jobj.getString("client_secret").toString());
                Prefs.putString(CHOPMOBILE_CONSTANTS.stripe_publishable_key, jobj.getString("stripe_publishable_key").toString());
                Prefs.putString(CHOPMOBILE_CONSTANTS.stripe_secret_key, jobj.getString("stripe_secret_key").toString());
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
