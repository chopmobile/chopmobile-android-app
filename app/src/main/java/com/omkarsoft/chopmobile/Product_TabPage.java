package com.omkarsoft.chopmobile;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.text.Html;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pixplicity.easyprefs.library.Prefs;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import HellpersClass.CHOPMOBILE_CONSTANTS;
import HellpersClass.Utils;
import Model.CATEGORY;
import Model.Products;
import Model.Products1;
import Model.SubProduct;

import static HellpersClass.Utils.CART_REVIEW;
import static com.omkarsoft.chopmobile.R.id.cart_text;


/**
 * Created by user1 on 7/4/2016.
 */
public class Product_TabPage extends Activity implements ActionBar.TabListener{

    AlertDialog.Builder alertDialogBuilder;
    AlertDialog alertDialog;
    String text, json;
    int attrid;
    SharedPreferences sharedpreferences;
    public static final String MyLogin = "Login";
    public static ArrayList<SubProduct> Cart_Arraylist;
    public static ArrayList<SubProduct> Cart_Arraylistnew;
    RelativeLayout bottom_bar_rl;
    ImageView chkout_bottom, chkoutcart_buttom, chkout_top;
    static double item_total_cost = 0;
    static ImageView chkoutcart_top, cart_empty;
    TextView bottombar, cartitem_count, total_cost, topbar, top_cartitem_count,
            top_total_cost, cart_empty_tv, tv_nofav;
    static RelativeLayout Main_layout;
    static RelativeLayout View_layout;
    Products item_of_product;
    SubProduct item;
    private GridView myfavoritelist, cartproductlist;
    private ArrayList<SubProduct> favList_Array;
    private GiftAdapter FL_Adapter;
    private String user_id, PID;
    int POS;
    protected int DELETE_POS = -1;
    private final int INVALID = -1;
    private FVCartAdapter CART_adapt;
    String total, scost, result1 = "Gift Items", gamt, extraAmt, usedAmt;
    ArrayList<SubProduct> finaldata;
    String Total, W_cost, S_Cost, D_Cost, Ser_Cost, VAT_Cost, voucherAmount, giftamount, extrapick, usedgiftamt, payable;
    int value = 0;
    String result11, json_result;
    int attvalue;
    public static TextView cart_show_total,loyal_text;
    private ProgressDialog pDialog;
    public String PRODUCT_QUANTITY="null";
    String PAGE_NAME;
    //  public static TextView product_quantity;
    private int itemCount = 0;
    private BigDecimal checkoutAmount = new BigDecimal(BigInteger.ZERO);
    ImageView cart_icon,search_button;
    ActionBar bar;
    int position_value;
    MenuItem textView;
    LinearLayout home_linear_background;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gift);

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);

        bar = getActionBar();

        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            position_value= Integer.parseInt(extras.getString("POSITION_VALUE"));
            // and get whatever type user account id is

            System.out.println("tabvalueint---"+position_value);
        }

        sharedpreferences = getSharedPreferences(MyLogin, Context.MODE_PRIVATE);
        alertDialogBuilder = new AlertDialog.Builder(Product_TabPage.this);
        user_id = sharedpreferences.getString("user_id", "");
        favList_Array = new ArrayList<SubProduct>();
        ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.actionbar_color,""))));
        actionBar.setDisplayUseLogoEnabled(false);
        actionBar.setDisplayShowHomeEnabled(false);

        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_arrow_back_white);
        upArrow.setColorFilter(getResources().getColor(R.color.white_text), PorterDuff.Mode.SRC_ATOP);
        actionBar.setHomeAsUpIndicator(upArrow);


        gift_product();

        home_linear_background=(LinearLayout) findViewById(R.id.home_linear_background);
        //changing background color
        home_linear_background.setBackgroundColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.background_app_color,"")));

    }

    public boolean IsInternetPresent() {
        if (new Utils(Product_TabPage.this).isNetworkAvailable())
            return true;
        else
            return false;
    }

    public static void SlideToDown() {
        Animation slide = null;
        slide = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF,
                0.0f, Animation.RELATIVE_TO_SELF, 5.2f);

        slide.setDuration(1000);
        slide.setFillAfter(true);
        slide.setFillEnabled(true);
        // View_layout.startAnimation(slide);
        chkoutcart_top.startAnimation(slide);

        slide.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {

                chkoutcart_top.clearAnimation();
                //   View_layout.clearAnimation();
            }

        });

    }

    public static void SlideToAbove() {
        Animation slide = null;

        slide = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF,
                1.0f, Animation.RELATIVE_TO_SELF, 0.0f);

        slide.setDuration(300);
        slide.setFillAfter(true);
        slide.setFillEnabled(true);
        View_layout.startAnimation(slide);
        chkoutcart_top.startAnimation(slide);
        slide.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                // Main_layout.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                View_layout.clearAnimation();
                chkoutcart_top.clearAnimation();
            }

        });

    }

    public class getgiftamount extends AsyncTask<Void, Void, String> {

        protected void onPreExecute() {
        }

        protected String doInBackground(Void... params) {

            return getpaymentcost();
        }

        protected void onPostExecute(String result) {
            JSONObject jObj_main;
            try {

                jObj_main = new JSONObject(result);
                total = jObj_main.getString("cart_total");
                scost = jObj_main.getString("shipping");
                D_Cost = jObj_main.getString("default_discount");
                Ser_Cost = jObj_main.getString("service_tax");
                VAT_Cost = jObj_main.getString("vat_tax");
                W_cost = jObj_main.getString("wallet_balance");
                gamt = jObj_main.getString("giftable_amount");
                extraAmt = jObj_main.getString("gift_extra_pick");
                usedAmt = jObj_main.getString("used_gift_amount");
                payable = jObj_main.getString("grand_total");

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public String getpaymentcost() {
        String resultt = null;
        //String URL1 = URLs.CART_COST_URL;
        String URL1 = CART_REVIEW;
        String C_Code = "";

		/*for (int i = 0; countrylist_array.size() > i; i++) {
            if (USER.get("User_Country").equals(
					countrylist_array.get(i).getcountry_name()))
				C_Code = countrylist_array.get(i).getcountry_id().toString();
		}
		System.out.println(plist.toString());*/

        List<NameValuePair> pairs = new ArrayList<NameValuePair>();

        pairs.add(new BasicNameValuePair("device_id", Utils.getDeviceId(Product_TabPage.this)));
        pairs.add(new BasicNameValuePair("device", "ANDROID"));
        //pairs.add(new BasicNameValuePair("location_id",Prefs.getString(CHOPMOBILE_CONSTANTS.location_id,"")));
        pairs.add(new BasicNameValuePair("rest",Prefs.getString(CHOPMOBILE_CONSTANTS.Restaurant_Id,"")));

        System.out.println(pairs.toString());

        try {

            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(URL1);
            httpPost.setEntity(new UrlEncodedFormEntity(pairs));


            HttpResponse httpResponse = httpClient.execute(httpPost);

            resultt = EntityUtils.toString(httpResponse.getEntity());
            System.out.println("COST RESULT:" + resultt);
        } catch (Exception e) {

            e.printStackTrace();

            Log.e("COST CAL URL", "Exception : " + e.getMessage(), e);
        }

        return resultt;
    }

    void gift_product() {
        if (IsInternetPresent()) {
            // new getgiftamount().execute();
            new getGiftProductsTask().execute();

        } else {
            alertDialogBuilder
                    .setMessage("Internet/Mobile Data Not Available!")
                    .setCancelable(false)
                    .setPositiveButton("Retry",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    gift_product();
                                }
                            });
            alertDialog = alertDialogBuilder.create();
            alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            alertDialog.show();
        }
    }


    ArrayList<Products1> parsejson(String homeproducts) {

//        final int tabCount = bar.getTabCount();
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setTitle("Menu");
        bar.setTitle(Html.fromHtml("<font color='#ffffff'>\t\t\tMenu</font>"));



        bar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        /*if (bar.getNavigationMode() == ActionBar.NAVIGATION_MODE_TABS) {
            bar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
            bar.setDisplayOptions(ActionBar.DISPLAY_SHOW_TITLE, ActionBar.DISPLAY_SHOW_TITLE);
        } else {
            bar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
            bar.setDisplayOptions(0, ActionBar.DISPLAY_SHOW_TITLE);
        }*/
        try {

            if (homeproducts != null && homeproducts.length() > 0) {

                JSONObject jObj_main = new JSONObject(homeproducts);
//                System.out.println("jsoncategoyarray"+jObj_main);
//                SpannableString str = new SpannableString(jObj_main.getString("loyalty_points").toString());
//                str.setSpan(new ForegroundColorSpan(Color.RED), 0, str.length(), 0);
//                textView.setTitle("LP:"+str);




                if (!jObj_main.getString("success").equals("0")) {
                    JSONArray result = jObj_main.getJSONArray("category");
                    System.out.println("String is  ...........1"+result);
                    if (result.length() > 0) {
                        for (int i = 0; i < result.length(); i++) {
                            JSONObject cat = result.getJSONObject(i);
                            //int total = cat.getInt("total_categories");
                            //System.out.println("String is  ...........1" );
                            //JSONArray product = cat.getJSONArray("category_list");
                            //for (int j = 0; j < product.length(); j++) {
                            //System.out.println("String is...........2" );
                            JSONObject items = result
                                    .getJSONObject(i);
                            String s = items.getString("category_name");
                            System.out.println("String is" + s);
                            // total products count

                            final String text = s;
                            SpannableStringBuilder builder = new SpannableStringBuilder();

                            SpannableString redSpannable= new SpannableString(text);
                            redSpannable.setSpan(new ForegroundColorSpan(Color.WHITE), 0, builder.length(), 0);
                            builder.append(redSpannable);


                            bar.addTab(bar.newTab()
                                    .setText(builder)
                                    .setTabListener(new TabListener(new TabContentFragment(text))));
                            // bar.setStackedBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimary)));
                            CATEGORY cate=new CATEGORY();



                            //}

                        }
                        bar.setSelectedNavigationItem(position_value);
                    }
                }
            }
            //System.out.println("BE:" + favList_Array.size());
            //return favList_Array;
        } catch (JSONException e) {
            System.out.println(e);
        }
        return null;
    }

    public class GiftAdapter extends BaseAdapter {

        public Activity activity;
        private ArrayList<SubProduct> data;
        private LayoutInflater inflater = null;

        SharedPreferences sharedPreferences, pref;
        private ImageView imgThumbnail, plus, closeit, minus, favo,right_go;
        private TextView prod_name, prod_cost, prod_offer_cost, prod_desc,
                prod_quantity, count, tab1, tab2, tab3, tab4, tab5;
        JSONObject promoJson;
        //  HashMap<String, String> item = new HashMap<String, String>();
        private Context mContext;

        String control = "";
        Dialog dialog;
        Product_TabPage product_tabPage;
        public GiftAdapter(Activity a, ArrayList<SubProduct> d) {
            activity = a;
            data = d;
            this.mContext = a;
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            pref = PreferenceManager.getDefaultSharedPreferences(activity);
            finaldata = data;

        }

        public int getCount() {
            return data.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        class ViewHolder {
            private ImageView imgThumbnail, plus, more, fav,right_go;
            private TextView lblDateEnd, tvmrp, tvoffcost, tvProdName, tvPercentage,
                    prod_qty, cart_text;
            LinearLayout view;
            LinearLayout llOutofstock, llAddcart;
            FloatingActionButton fab_productlist;
            ImageView count,minus;
            TextView product_quantity123;
            LinearLayout product_sum_background;
        }

        public View getView(final int p, View convertView, ViewGroup parent) {
            final ViewHolder holder;
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.product_adapter,
                        parent, false);// adapter_home
                holder = new ViewHolder();
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            holder.view = (LinearLayout) convertView
                    .findViewById(R.id.wrap_rl);
            //holder.lblDateEnd = (TextView) convertView
            //        .findViewById(R.id.lblEndDate);
            holder.product_quantity123=(TextView)convertView.findViewById(R.id.textView222);
            holder.tvProdName = (TextView) convertView
                    .findViewById(R.id.lblstore);
            // holder.prod_qty = (TextView) convertView.findViewById(R.id.p_qty);
            holder.imgThumbnail = (ImageView) convertView
                    .findViewById(R.id.imgThumbnail);
//            holder.right_go=(ImageView)convertView.findViewById(R.id.imageView8);
            holder.tvPercentage = (TextView) convertView.findViewById(R.id.tvPercentage);
            //holder.plus = (ImageView) convertView.findViewById(R.id.plus);
            holder.minus = (ImageView) convertView.findViewById(R.id.textView3);
            // holder.fav = (ImageView) convertView.findViewById(R.id.fav);
            // holder.more = (ImageView) convertView.findViewById(R.id.more);
            holder.count = (ImageView) convertView.findViewById(R.id.textView);
            holder.tvmrp = (TextView) convertView.findViewById(R.id.lblAddress);
            holder.tvoffcost = (TextView) convertView
                    .findViewById(R.id.prod_off_cost);
            holder.tvProdName.setMaxLines(2);
            holder.product_sum_background= (LinearLayout) convertView.findViewById(R.id.product_sum_background);

            //change color
            holder.product_sum_background.setBackgroundColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_background_color,"")));
            holder.tvProdName.setBackgroundColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_background_color,"")));
            holder.tvoffcost.setBackgroundColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_background_color,"")));
            holder.tvProdName.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_text_color,"")));
            holder.tvoffcost.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_text_color,"")));
            holder.product_quantity123.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.font_color,"")));

            //images replace color
//            Drawable myIcon1 = getResources().getDrawable( R.drawable.minus);
//            ColorFilter filter1 = new LightingColorFilter( Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_text_color,"")),  Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_text_color,"")) );
//            myIcon1.setColorFilter(filter1);
//            holder.minus.setImageDrawable(myIcon1);
//
//            Drawable myIcon2 = getResources().getDrawable( R.drawable.plus_symbol);
//            ColorFilter filter2 = new LightingColorFilter( Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_text_color,"")),  Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_text_color,"")) );
//            myIcon2.setColorFilter(filter2);
//            holder.count.setImageDrawable(myIcon2);

//            Drawable myIcon3 = getResources().getDrawable( R.drawable.rightarrow);
//            ColorFilter filter3 = new LightingColorFilter( Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_text_color,"")),  Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_text_color,"")) );
//            myIcon3.setColorFilter(filter3);
//            holder.right_go.setImageDrawable(myIcon3);

            holder.product_quantity123.setTextColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.button_text_color,"")));


            holder.count.setTag(p);
            holder.minus.setTag(p);
//            holder.llOutofstock = (LinearLayout) convertView
//                    .findViewById(R.id.lloutofstock);
//            holder.llAddcart = (LinearLayout) convertView
//                    .findViewById(R.id.lladdcart);
//            holder.cart_text=(TextView)activity.findViewById(R.id.cart_text);
            cart_show_total=(TextView)activity.findViewById(cart_text) ;
            loyal_text=(TextView)activity.findViewById(R.id.loyal_text);


            //giftecode
            if (data.get(p).getitem_image() == null
                    || data.get(p).getitem_image().equals(""))
                holder.imgThumbnail.setImageResource(R.drawable.ic_launcher);
            else
                Picasso.with(activity)
                        .load(data.get(p).getitem_image())
                        .placeholder(R.drawable.foodzu_noimage).fit()
                        .centerCrop().tag(activity).into(holder.imgThumbnail);



            holder.tvProdName.setText(data.get(p).getitem_name());
            System.out.println("productquantityfromser--"+data.get(p).getProduct_quantity().toString());
            holder.product_quantity123.setText(data.get(p).getProduct_quantity().toString());
            holder.tvoffcost.setText("$"+data.get(p).getSelling_price().toString());
            OnclickListeners(p,holder);


//
//            if (data.get(p) != null) {
//                int ind = UpdatePlusCounter(data.get(p)
//                        .getitem_ID());
//                if (ind > -1) {
//                    //holder.count.setText(Integer.toString(Cart_Arraylist.get(
//                    //       ind).getqty_count()));
//                }
//            }

            return convertView;
        }
        public void OnclickListeners(final int positi,final ViewHolder holder){

            holder.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i=new Intent(activity,Product_Details.class);
                    System.out.println("productidintent---"+position_value);
                    i.putExtra("Product_ID",data.get(positi).getitem_ID());
                    i.putExtra("Product_Name",data.get(positi).getitem_name());
                    i.putExtra("Tabvalue",String.valueOf(position_value));
                    System.out.println("tabvaluefromproductpage---"+String.valueOf(position_value));
                    startActivity(i);
                    finish();
//                    ProductDetailsFragment1 fragmentS1 = new ProductDetailsFragment1(data.get(positi).getitem_ID(),positi,false);
//                    FragmentManager fragmentManager = getFragmentManager();
//                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//                    fragmentTransaction.replace(R.id.fragment_content, fragmentS1);
//                    fragmentTransaction.commit();

                }
            });
//            holder.right_go.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    Intent i=new Intent(activity,Product_Details.class);
//                    System.out.println("productidintent---"+position_value);
//                    i.putExtra("Product_ID",data.get(positi).getitem_ID());
//                    i.putExtra("Product_Name",data.get(positi).getitem_name());
//                    i.putExtra("Tabvalue",String.valueOf(position_value));
//                    startActivity(i);
//                    finish();
//                }
//            });

            holder.count.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    //  int position = (Integer) v.getTag();
                    PAGE_NAME="add_to_cart";
                    System.out.println("plusdatewhileplus---"+data.get(positi).getProduct_quantity());
                    value = Integer.parseInt(data.get(positi).getProduct_quantity());
                    if (value >= 0) {
                        attvalue = data.get(positi).getAttribute_id();
                        value = value + 1;
                        System.out.println("value in Add---"+value);
                        //new AsyCartAdd().execute(data.get(p).getitem_ID()+ "-1-" + data.get(p).getpd_wieght());
                        try {
                            holder.product_quantity123.setText(String.valueOf(value));
                        }
                        catch (Exception e){
                            e.printStackTrace();
                        }
                        new AsyCartAdd().execute(data.get(positi).getitem_ID(),Integer.toString(value),String.valueOf(positi));

                        data.get(positi).setProduct_quantity(String.valueOf(value));



                        if (data.get(positi).getitem_in_cart().equals("NO")) {
                            data.get(positi).setitem_in_cart("YES");
                            Cart_Arraylist.add(data.get(positi));
                        } else
                            data.get(positi).setqty_count(value);

                        if (Cart_Arraylist.size() == 0)
                            item_total_cost = 0;
                        item_total_cost = item_total_cost
                                + Double.parseDouble(data.get(positi)
                                .getSelling_price());
                        System.out.println("item_total_cost--"+item_total_cost);
                    }

                    notifyDataSetChanged();
                    FL_Adapter.notifyDataSetChanged();


                }
            });

            holder.minus.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    //  int position=(Integer)v.getTag();
                    PAGE_NAME="removecart";
                    int cnt_val = 0;
                    for (int m = 0; m < Cart_Arraylist.size(); m++) {
                        if (Cart_Arraylist.get(m) != null)
                            if (Cart_Arraylist.get(m).getitem_ID()
                                    .equals(data.get(positi).getitem_ID()))
                                cnt_val = m;
                    }
                    value = Integer.parseInt(data.get(positi).getProduct_quantity());
                    if (value > 0) {

                        value = value - 1;
                        Log.d("TAG","value in Remove "+value);
                        //  new AsyCartRemove().execute(data.get(p).getitem_ID()+ "-1-" + data.get(p).getpd_wieght());
                        attvalue = data.get(positi).getAttribute_id();

                        holder.product_quantity123.setText(String.valueOf(value));
                        new AsyCartRemove().execute(data.get(positi).getitem_ID(),Integer.toString(value));

                        data.get(positi).setProduct_quantity(String.valueOf(value));
                        //  Cart_Arraylist.get(cnt_val).setqty_count(value);
//                        if (Cart_Arraylist.size() == 0)
//                            item_total_cost = 0;
//                        item_total_cost = item_total_cost
//                                - Double.parseDouble(data.get(positi)
//                                .getSelling_price());

                        if (value == 0) {
                            if (data.get(positi).getitem_in_cart().equals("YES")) {
                                data.get(positi).setitem_in_cart("NO");
                                Cart_Arraylist.remove(Cart_Arraylist
                                        .get(cnt_val));
                                Utils.Cart_checklist.getcartdata();
                                notifyDataSetChanged();
                                FL_Adapter.notifyDataSetChanged();
//                                ((HomeActivity) mContext).buttombarAction(
//                                        Cart_Arraylist, item_total_cost);
                            }
                        }
                    }
                    notifyDataSetChanged();
                    FL_Adapter.notifyDataSetChanged();
//                    ((HomeActivity) mContext).buttombarAction(Cart_Arraylist,
//                            item_total_cost);

                }
            });

        }

        private class AsyCartAdd extends AsyncTask<String, Void, String> {

            Dialog dialog;

            protected void onPreExecute() {
                showProgressDialog();
            }

            @Override
            protected String doInBackground(String... place) {
                String productId = place[0];
                String v = place[1];
                String posi=place[2];
                Log.d("TAG","Quantity  "+v);
                Log.d("TAG","Attributes  "+attvalue);
                return cartRequest(productId, attvalue,v);
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                hideProgressDialog();

                Log.v("RESPONSE ","ADD ITEM  ::::: "+result);

                if (result != null) {
                    JSONObject jObject = null;
                    try {
                        jObject = new JSONObject(result);
                        System.out.println("loginjson object" + jObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    try {
                        json_result = jObject.getString("success");
                        if (json_result.equals("1")) {
                            System.out.println("cartquantitywhileplus"+String.valueOf(jObject.get("cart_quantity")));
                            Prefs.putString(CHOPMOBILE_CONSTANTS.cart_show_value,String.valueOf(jObject.get("cart_quantity")));
                            cart_show_total.setText(Prefs.getString(CHOPMOBILE_CONSTANTS.cart_show_value,"0"));
                            System.out.println("menuquantity--"+jObject.get("menu_quantity").toString());
                            PRODUCT_QUANTITY=jObject.get("menu_quantity").toString();

                            item=new SubProduct();
                            item.setProduct_quantity(PRODUCT_QUANTITY);

//                            Intent intent = getIntent();
//                            finish();
//                            startActivity(intent);
//                            gift_product();
//                            new getGiftProductsTask().execute();
                            notifyDataSetChanged();
                            FL_Adapter.notifyDataSetChanged();
//                            if(PRODUCT_QUANTITY!=null&&!PRODUCT_QUANTITY.isEmpty()) {
//                                product_quantity.setText(String.valueOf(PRODUCT_QUANTITY));
//                            }
                            Toast.makeText(activity, "Cart Successfully Updated", Toast.LENGTH_LONG).show();

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    Log.d("TAG", "Result " + result.toString());
                }

            }

        }
        //addcartrequest
        public String cartRequest(String productId, int attrvalue,String v) {

            String result = null;
            String URL = Utils.DOMAIN_NAME;
            if(attrvalue==0)
                attrvalue=1;
            try {

                List<NameValuePair> pairs = new ArrayList<NameValuePair>();
                //pairs.add(new BasicNameValuePair("user_id", UID));
                pairs.add(new BasicNameValuePair("device_id", Utils
                        .getDeviceId(activity)));
                pairs.add(new BasicNameValuePair("page_name", PAGE_NAME));
                pairs.add(new BasicNameValuePair("menuid", productId));
                pairs.add(new BasicNameValuePair("device", "ANDROID"));
                pairs.add(new BasicNameValuePair("attribute_id",Integer.toString(attrvalue) )); //Integer.toString(attrvalue)
                pairs.add(new BasicNameValuePair("quantity",v ));  //Integer.toString(value)
                pairs.add(new BasicNameValuePair("user_id",Prefs.getString(CHOPMOBILE_CONSTANTS.shared_user_id, "")));
               // pairs.add(new BasicNameValuePair("location_id",Prefs.getString(CHOPMOBILE_CONSTANTS.location_id,"")));
                pairs.add(new BasicNameValuePair("rest",Prefs.getString(CHOPMOBILE_CONSTANTS.Restaurant_Id,"")));

                HttpClient httpClient = new DefaultHttpClient();
                Log.d("TAG","Values add to cart"+pairs);
                HttpPost httpPost = new HttpPost(URL);
                httpPost.setEntity(new UrlEncodedFormEntity(pairs));
                HttpResponse httpResponse = httpClient.execute(httpPost);
                result = EntityUtils.toString(httpResponse.getEntity());
                Log.d("TAG","Added "+v);
                Log.d("TAG","Result "+result);
                //Log.d("TAG","HTTP Entity : " + convertStreamToString(httpPost.getEntity().getContent()));
            } catch (Exception e) {
                Log.d("InputStream", e.getLocalizedMessage());
            }

            return result;

        }
        private class AsyCartRemove extends AsyncTask<String, Void, String> {

            Dialog dialog;

            protected void onPreExecute() {
                showProgressDialog();
            }

            @Override
            protected String doInBackground(String... place) {
                String productId = place[0];
                String v = place[1];
                Log.d("TAG","Quantity  "+v);

                return cartRequest(productId, attvalue,v);
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                hideProgressDialog();
                if (result != null) {
                    JSONObject jObject = null;
                    try {
                        jObject = new JSONObject(result);
                        System.out.println("loginjson object" + jObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    try {
                        json_result = jObject.getString("success");
                        if (json_result.equals("1")) {
                            Prefs.putString(CHOPMOBILE_CONSTANTS.cart_show_value,String.valueOf(jObject.get("cart_quantity")));
                            cart_show_total.setText(String.valueOf(jObject.get("cart_quantity")));
                            PRODUCT_QUANTITY=jObject.get("menu_quantity").toString();

                            item=new SubProduct();
                            item.setProduct_quantity(PRODUCT_QUANTITY);

//                            new getGiftProductsTask().execute();
//                            Intent intent = getIntent();
//                            finish();
//                            startActivity(intent);
//                            gift_product();
                            notifyDataSetChanged();
                            FL_Adapter.notifyDataSetChanged();
                            Toast.makeText(activity, "Removed from Cart", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Log.d("TAG", "Result " + result.toString());

                }
            }

        }


        public void deleteItem(int pos) {

            data.remove(pos);
            DELETE_POS = INVALID;
            notifyDataSetChanged();
        }

        public void onSwipeItem(boolean isRight, int position) {

            if (isRight == false) {
                DELETE_POS = position;
            } else if (DELETE_POS == position) {
                DELETE_POS = INVALID;
            }
            notifyDataSetChanged();
        }
    }

    int UpdatePlusCounter(String Id) {
        for (int i = 0; i < Cart_Arraylist.size(); i++) {
            if (Cart_Arraylist.get(i) != null) {
                if (Cart_Arraylist.get(i).getitem_in_cart().equals("YES")) {

                    if (Cart_Arraylist.get(i).getitem_ID().equals(Id)) {
                        System.out.println(i);
                        return i;
                    }
                }
            }
        }
        return -1;
    }

    void favorite(final String prod_id) {
        PID = prod_id;
        if (IsInternetPresent()) {
            //new remove_favList().execute();
        } else {
            alertDialogBuilder
                    .setMessage("Internet/Mobile Data Not Available!")
                    .setCancelable(false)
                    .setPositiveButton("Retry",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    favorite(prod_id);
                                }
                            });
            alertDialog = alertDialogBuilder.create();
            alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            alertDialog.show();
        }
    }

    int updatedata(String itemID) {
        if (Cart_Arraylist != null && Cart_Arraylist.size() > 0) {
            for (int c = 0; c < Cart_Arraylist.size(); c++) {
                if (Cart_Arraylist.get(c) != null) {
                    if (Cart_Arraylist.get(c).getitem_ID().equals(itemID)) {
                        if (Cart_Arraylist.get(c).getqty_count() > 0
                                && Cart_Arraylist.get(c).getitem_in_cart()
                                .equals("YES")) {
                            return c;
                        }
                    }
                }
            }
        }
        return -1;
    }


    public void onRemoveAllTabs(View v) {
        getActionBar().removeAllTabs();
    }

    @Override
    public void onTabSelected(Tab tab, FragmentTransaction ft) {
        bar.setSelectedNavigationItem(tab.getPosition());
        System.out.println("onTabSelected----"+tab.getPosition());
    }

    @Override
    public void onTabUnselected(Tab tab, FragmentTransaction ft) {

    }

    @Override
    public void onTabReselected(Tab tab, FragmentTransaction ft) {


    }



    /**
     * A TabListener receives event callbacks from the action bar as tabs
     * are deselected, selected, and reselected.
     **/
    private class TabListener implements ActionBar.TabListener {
        private TabContentFragment mFragment;

        public TabListener(TabContentFragment fragment) {
            mFragment = fragment;

        }

        public void onTabSelected(Tab tab, FragmentTransaction ft) {

            ft.show(mFragment);
            ft.add(R.id.fragment_content, mFragment, mFragment.getText());
            text = mFragment.getText().toString();


            parsejsonnew(json);
            System.out.println("positionvsaluetabchange---"+position_value);


//            bar.setSelectedNavigationItem(position_value);
//            FL_Adapter = new GiftAdapter(Product_TabPage.this,finaldata);
//            myfavoritelist.setAdapter(FL_Adapter);
//            bar.setSelectedNavigationItem(position_value);
//            ft.detach(mFragment);
        }

        public void onTabUnselected(Tab tab, FragmentTransaction ft) {
            ft.remove(mFragment);
            System.out.println("inside Tabunselected");
//            parsejsonnew(json);
            // gift_product();

        }

        public void onTabReselected(Tab tab, FragmentTransaction ft) {
//            gift_product();
            System.out.println("inside onTabReselected");
//            parsejsonnew(json);
        }
    }

    @SuppressLint("ValidFragment")
    public class TabContentFragment extends Fragment {
        private String mText;

        public TabContentFragment(String text) {
            mText = text;
        }

        public String getText() {
            return mText;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View fragView = inflater.inflate(R.layout.action_bar_tab_content, container, false);



//            bar.setSelectedNavigationItem(position_value);

            TextView text = (TextView) fragView.findViewById(R.id.text);
            text.setText(mText);
            System.out.println("cuming inside tab fist---"+mText);



            // Main_layout = (RelativeLayout) fragView.findViewById(R.id.main_rel_layout);
            //   View_layout = (RelativeLayout) fragView.findViewById(R.id.view_rel_layout);
            //  Main_layout.setVisibility(View.VISIBLE);

            Cart_Arraylist = new ArrayList<SubProduct>();
            Cart_Arraylistnew = new ArrayList<SubProduct>();
            chkoutcart_top = (ImageView) fragView.findViewById(R.id.cart_icon_top);
            myfavoritelist = (GridView) fragView.findViewById(R.id.myfavorite_list);
            cart_icon=(ImageView)fragView.findViewById(R.id.cart_icon);
            cart_show_total=(TextView)fragView.findViewById(cart_text) ;
            loyal_text=(TextView)fragView.findViewById(R.id.loyal_text);
            search_button=(ImageView)fragView.findViewById(R.id.imageView7);
            System.out.println("sharevalue--"+Prefs.getString(CHOPMOBILE_CONSTANTS.cart_show_value,"0"));
            RelativeLayout  actionbar_back_product=(RelativeLayout) fragView.findViewById(R.id.actionbar_back_product);

            //background colr
            myfavoritelist.setBackgroundColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.background_app_color,"")));
            bar.setStackedBackgroundDrawable(new ColorDrawable(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.actionbar_color,""))));
            actionbar_back_product.setBackgroundColor(Color.parseColor(Prefs.getString(CHOPMOBILE_CONSTANTS.actionbar_color,"")));
            cart_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(cart_show_total.getText().toString().equals("0")){
                        Toast.makeText(Product_TabPage.this,"Cart is Empty",Toast.LENGTH_LONG).show();
                    }
                    else {
                        Intent i = new Intent(Product_TabPage.this, Checkout.class);
                        startActivity(i);
                    }
                }
            });
            search_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i=new Intent(Product_TabPage.this,Search_Activity.class);
                    startActivity(i);
                }
            });
            cart_show_total.setText(Prefs.getString(CHOPMOBILE_CONSTANTS.cart_show_value,"0"));
            loyal_text.setText(Prefs.getString(CHOPMOBILE_CONSTANTS.loyal_points,"0"));


            tv_nofav = (TextView) fragView.findViewById(R.id.no_favorite);
            //favList_Array = new ArrayList<Products>();

            if (Utils.Cart_checklist.getcartdata().size() != 0) {
                Cart_Arraylist.addAll(Utils.Cart_checklist.getcartdata());
                buttombarAction(Cart_Arraylist, Double.valueOf(Utils.GrandTotal), result1);
                item_total_cost = Double.valueOf(Utils.GrandTotal);

            } else {
                Cart_Arraylist.addAll(Utils.Cart_checklist.getcartdata());
                buttombarAction(Cart_Arraylist, Double.valueOf(Utils.GrandTotal), result1);
                // bottom_bar_rl.setVisibility(View.VISIBLE);
                // chkoutcart_buttom.setVisibility(View.GONE);
                ////
                item_total_cost = 0.0;
            }


            System.out.println("ENteredddd");
            if (favList_Array.size() == 0) {
                myfavoritelist.setVisibility(View.GONE);
                tv_nofav.setVisibility(View.VISIBLE);
                //  bottom_bar_rl.setVisibility(View.VISIBLE);
//                chkoutcart_buttom.setVisibility(View.VISIBLE);
            } else {
                if (favList_Array != null)
                    FL_Adapter = new GiftAdapter(Product_TabPage.this,
                            favList_Array);
                FL_Adapter.notifyDataSetChanged();
                myfavoritelist.setAdapter(FL_Adapter);


            }
            System.out.println("ENteredddd  222");
            return fragView;
        }

        int UpdatePlusCounter(String Id) {
            for (int i = 0; i < Cart_Arraylist.size(); i++) {
                if (Cart_Arraylist.get(i) != null) {
                    if (Cart_Arraylist.get(i).getitem_in_cart().equals("YES")) {

                        if (Cart_Arraylist.get(i).getitem_ID().equals(Id)) {
                            System.out.println(i);
                            return i;
                        }
                    }
                }
            }
            return -1;
        }

    }


    public class FVCartAdapter extends BaseAdapter {

        private Activity activity;
        private ArrayList<SubProduct> data;
        private LayoutInflater inflater = null;

        SharedPreferences sharedPreferences;
        private ImageView imgThumbnail, plus, closeit, minus;
        private TextView prod_name, prod_cost, prod_offer_cost, prod_desc,
                prod_quantity, count, tab1, tab2, tab3, tab4, tab5;
        JSONObject promoJson;
        HashMap<String, String> item = new HashMap<String, String>();
        private Context mContext;
        int value = 0;
        String control = "";

        public FVCartAdapter(Activity a, ArrayList<SubProduct> d) {
            activity = a;
            data = d;
            this.mContext = a;
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            sharedPreferences = PreferenceManager
                    .getDefaultSharedPreferences(activity);

        }

        public int getCount() {
            return data.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        class ViewHolder {
            private ImageView imgThumbnail, plus, minus, fav;
            private TextView lblDateEnd, tvmrp, tvoffcost, tvProdName, tvPercentage,
                    prod_qty, count;
            RelativeLayout view;
            LinearLayout llOutofstock, llAddcart;
            // ImageLoader imgLoader;
        }

        public View getView(final int p, View convertView, ViewGroup parent) {
            final ViewHolder holder;
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.adapter_home_new, parent,
                        false);
                holder = new ViewHolder();
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            holder.view = (RelativeLayout) convertView
                    .findViewById(R.id.wrap_rl);

            if (data.get(p).getitem_image() == null
                    || data.get(p).getitem_image().equals(""))
                holder.imgThumbnail.setImageResource(R.drawable.ic_launcher);
            else
                Picasso.with(activity).load(data.get(p).getitem_image())
                        .placeholder(R.drawable.foodzu_noimage).fit()
                        .centerCrop().tag(activity).into(holder.imgThumbnail);

            return convertView;
        }

    }


    private class AsyGiftAdd extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... place) {
            String productId = place[0];
            String att = place[1];
            return cartGiftRequest(productId, att);

        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            result1 = result;
            buttombarAction(Cart_Arraylist, item_total_cost, result1);


        }

    }

    private class AsyGiftRemove extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... place) {
            String productId = place[0];
            String att = place[1];
            return cartGiftRemove(productId, att);

        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            result1 = result;
            buttombarAction(Cart_Arraylist, item_total_cost, result1);


        }

    }


    public void buttombarAction(ArrayList<SubProduct> product,
                                Double item_total_sum, String result) {

        if (product.size() > 0) {
            chkoutcart_top.setEnabled(true);
            cart_empty_tv.setVisibility(View.GONE);
            cart_empty.setVisibility(View.GONE);
            // bottom_bar_rl.setVisibility(View.VISIBLE);
            chkoutcart_buttom.setVisibility(View.VISIBLE);
            cartitem_count.setText(Integer.toString(product.size()));
            top_cartitem_count.setText(Integer.toString(product.size()));
            // GRD_TOTAL = Double.toString(item_total_sum);

            this.item_total_cost = item_total_sum;
            //total_cost.setText("\u20B9  " + Double.toString(item_total_sum));
            this.result1 = result;
            System.out.println("Results....." + result1);

            JSONObject jObj_main = null;
            try {
                jObj_main = new JSONObject(result1);
                gamt = jObj_main.getString("giftable_amount");
                extraAmt = jObj_main.getString("gift_extra_pick");
                usedAmt = jObj_main.getString("used_gift_amount");

                // System.out.println("A B C is...."+a+b+c);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            //System.out.println("A B C is....111"+a+b+c);
            total_cost.setText("Giftable amt:       ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¹" + gamt + "\n" + "Used Gift amt:      ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¹" + usedAmt + "\n" + "Extra Pick:           ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¹" + extraAmt);

            top_total_cost
                    .setText("\u20B9  " + Double.toString(item_total_sum));

            if (CART_adapt != null) {
                CART_adapt.notifyDataSetChanged();
                CART_adapt = new FVCartAdapter(Product_TabPage.this,
                        Cart_Arraylist);
                cartproductlist.invalidateViews();
            } else {
                CART_adapt = new FVCartAdapter(Product_TabPage.this,
                        Cart_Arraylist);
                cartproductlist.setAdapter(CART_adapt);
                CART_adapt.notifyDataSetChanged();
            }
            if (FL_Adapter != null) {
                FL_Adapter.notifyDataSetChanged();

                FL_Adapter = new GiftAdapter(Product_TabPage.this,
                        favList_Array);
                myfavoritelist.invalidateViews();
                // homeproductlist.setAdapter(PD_adapt);
            } else {

                FL_Adapter = new GiftAdapter(Product_TabPage.this,
                        favList_Array);
                myfavoritelist.invalidateViews();
                // homeproductlist.setAdapter(PD_adapt);
                FL_Adapter.notifyDataSetChanged();
            }

        } else {
            // chkout_top.setVisibility(View.INVISIBLE);
            chkoutcart_top.setEnabled(false);
            if (FL_Adapter != null)
                FL_Adapter.notifyDataSetChanged();
            else {

                FL_Adapter = new GiftAdapter(Product_TabPage.this,
                        favList_Array);
                // homeproductlist.setAdapter(PD_adapt);
                FL_Adapter.notifyDataSetChanged();
            }

        }
    }


    public String cartGiftRequest(String productId, String task) {

        String result = null;
        String URL = Utils.GIFT_CART;
        Log.d("TAGG", user_id + " " + productId + " " + task + " " + total + " " + scost);
        try {

            List<NameValuePair> pairs = new ArrayList<NameValuePair>();
            //pairs.add(new BasicNameValuePair("user_id", user_id));
            pairs.add(new BasicNameValuePair("device", "ANDROID"));
            pairs.add(new BasicNameValuePair("device_id", Utils.getDeviceId(Product_TabPage.this)));
            pairs.add(new BasicNameValuePair("product_id", productId));
            pairs.add(new BasicNameValuePair("attribute_id", task));
            pairs.add(new BasicNameValuePair("quantity", "1"));
            //pairs.add(new BasicNameValuePair("location_id",Prefs.getString(CHOPMOBILE_CONSTANTS.location_id,"")));
            pairs.add(new BasicNameValuePair("rest",Prefs.getString(CHOPMOBILE_CONSTANTS.Restaurant_Id,"")));
            //pairs.add(new BasicNameValuePair("sub_total", total));
            //pairs.add(new BasicNameValuePair("shipping", scost));
            //pairs.add(new BasicNameValuePair("user_country", "105"));

            DefaultHttpClient httpClient = new DefaultHttpClient();

            HttpPost httpPost = new HttpPost(URL);
            httpPost.setEntity(new UrlEncodedFormEntity(pairs));
            HttpResponse httpResponse = httpClient.execute(httpPost);
            result = EntityUtils.toString(httpResponse.getEntity());
            System.out.println("TASK.......11........." + user_id + " " + productId + " " + task + " " + total + " " + scost);

        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }
        Log.d("TAGG", "RESULT IS ........" + result);
        return result;

    }

    public String cartGiftRemove(String productId, String task) {

        String result = null;
        String URL = Utils.GIFT_CART;
        Log.d("TAGG", "" + user_id + " " + productId + " " + task + " " + total + " " + scost);
        try {

            List<NameValuePair> pairs = new ArrayList<NameValuePair>();
            //pairs.add(new BasicNameValuePair("user_id", user_id));
            pairs.add(new BasicNameValuePair("device", "ANDROID"));
            pairs.add(new BasicNameValuePair("device_id", Utils.getDeviceId(Product_TabPage.this)));
            pairs.add(new BasicNameValuePair("product_id", productId));
            pairs.add(new BasicNameValuePair("attribute_id", task));
            pairs.add(new BasicNameValuePair("quantity", "0"));
            //pairs.add(new BasicNameValuePair("location_id",Prefs.getString(CHOPMOBILE_CONSTANTS.location_id,"")));
            pairs.add(new BasicNameValuePair("rest",Prefs.getString(CHOPMOBILE_CONSTANTS.Restaurant_Id,"")));
            //pairs.add(new BasicNameValuePair("sub_total", total));
            //pairs.add(new BasicNameValuePair("shipping", scost));
            //pairs.add(new BasicNameValuePair("user_country", "105"));

            DefaultHttpClient httpClient = new DefaultHttpClient();

            HttpPost httpPost = new HttpPost(URL);
            httpPost.setEntity(new UrlEncodedFormEntity(pairs));
            HttpResponse httpResponse = httpClient.execute(httpPost);
            result = EntityUtils.toString(httpResponse.getEntity());
            System.out.println("TASK.......11........." + user_id + " " + productId + " " + task + " " + total + " " + scost);

        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }
        Log.d("TAGG", "RESULT IS ........" + result);
        return result;

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //Toast.makeText(getApplicationContext(),"Password Reset Success", Toast.LENGTH_SHORT).show();
                onBackPressed();
//                Intent a = new Intent(getApplicationContext(), HomeActivity.class);
//                startActivity(a);
                finish();

                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private class getGiftProductsTask extends AsyncTask<String, Void, String> {

        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... URL) {

            try {

                String login_url = Utils.DOMAIN_NAME;
                //  String login_url = Utils.GIFT_PRODUCTS_NEW;
                System.out.println("producturl" + login_url);
                HttpClient httpclient = new DefaultHttpClient();
                System.out.println("post url:-" + login_url);
                HttpPost httppost = new HttpPost(login_url);

                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);

                nameValuePairs.add(new BasicNameValuePair("page_name", "category_menu"));
//                nameValuePairs.add(new BasicNameValuePair("location_id","1"));
                nameValuePairs.add(new BasicNameValuePair("user_id",Prefs.getString(CHOPMOBILE_CONSTANTS.shared_user_id,"")));
                //nameValuePairs.add(new BasicNameValuePair("location_id",Prefs.getString(CHOPMOBILE_CONSTANTS.location_id,"")));
                nameValuePairs.add(new BasicNameValuePair("rest",Prefs.getString(CHOPMOBILE_CONSTANTS.Restaurant_Id,"")));
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                System.out.println("productpost all namevalues" + nameValuePairs);

                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);

                BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                sb.append(reader.readLine() + "\n");
                String line = "0";
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                reader.close();
                result11 = sb.toString();
                System.out.println("post result19:-" + result11);
                // parsing data

            } catch (Exception e) {
                e.printStackTrace();

            }
            return result11;
        }

        protected void onPostExecute(String getResult) {
            super.onPostExecute(getResult);
            System.out.println("logininside post");
            System.out.println("productlogin post result:-" + getResult);
            json = "";
            json = getResult;
            parsejson(getResult);

            parsejsonnew(getResult);
            System.out.println("5esulttest---" + getResult);
        }
    }
    ArrayList<SubProduct> parsejsonnew(String homeproducts) {

        Log.v("CALL METHOD"," :::::::::::::::::: "+homeproducts);
        try {
            favList_Array.clear();

            if (homeproducts != null && homeproducts.length() > 0) {

                JSONObject jObj_main = new JSONObject(homeproducts);
                System.out.println("cartquantity----"+String.valueOf(jObj_main.getString("cart_quantity")));
                Prefs.putString(CHOPMOBILE_CONSTANTS.cart_show_value,String.valueOf(jObj_main.getString("cart_quantity")));
                // cart_show_total.setText(String.valueOf(jObj_main.getString("cart_quantity")));
                if (!jObj_main.getString("success").equals("0")) {
                    System.out.println("cuming inside product json---");
                    JSONArray result = jObj_main.getJSONArray("category");
                    if (result.length() > 0) {
                        for (int i = 0; i < result.length(); i++) {
                            JSONObject cat = result.getJSONObject(i);
                            int total = cat.getInt("menu_total");
                            System.out.println("Total" +total);
                            if (total > 0) {
                                JSONArray product = cat.getJSONArray("menu_data");
                                System.out.println("Productlenght"+product.length());

                                if (product.length() > 0) {
                                    for (int j = 0; j < product.length(); j++) {
                                        item_of_product = new Products();
                                        JSONObject items = product
                                                .getJSONObject(j);
                                        System.out.println("GIFTSterm----" +items);
                                        //int sub_prod = items
                                        //        .getInt("products_count");
                                        //System.out.println("Sub" + sub_prod);
                                        if(text.compareTo(cat.getString("category_name"))==0){
                                            System.out.println("TEXT is.........."+text);
                                            Log.d("TAGG","TEXT is"+text);
                                            //if (sub_prod > 0) {
                                            //JSONArray subproduct = items
                                            //      .getJSONArray("product_list");
                                            //for (int x = 0; x < subproduct
                                            //        .length(); x++) {
                                            // JSONObject obj = subproduct
                                            //         .getJSONObject(x);
                                            item = new SubProduct();
                                            item.setitem_ID(items.getString(
                                                    "menu_id")
                                                    .toString());
                                            item.setitem_name(items
                                                    .getString(
                                                            "menu_name")
                                                    .toString());
                                            item.setProduct_quantity(items.getString("menu_quantity").toString());
//                                            item.setMRP_price(items
//                                                    .getString(
//                                                            "product_mrp")
//                                                    .toString());
                                            item.setSelling_price(items.getString("menu_price").toString());
//                                            item.setvaliddiscount(items
//                                                    .getString(
//                                                            "purchased_offer")
//                                                    .toString());
//                                            item.setAttribute_id(items
//                                                    .getInt("attribute_id")
//                                            );
//                                            item.setOfferPercentage(items
//                                                    .getString(
//                                                            "product_offer_percentage")
//                                                    .toString());
                                            item.setitem_image(items
                                                    .getString("menu_image")
                                                    .toString());
//                                            item.setpd_wieght(items
//                                                    .getString(
//                                                            "product_weight")
//                                                    .toString());
//                                            item.setIsOutOfStock(items.getString(
//                                                    "max_quantity").toString());
                                            //if (x == 0) {
                                            int index = updatedata(items
                                                    .getString(
                                                            "menu_id")
                                                    .toString());
                                            if (index > -1) {
                                                item.setqty_count(Cart_Arraylist
                                                        .get(index)
                                                        .getqty_count());
                                                item.setitem_in_cart("YES");
                                            } else {
                                                item.setitem_in_cart("NO");
                                                item.setqty_count(0);
                                            }
                                            item_of_product
                                                    .setprod_0(item);


                                            // for loop for subproducts
                                            item_of_product
                                                    .setpd_description(items
                                                            .getString(
                                                                    "menu_name")
                                                            .toString());
//                                            item_of_product
//                                                    .setlocation_specific(items
//                                                            .getString(
//                                                                    "islocspecific")
//                                                            .toString());
//                                            item_of_product
//                                                    .setlocation_availability(items
//                                                            .getString(
//                                                                    "islocspecific")
//                                                            .toString());
//                                            item_of_product
//                                                    .setlocation_address(items
//                                                            .getString(
//                                                                    "islocspecific")
//                                                            .toString());
//                                            item_of_product
//                                                    .setIs_Favorite(items
//                                                            .getString("is_fav")
//                                                            .toString());


                                            favList_Array.add(item);
                                            //}
                                            //}
                                        }
                                        //favList_Array.add(item);
                                    }// End of for-loop
                                }

                            }// total products count
                        }
                    }
                }
            }
            System.out.println("BE:" + favList_Array.size());

            return favList_Array;
        } catch (JSONException e) {
            System.out.println(e);
        }
        return null;
    }
    //    @Override
//    public void onCartUpdate(ArrayList<SubProduct> cartList, Double totalSum,
//                             String type, SubProduct cartProduct) {
//        Cart_Arraylist = cartList;
//        if (type.equals("plus")) {
//
//            item_total_cost = item_total_cost
//                    + Double.parseDouble(cartProduct.getSelling_price());
//            value = cartProduct.getqty_count();
//            System.out.println("value in add "+value);
//            String[] ID = cartProduct.getitem_ID().split("-");
//            attvalue = cartProduct.getAttribute_id();
//            //new AsyCartAdd().execute(ID[0]+ "-1-"+ cartProduct.getpd_wieght());
//
//            //  new AsyCartAdd().execute(ID[0],Integer.toString(value));
//
//        }
//        //else if (type.equals("minus")) {
////
////                item_total_cost = item_total_cost
////                        - Double.parseDouble(cartProduct.getSelling_price());
////                buttombarAction(Cart_Arraylist, item_total_cost);
////                String[] ID = cartProduct.getitem_ID().split("-");
////                attvalue = cartProduct.getAttribute_id();
////                value = cartProduct.getqty_count();
////                Log.d("TAG","value in removeeee "+value);
////                new AsyCartRemove().execute(ID[0],Integer.toString(value));
////            }
//
//    }
//    @Override
//    public void onAllBrandProduct(String brandId){
//
//        Log.d("TAGG","I am from detaisl"+brandId);
//        FragmentManager fragmentManager = getFragmentManager();
////        fragmentManager.beginTransaction()
////                .replace(R.id.container, new PlaceholderFragment()).commit();
////        this.brandId=brandId;
////        onBrandSelect();
//
//
//    }
    private void showProgressDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideProgressDialog() {
        if (pDialog.isShowing())
            pDialog.hide();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        pDialog.dismiss();
    }

    //    @Override
//    protected void onResume() {
//        super.onResume();
//    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_product, menu);


        // show menu only when home fragment is selected
//        if (navItemIndex == 0) {
//            getMenuInflater().inflate(R.menu.main_product, menu);
//            textView=(MenuItem)menu.findItem(R.id.action_logout);
//
//            int positionOfMenuItem = 0; // or whatever...
//            MenuItem item = menu.getItem(positionOfMenuItem);
//            SpannableString s = new SpannableString("My red MenuItem");
//            s.setSpan(new ForegroundColorSpan(Color.RED), 0, s.length(), 0);
//
////            textView.setIcon(R.drawable.list_background_rounded);
//        }
//
//        // when fragment is notifications, load the menu created for notifications
//        if (navItemIndex == 6) {
//            getMenuInflater().inflate(R.menu.notifications, menu);
//        }
        return true;
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        System.out.println("cuming inside onresume----");
        finish();
        startActivity(getIntent());
    }
}

