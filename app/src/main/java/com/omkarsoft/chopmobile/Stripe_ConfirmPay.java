package com.omkarsoft.chopmobile;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.pixplicity.easyprefs.library.Prefs;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import HellpersClass.CHOPMOBILE_CONSTANTS;
import HellpersClass.Utils;

/**
 * Created by user1 on 12-Jun-17.
 */

public class Stripe_ConfirmPay extends Activity{
    TextView total_amount;
    Button confirm_pay;
    private ProgressDialog pDialog;
    private ProgressDialog mProgressDialog;
    String pay_money_url;
    String result11;
    String TOKEN_STRIPE;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.stripe_confirm_pay);

        pDialog = new ProgressDialog(Stripe_ConfirmPay.this);
        pDialog.setIndeterminate(true);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
        pDialog.setCanceledOnTouchOutside(false);
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

//        mProgressDialog = new ProgressDialog(Stripe_ConfirmPay.this);
//        mProgressDialog.setIndeterminate(true);
//        mProgressDialog.setMessage("Loading...");
//        mProgressDialog.setCancelable(false);
//        mProgressDialog.setCanceledOnTouchOutside(false);
//        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        TOKEN_STRIPE=getIntent().getStringExtra("token_id123");

        new STRIPE_TALK_TO_SERVER().execute();
//       total_amount=(TextView)findViewById(R.id.total_amount);
//        confirm_pay=(Button) findViewById(R.id.confirm_but);
//
//        total_amount.setText("Total Amount: "+Prefs.getString(CHOPMOBILE_CONSTANTS.stripe_amount,""));
//        confirm_pay.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
               // LoadPrivacy(Stripe_ConfirmPay.this,getIntent().getStringExtra("token_id123"));
//            }
//        });

    }
    public void LoadPrivacy(Context context, final String tokenId) {
        // mPostCommentResponse.requestStarted();
        showProgressDialog();
        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest sr = new StringRequest(Request.Method.POST, Utils.DOMAIN_NAME, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //mPostCommentResponse.requestCompleted();
                System.out.println("deleteresponsestring123---" + response.toString());
                aie_fetch_delete_details(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("stripeeroror----"+error);
                // mPostCommentResponse.requestEndedWithError(error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("page_name", "stripetoken_pay");
                params.put("amt", Prefs.getString(CHOPMOBILE_CONSTANTS.stripe_amount,""));
                params.put("token",tokenId);
                params.put("email",Prefs.getString(CHOPMOBILE_CONSTANTS.profile_email,""));
              //  params.put("location_id",Prefs.getString(CHOPMOBILE_CONSTANTS.location_id,""));
//                params.put("rest",Prefs.getString(CHOPMOBILE_CONSTANTS.Restaurant_Id,""));

                System.out.println("stripeaddparamscheckout---" + params);
                return params;
            }

            @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
            Map<String, String> params = new HashMap<String, String>();
            params.put("Content-Type", "application/x-www-form-urlencoded");
            return params;
        }
    };
        queue.add(sr);
    }

    public void aie_fetch_delete_details(String jsonObject) {
        Gson gson = new Gson();
        hideProgressDialog();
        try {
            JSONObject jobj = new JSONObject(jsonObject);
            System.out.println("stripejosobjectfromser--"+jobj);

            if(jobj.getString("status").equals("succeeded")) {
                Toast.makeText(Stripe_ConfirmPay.this,jobj.getString("seller_message"),Toast.LENGTH_LONG).show();
                new TALK_TO_SERVER().execute();
            }
            else {
                Toast.makeText(Stripe_ConfirmPay.this,jobj.getString("message"),Toast.LENGTH_LONG).show();
                finish();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void showProgressDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideProgressDialog() {
        if (pDialog.isShowing())
            pDialog.hide();
        pDialog.dismiss();
    }
    private class TALK_TO_SERVER extends AsyncTask<String, Void, String> {

        protected void onPreExecute() {
            super.onPreExecute();
            //pDialog.show();
        }
        @Override
        protected String doInBackground(String...URL) {
            try {

                pay_money_url= Utils.DOMAIN_NAME;
                //pay_money_url = "http://maithreyaa.com/app/index.php?page=payment_process&product_id="+productId+"&product_name="+productDesc+"&product_price="+PRICE+"&payment_type=3&user_id="+ Prefs.getString(CHOPMOBILE_CONSTANTS.shared_user_id,"")+"&transcation_id="+transaction_id+"&payment_status=approved";

                HttpClient httpclient = new DefaultHttpClient();
                System.out.println("sampletimepost_url:-"+ pay_money_url);
                HttpPost httppost = new HttpPost(pay_money_url);
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);

                System.out.println("print_product_ur_id all namevalues" + nameValuePairs);
                nameValuePairs.add(new BasicNameValuePair("page_name","orders"));
                nameValuePairs.add(new BasicNameValuePair("userid", Prefs.getString(CHOPMOBILE_CONSTANTS.shared_user_id,"")));
                nameValuePairs.add(new BasicNameValuePair("device_id",Utils.getDeviceId(Stripe_ConfirmPay.this)));
                nameValuePairs.add(new BasicNameValuePair("delivery_address",Prefs.getString(CHOPMOBILE_CONSTANTS.customer_delivery_address,"")));
                nameValuePairs.add(new BasicNameValuePair("delivery_type",Prefs.getString(CHOPMOBILE_CONSTANTS.delivery_type,"")));
                //nameValuePairs.add(new BasicNameValuePair("location_id",Prefs.getString(CHOPMOBILE_CONSTANTS.location_id,"")));
                nameValuePairs.add(new BasicNameValuePair("loyalty_status",Prefs.getString(CHOPMOBILE_CONSTANTS.loyality_status,"")));
                nameValuePairs.add(new BasicNameValuePair("rest",Prefs.getString(CHOPMOBILE_CONSTANTS.Restaurant_Id,"")));

                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                System.out.println("final check values"+nameValuePairs);
                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                System.out.println("print_response" + response);
                BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                sb.append(reader.readLine() + "\n");
                String line = "0";
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                reader.close();
                result11 = sb.toString();
                System.out.println("samplefuelpost result19:-" + result11);
                // parsing data

            } catch (Exception e) {
                e.printStackTrace();

            }
            return result11;
        }
        protected void onPostExecute(String getResult) {
            super.onPostExecute(getResult);
            System.out.println("inside post");
            System.out.println("sampletime post result:-" + getResult);

            if(getResult!=null) {
                pDialog.dismiss();
                System.out.println("cuming cuminginised sample post");

//                AlertDialog.Builder builder = new AlertDialog.Builder(Stripe_ConfirmPay.this);
//                builder.setMessage("Your order has been placed successfully")
//                        .setCancelable(false)
//                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int id) {
//
//                                dialog.cancel();

                                //making share empty
                                Prefs.putString(CHOPMOBILE_CONSTANTS.customer_delivery_address,"");
                                Intent zz=new Intent(Stripe_ConfirmPay.this,HomeActivity.class);
//                                zz.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                                zz.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(zz);
//                                AppController.getInstance().clearApplicationData();
//                                finish();

//                            }
//                        })
//                        //Set your icon here
//                        .setTitle("ChopMobile")
//                        .setIcon(R.drawable.am_logo);
//                AlertDialog alert = builder.create();
//                alert.show();//showing the dialog

                //timeparseResult(getResult);

            }else{
                pDialog.dismiss();
                System.out.println("inside no result");
                System.out.println("number result details:"+getResult);
                AlertDialog.Builder builder = new AlertDialog.Builder(Stripe_ConfirmPay.this);
                builder.setMessage("Somethings went wrong please try after Sometime")
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                dialog.cancel();
                            }
                        })
                        //Set your icon here
                        .setTitle("ChopMobile")
                        .setIcon(R.drawable.ic_launcher);
                AlertDialog alert = builder.create();
                alert.show();//showing the dialog
            }
        }
    }//Async Task Ends

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private class STRIPE_TALK_TO_SERVER extends AsyncTask<String, Void, String> {

        protected void onPreExecute() {
            super.onPreExecute();
            pDialog.show();
        }
        @Override
        protected String doInBackground(String...URL) {
            try {

                pay_money_url= Utils.DOMAIN_NAME;


                HttpClient httpclient = new DefaultHttpClient();
                System.out.println("sampletimepost_url:-"+ pay_money_url);
                HttpPost httppost = new HttpPost(pay_money_url);
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);

                System.out.println("print_product_ur_id all namevalues" + nameValuePairs);
                nameValuePairs.add(new BasicNameValuePair("page_name","stripetoken_pay"));
                nameValuePairs.add(new BasicNameValuePair("amt", Prefs.getString(CHOPMOBILE_CONSTANTS.stripe_amount,"")));
                nameValuePairs.add(new BasicNameValuePair("token",TOKEN_STRIPE));
                nameValuePairs.add(new BasicNameValuePair("email",Prefs.getString(CHOPMOBILE_CONSTANTS.profile_email,"")));



                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                System.out.println("stripe final check values"+nameValuePairs);
                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);
                System.out.println("print_response" + response);
                BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                sb.append(reader.readLine() + "\n");
                String line = "0";
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                reader.close();
                result11 = sb.toString();
                System.out.println("samplefuelpost result19:-" + result11);
                // parsing data

            } catch (Exception e) {
                e.printStackTrace();

            }
            return result11;
        }
        protected void onPostExecute(String getResult) {
            super.onPostExecute(getResult);
            System.out.println("inside post");
            System.out.println("sampletime post result:-" + getResult);

            if(getResult!=null) {
               // pDialog.dismiss();
                Gson gson = new Gson();
               // hideProgressDialog();
                try {
                    JSONObject jobj = new JSONObject(getResult);
                    System.out.println("stripejosobjectfromser--"+jobj);

                    if(jobj.getString("status").equals("succeeded")) {
                        Toast.makeText(Stripe_ConfirmPay.this,jobj.getString("seller_message"),Toast.LENGTH_LONG).show();
                        new TALK_TO_SERVER().execute();
                    }
                    else {
                        Toast.makeText(Stripe_ConfirmPay.this,jobj.getString("message"),Toast.LENGTH_LONG).show();
                        finish();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }else{
                pDialog.dismiss();
                System.out.println("inside no result");
                System.out.println("number result details:"+getResult);
                AlertDialog.Builder builder = new AlertDialog.Builder(Stripe_ConfirmPay.this);
                builder.setMessage("Somethings went wrong please try after Sometime")
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                dialog.cancel();
                            }
                        })
                        //Set your icon here
                        .setTitle("ChopMobile")
                        .setIcon(R.drawable.ic_launcher);
                AlertDialog alert = builder.create();
                alert.show();//showing the dialog
            }
        }
    }//Async Task Ends

    @Override
    protected void onDestroy() {
        super.onDestroy();
        pDialog.dismiss();
    }
}
