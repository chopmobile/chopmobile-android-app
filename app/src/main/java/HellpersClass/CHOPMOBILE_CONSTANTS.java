package HellpersClass;

/**
 * Created by user1 on 02-Mar-17.
 */

public class CHOPMOBILE_CONSTANTS {

    public static String shared_user_id = "shared_user_id";
    public static String shared_user_name = "shared_user_name";
    public static String shared_user_email = "shared_user_email";
    public static String cart_show_value="cart_show_value";
    public static String product_quantity="product_quantity";
    public static String remeber_check="remeber_check";
    public static String forget_email="forget_email";
    public static String push_status="push_status";
    public static String loyal_points="loyal_points";
    public static String profile_full_name="profile_full_name";
    public static String profile_email="profile_email";
    public static String stripe_token="stripe_token";
    public static String stripe_amount="stripe_amount";
    public static String delivery_type="delivery_type";
    public static String customer_delivery_address="customer_delivery_address";
    public static String location_id="location_id";
    public static String use_location_future="use_location_future";

    public static String transparent_app_color="transparent_app_color";
    public static String transparent_text_color="transparent_text_color";
    public static String background_app_color="background_app_color";
    public static String font_color="font_color";
    public static String font="font";
    public static String button_background_color="button_background_color";
    public static String button_text_color="button_text_color";
    public static String background_image="background_image";
    public static String application_logo="application_logo";
    public static String paypal_client_id="paypal_client_id";
    public static String stripe_publishable_key="stripe_publishable_key";
    public static String stripe_secret_key="stripe_secret_key";
    public static String actionbar_color="actionbar_color";
    public static String select_resturant_size="select_resturant_size";
    public static String loyality_status="loyality_status";
    public static String rest_open_status="rest_open_status";
    public static String Restaurant_Id="Restaurant_Id";

}
