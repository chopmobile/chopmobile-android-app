package HellpersClass;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Vibrator;
import android.provider.Settings;

import Model.Data_Models;

/**
 * Created by user1 on 02-Mar-17.
 */

public class Utils {
//    public static final String DOMAIN_NAME = "https://181.224.147.213/chopadmin/appdata.php";
   public static final String DOMAIN_NAME = "https://www.chopmobile.com/chopadmin/appdata.php";

    Context ctx;
    //food
    public static String MAIN = "https://foodzu.com/";
    public static String DOMAIN = "api/";// app/";//apptesting/
    public static String BASE = MAIN + DOMAIN;
    public static String CART_REVIEW	        = BASE + "?page=review_cart";
    public static String getDeviceId(Activity activity) {
        return Settings.Secure.getString(activity.getContentResolver(), Settings.Secure.ANDROID_ID);
    }
    public static String GIFT_PRODUCTS_NEW      = BASE + "?page=gift_products";
    public static Data_Models Cart_checklist = new Data_Models();
    public static String GIFT_CART          	= BASE + "?page=pick_gift_product";
    public static String GrandTotal = "0";
    public Utils(Context c){
        ctx = c;
    }
    // Method to check internet connection
    public boolean isNetworkAvailable() {
        ConnectivityManager connectivity = (ConnectivityManager) ctx
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity == null) {
            return false;
        } else {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
    public static void vibrate(Context context) {
        // Get instance of Vibrator from current Context and Vibrate for 400
        // milliseconds
        ((Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE))
                .vibrate(100);
    }

}
